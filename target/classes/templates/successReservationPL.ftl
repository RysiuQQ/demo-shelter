<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div style="text-align: center">
    <img style=" width: 200px;" src="http://serwer194411.lh.pl/common/images/logo-wielka-racza.png">
</div>

<div style="width: 100%; margin-top: 15px">
    <p><span style="color: red; font-weight: bold;">*</span> Prosimy o nie odpowiadanie na wskazaną wiadomość. Kontaktem mailowym jest: <b>wielka.racza@op.pl</b></p>
    <div style="width: 98%; padding: 1px 5px 20px 5px; border-radius: 5px; text-align: left; color: #155724; background-color: #d4edda; border-color: #c3e6cb;">
        <span style="font-size: 20px; font-weight: 600">Szanowny/a ${fullName}</span><br><br>
        <span>Gratulujemy zarezerwowania pokoju. Liczymy, że będzie to dla Ciebie przyjemny pobyt.</span><br>
        <span>Data rezerwacji: <b>${reservationDate}</b></span>
        <span>Identyfikator rezerwacji: <b>${reservationId}</b></span>
    </div>

    <div style="padding: 10px 5px 20px 5px;">
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">TWÓJ WYBÓR</span><br>
            <span style="font-size: 16px;">Łączna ilość nocy: <span><b>${amountOfNights}</b></span></span><br><br>
            <#list rooms as current>
                <span>Pokój: <b>${current.titlePL()}</b></span><br>
                <span>Opis: <b>${current.descriptionPL()}</b></span><br>
                <span>Zarezerwowanych miejsc w nim: <b>${current.numberOfPeople()}</b></span><br>
                <span>Numer pokoju: <b>${current.roomNumber()}</b></span><br><br>
            </#list>

        </div>
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">OKRES PODRÓŻY</span><br>
            <span>Data zameldowania: <b>${arrivalDate}</b>&nbsp;<span style="color: gray">(g: 15:00)</span></span><br>
            <span>Data wymeldowania: <b>${departureDate}</b>&nbsp;<span  style="color: gray">(g: 10:00)</span></span><br><br>
        </div>
        <div>
            <span style="font-size: 18px; font-weight: 700">PŁATNOŚCI</span><br>
            <span>Łączna kwota do zapłaty: <b>${amountToPay} PLN</b></span><br>
            <span>Wpłacony zadatek wynosi: <b>${deposit} PLN</b></span><br>
        </div>
    </div>


    <div style="width: 100%; text-align: left;">
        <h4 style="color: red">Regulamin Schroniska PTTK Wielka Racza</h4>
        <h5 style="color: darkred">Dziękujemy za dokonanie rezerwacji w
            Schronisku Wielka Racza, poniżej zamieszczamy istotne warunki rezerwacyjne i
            informacyjne:</h5>
        <p><b>1.</b> Regulamin określa zasady świadczenia usług, odpowiedzialności oraz przebywania
            na terenie Schroniska. Dokonanie zameldowania jak również dokonanie rezerwacji lub zapłaty
            zadatku traktuje się jako integralną część umowy. Dokonując ww. czynności, Gość potwierdza, iż zapoznał się i akceptuje warunki regulaminu.
        </p>
        <p>
            <b>2.</b> Regulamin dotyczy wszystkich Gości przebywających na terenie Schroniska PTTK Wielka Racza.
        </p>
        <p>
            <b>3.</b> Gość Schroniska zobowiązuje się do okazania pracownikowi Recepcji dokumentu ze
            zdjęciem potwierdzającego tożsamość podczas zameldowania gościa. W przypadku odmowy okazania dokumentu recepcjonista ma obowiązek odmówić wydania klucza do pokoju.
        </p>
        <p>
            <b>4.</b> Zameldowanie odbywa się na podstawie spisania podstawowych danych meldunkowych w książce meldunkowej
            na terenie recepcji przez recepcjonistę. Pokój w Schronisku wynajmowany jest na doby. Doba hotelowa trwa
            od godziny <b>15:00</b> w dniu wynajmu do godziny <b>10:00</b> w dniu wymeldowania. Okres zameldowania jest przyjmowany na podstawie rezerwacji lub preferencji gościa, w innym przypadku przyjmuje się, że pokój został wynajęty na jedną dobę.
        </p>
        <p>
            <b>5.</b> Pozostawienie rzeczy po godzinie 10:00 bez wcześniejszej informacji, upoważnia obsługę do przeniesienia tych rzeczy bez zgody Gościa do przechowalni bagażu.
            W przypadku pozostania gościa w pokoju po zakończeniu doby noclegowej zostanie naliczona opłata za następny nocleg.
        </p>
        <p>
            <b>6.</b> W przypadku pozostania gościa w pokoju po zakończeniu doby noclegowej zostanie naliczona opłata za następny nocleg.
        </p>
        <p>
            <b>7.</b> Prośba o przedłużenie doby może zostać najpóźniej złożona przed zamknięciem recepcji przed dniem wymeldowania.
            Schronisko może nie uwzględnić życzenia przedłużenia pobytu ze względu na wykorzystanie wszystkich
            miejsc noclegowych lub w przypadku gości nie przestrzegających obowiązującego regulaminu.
        </p>
        <p>
            <b>8.</b> Najmujący pokój Gość, nie może dokonać przekazania pokoju innym osobom.
        </p>
        <p>
            <b>9.</b> Osoby niezameldowane mogą przebywać na terenie obiektu do godziny 22:00.
            Przebywanie osób niezameldowanych po godzinie 22:00 jest równoznaczne z wyrażeniem
            przez nich zgody na dokonanie zakwaterowania w najtańszej możliwej opcji noclegowej na wskazany dzień.
            W przypadku braku miejsc jest naliczana opłata 30zł/os. Odpowiadająca miejscu na podłodze.
        </p>
        <p>
            <b>10.</b> Dzieci do lat 5 nocują bezpłatnie z opiekunem na łóżku.
            W przypadku osobnego łóżka zostanie naliczona opłata
            Za wszelkie szkody powstałe w wyniku działania dzieci, odpowiedzialność ponoszą opiekunowie prawni.
        </p>
        <p>
            <b>11.</b> Na schronisku istnieje możliwość dokonania rezerwacji z psem, na określonych warunkach:
            <ul>
                <li>Klient dokonuje rezerwacji całego pokoju,  nie ma możliwości dokonania rezerwacji w pokoju częściowo zajętym przez inne osoby.</li>
                <li>Klient bierze pełną odpowiedzialność za ewentualne szkody wyrządzone przez psa na terenie całego budynku.</li>
                <li>Klient uiszcza dodatkowe opłaty zamieszczone w cenniku na stronie schroniska PTTK wielka racza.</li>
            </ul>
        </p>
        <p>
            <b>1.</b> Rezerwacja złożona przez klienta jest ważna do godziny 19:30 w dniu przybycia, przekroczenie tej godziny wiąże się z anulacją rezerwacji.
        </p>
        <p>
            <b>2.</b> Na terenie Schroniska, w tym – w pokojach Schroniska zgodnie z ustawą z dnia 8 kwietnia 2010 roku o zmianie ustawy o
            ochronie zdrowia przed następstwami używania tytoniu i wyrobów tytoniowych oraz ustawy o Państwowej Inspekcji Sanitarnej (Dz. U. Nr 81, poz. 529) –
            obowiązuje całkowity zakaz palenia papierosów i wyrobów tytoniowych, zakaz obejmuje korzystanie z e-papierosa.
            Złamanie powyższego zakazu jest równoznaczne z wyrażeniem przez najmującego zgody na pokrycie kosztów dearomatyzacji pokoju w wysokości 500 złotych.
        </p>
        <p>
            <b>3.</b> W schronisku obowiązuje zachowanie ciszy nocnej od godziny 22.00 do 6.00 dnia następnego
        </p>
        <p>
            <b>4.</b> W godzinach ciszy nocnej nie wolno zakłócać spokoju innych gości.
            Obsługa schroniska ma prawo zarekwirować wszelkiego odtwarzacze muzyczne zakłócające ciszę nocną. Urządzenia zostaną zwrócone podczas wymeldowania.
        </p>
        <p>
            <b>5.</b> Gość ponosi pełną odpowiedzialność materialną i prawną za wszelkiego rodzaju
            uszkodzenia lub zniszczenia przedmiotów wyposażenia i urządzeń Schroniska powstałe z winy jego lub odwiedzających go osób.
        </p>
        <p>
            <b>6.</b> Na terenie schroniska zabronione jest używanie: grzałek, żelazek i innych urządzeń elektrycznych.
            Powyższe nie dotyczy ładowarek do urządzeń elektrycznych oraz suszarek do włosów na terenie sanitariatów
        </p>
        <p>
            <b>7.</b> Gość powinien zawiadomić recepcję Schroniska o wystąpieniu szkody niezwłocznie po jej stwierdzeniu.
        </p>
        <p>
            <b>8.</b> Zabrania się nadmiernego hałasowania na terenie Schroniska bez uprzedniego ustalenia z obsługą Schroniska.
        </p>
        <p>
            <b>9.</b> Schronisko zezwala na pozostawienie swoich rzeczy w specjalnie do tego wyznaczonym miejscu, ponadto nie
            bierze żadnej odpowiedzialności za pozostawione w tym miejscu rzeczy.
        </p>
        <p>
            <b>10.</b> W przypadku naruszenia postanowień powyższego regulaminu. Schronisko może odmówić dalszego świadczenia usług osobie, która je narusza.
            Osoba naruszająca powyższy regulamin ma obowiązek zastosowania się do żądań personelu Schroniska oraz do zapłaty za ewentualnie poczynione uszkodzenia i zniszczenia.
        </p>
    </div>
</div>
</body>
</html>
