<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div style="text-align: center">
    <img style=" width: 200px;" src="http://serwer194411.lh.pl/common/images/logo-wielka-racza.png">
</div>

<div style="width: 100%; margin-top: 15px">
    <p><span style="color: red; font-weight: bold;">*</span> Prosimy o nie odpowiadanie na wskazaną wiadomość. Kontaktem mailowym jest: <b>wielka.racza@op.pl</b></p>
    <div style="width: 98%; padding: 1px 5px 20px 5px; border-radius: 5px; text-align: left; color: #000; background-color: #ffdddd; border-color: #ececec;">
        <span style="font-size: 20px; font-weight: 600">Szanowny/a ${fullName}</span><br><br>
        <span>Twoja rezerwacja o ID: <b>${reservationId}</b> została anulowana ponieważ nie odnotowaliśmy płatności w naszym systemie.</span><br>
        <span>Prosimy o ponowne złożenie rezerwacji lub kontakt ze schroniskiem.</span><br>
        <span>Data rezerwacji: <b>${reservationDate}</b></span>
    </div>

    <div style="padding: 10px 5px 20px 5px;">
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">TWÓJ WYBÓR KTÓRY ZOSTAŁ ANULOWANY</span><br>
            <span style="font-size: 16px;">Łączna ilość nocy: <span><b>${amountOfNights}</b></span></span><br><br>
            <#list rooms as current>
                <span>Pokój: <b>${current.titlePL()}</b></span><br>
                <span>Opis: <b>${current.descriptionPL()}</b></span><br>
                <span>Zarezerwowanych miejsc w nim: <b>${current.numberOfPeople()}</b></span><br>
                <span>Numer pokoju: <b>${current.roomNumber()}</b></span><br><br>
            </#list>

        </div>
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">OKRES PODRÓŻY</span><br>
            <span>Data zameldowania: <b>${arrivalDate}</b>&nbsp;<span style="color: gray">(g: 15:00)</span></span><br>
            <span>Data wymeldowania: <b>${departureDate}</b>&nbsp;<span  style="color: gray">(g: 10:00)</span></span><br><br>
        </div>
    </div>
</div>
</body>
</html>
