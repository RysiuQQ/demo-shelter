--liquibase formatted sql
--changeset krys:7 labels:racza-7

CREATE TABLE IF NOT EXISTS reservation_rooms
(
    reservation_id SERIAL REFERENCES reservation,
    room_id SERIAL REFERENCES room,
    PRIMARY KEY (reservation_id, room_id)
);

--rollback DROP TABLE reservation_rooms