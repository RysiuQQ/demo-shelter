--liquibase formatted sql
--changeset krys:3 labels:racza-3

CREATE TABLE report
(
    id              SERIAL PRIMARY KEY,
    uuid            UUID UNIQUE    NOT NULL,
    date            DATE           NOT NULL,
    day_of_week     VARCHAR(50)    NOT NULL,
    cash            NUMERIC(10, 2) NOT NULL,
    cash_per_person NUMERIC(10, 2) NOT NULL
);

--rollback DROP TABLE report;
