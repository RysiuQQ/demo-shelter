CREATE TABLE IF NOT EXISTS room_blocked_date
(
    id                    SERIAL PRIMARY KEY,
    uuid                  UUID UNIQUE NOT NULL,
    date_from             DATE        NOT NULL,
    date_to               DATE        NOT NULL,
    creation_date         TIMESTAMP   NOT NULL,
    room_id               SERIAL REFERENCES room (id)
);

CREATE INDEX IF NOT EXISTS idx_room
    ON "room_blocked_date" (room_id);

--rollback DROP INDEX idx_room
--rollback DROP TABLE room_blocked_date;