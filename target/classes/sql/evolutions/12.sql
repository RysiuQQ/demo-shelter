--liquibase formatted sql
--changeset krys:12 labels:racza-12

ALTER TABLE geolocation DROP COLUMN "offset";

-- rollback ALTER TABLE geolocation ADD COLUMN "offset" INTEGER;

