--liquibase formatted sql
--changeset krys:11 labels:racza-11

CREATE TABLE IF NOT EXISTS geolocation
(
    id             SERIAL PRIMARY KEY,
    uuid           UUID UNIQUE NOT NULL,
    query          VARCHAR(255),
    status         VARCHAR(255),
    continent      VARCHAR(255),
    continent_code VARCHAR(255),
    country        VARCHAR(255),
    country_code   VARCHAR(255),
    region         VARCHAR(255),
    region_name    VARCHAR(255),
    city           VARCHAR(255),
    district       VARCHAR(255),
    zip            VARCHAR(255),
    latitude       DOUBLE PRECISION,
    longitude      DOUBLE PRECISION,
    timezone       VARCHAR(255),
    "offset"       INTEGER,
    currency       VARCHAR(255),
    isp            VARCHAR(255),
    org            VARCHAR(255),
    as_info        VARCHAR(255),
    as_name        VARCHAR(255),
    is_mobile      BOOLEAN,
    is_proxy       BOOLEAN,
    is_hosting     BOOLEAN,
    reservation_id SERIAL references reservation (id)
);

CREATE INDEX IF NOT EXISTS idx_reservation
    ON "geolocation" (reservation_id);

--rollback DROP INDEX idx_reservation
--rollback DROP TABLE geolocation;