--liquibase formatted sql
--changeset krys:13 labels:racza-13

ALTER TABLE reservation_details ADD dogs BOOLEAN NOT NULL DEFAULT false;

--rollback DROP column dogs