--liquibase formatted sql
--changeset krys:9 labels:racza-9

ALTER TABLE room ADD number_of_normal_beds INT NOT NULL DEFAULT 1;
ALTER TABLE room ADD number_of_double_beds INT NOT NULL DEFAULT 1;

--rollback DROP column number_of_double_beds
--rollback DROP column number_of_normal_beds