--liquibase formatted sql
--changeset krys:6 labels:racza-6

CREATE TABLE IF NOT EXISTS unavailable_term
(
    id                    SERIAL PRIMARY KEY,
    uuid                  UUID UNIQUE NOT NULL,
    reservation_day_start DATE        NOT NULL,
    reservation_day_end   DATE        NOT NULL,
    room_id               SERIAL REFERENCES room (id),
    reservation_id        SERIAL REFERENCES reservation (id)
);

CREATE INDEX IF NOT EXISTS idx_room
    ON "unavailable_term" (room_id);

CREATE INDEX IF NOT EXISTS idx_reservation
    ON "unavailable_term" (reservation_id);

--rollback DROP INDEX idx_reservation
--rollback DROP INDEX idx_room
--rollback DROP TABLE unavailable_term
