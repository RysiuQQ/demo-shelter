--liquibase formatted sql
--changeset krys:1 labels:racza-1

CREATE TABLE room
(
    id          SERIAL PRIMARY KEY,
    uuid        UUID UNIQUE    NOT NULL,
    description VARCHAR(255)   NOT NULL,
    max_people  INT            NOT NULL,
    bathroom    BOOLEAN        NOT NULL,
    price       NUMERIC(10, 2) NOT NULL,
    priority    VARCHAR(20)    NOT NULL,
    room_number INT            NOT NULL
);

--rollback DROP TABLE room;
--rollback DROP SEQUENCE hibernate_sequence;