--liquibase formatted sql
--changeset krys:9 labels:racza-9

ALTER TABLE reservation ADD deleted BOOLEAN NOT NULL DEFAULT false;

--rollback DROP columnt deleted

