--liquibase formatted sql
--changeset krys:4 labels:racza-4

CREATE TABLE IF NOT EXISTS reservation
(
    id                         SERIAL PRIMARY KEY,
    uuid                       UUID UNIQUE    NOT NULL,
    first_name                 VARCHAR(50)    NOT NULL,
    last_name                  VARCHAR(50)    NOT NULL,
    people                     INT            NOT NULL,
    reservation_day_start      DATE           NOT NULL,
    reservation_day_end        DATE           NOT NULL,
    date_of_adding_reservation TIMESTAMP      NOT NULL,
    nights                     INT            NOT NULL,
    amount_already_paid        NUMERIC(10, 2) NOT NULL,
    accommodation_paid         BOOLEAN        NOT NULL,
    amount_to_pay              NUMERIC(10, 2) NOT NULL
);

CREATE TABLE IF NOT EXISTS reservation_details
(
    id                SERIAL PRIMARY KEY,
    uuid              UUID UNIQUE    NOT NULL,
    phone_number      VARCHAR(12)  NOT NULL,
    language          VARCHAR(50)  NOT NULL,
    extra_information VARCHAR(255),
    email             VARCHAR(100) NOT NULL,
    reservation_type  VARCHAR(20)  NOT NULL CHECK (reservation_type IN
                                                   ('INTERNET_RESERVATION', 'CALL_RESERVATION',
                                                    'EXPIRED_RESERVATION', 'ACTIVE_RESERVATION',
                                                    'CANCELED_RESERVATION')),
    arrived           BOOLEAN      NOT NULL,
    reservation_id    SERIAL references reservation (id)
);

CREATE INDEX IF NOT EXISTS idx_reservation
    ON "reservation_details" (reservation_id);

--rollback DROP INDEX idx_reservation
--rollback DROP TABLE reservation_details;
--rollback DROP TABLE reservation;
