--liquibase formatted sql
--changeset krys:2 labels:racza-2

CREATE TABLE "user"
(
    id           SERIAL PRIMARY KEY,
    uuid         UUID UNIQUE        NOT NULL,
    username     VARCHAR(50) UNIQUE NOT NULL,
    first_name   VARCHAR(50)        NOT NULL,
    last_name    VARCHAR(50)        NOT NULL,
    password     VARCHAR(120)       NOT NULL,
    notification INT,
    role         VARCHAR(14)        NOT NULL CHECK (role IN ('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'))
);

--rollback DROP TABLE user;
--rollback DROP SEQUENCE hibernate_sequence;