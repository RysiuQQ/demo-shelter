ALTER TABLE reservation
    ADD COLUMN session_id VARCHAR(40);
ALTER TABLE reservation_details
    ADD COLUMN is_accommodation_paid BOOLEAN;
ALTER TABLE room
    ADD COLUMN description_en VARCHAR(255);
ALTER TABLE room
    ADD COLUMN title_pl VARCHAR(255);
ALTER TABLE room
    ADD COLUMN title_en VARCHAR(255);
ALTER TABLE room RENAME COLUMN description TO description_pl;