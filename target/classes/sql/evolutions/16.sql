CREATE TABLE amenities
(
    id          SERIAL PRIMARY KEY,
    uuid        UUID UNIQUE  NOT NULL,
    name        VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    icon        VARCHAR(255) NOT NULL
);

CREATE TABLE rules
(
    id          SERIAL PRIMARY KEY,
    uuid        UUID UNIQUE  NOT NULL,
    name        VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    icon        VARCHAR(255) NOT NULL
);

