--liquibase formatted sql
--changeset krys:5 labels:racza-5

CREATE TABLE IF NOT EXISTS product_room
(
    id               SERIAL PRIMARY KEY,
    uuid             UUID UNIQUE NOT NULL,
    number_of_people INT         NOT NULL,
    room_number      INT         NOT NULL,
    room_id          SERIAL REFERENCES room (id),
    reservation_id   SERIAL REFERENCES reservation (id)
);

CREATE INDEX IF NOT EXISTS idx_room
    ON "product_room" (room_id);

CREATE INDEX IF NOT EXISTS idx_reservation
    ON "product_room" (reservation_id);

--rollback DROP INDEX idx_reservation
--rollback DROP INDEX idx_room
--rollback DROP TABLE product_room