CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO "room"
(id, uuid, title_pl, description_pl, number_of_double_beds, number_of_normal_beds, title_en, description_en, priority, max_people, room_number, bathroom, price)
VALUES
    (1, uuid_generate_v4(), 'Pokój dwuosobowy','Jedna łazienka i jedno podwojne lozko', 1, 1, 'Double room','One bathroom, one king bed','NO', 2, 5, false, 100.00),
    (2, uuid_generate_v4(), 'Pokój trójosobowy','Jedna łazienka i trzy osobne lozka', 1, 1, 'Triple room','One bathroom, three separate beds','NO', 3, 7, false, 120.00),
    (3, uuid_generate_v4(), 'Pokój czteroosobowy','Dwie łazienki i cztery osobne lozka', 1, 1, 'Room for 4 people','Two bathrooms, four separate beds','NO', 4, 10, true, 50.00);