CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO reservation
(id, uuid, first_name, last_name, people, reservation_day_start, reservation_day_end, date_of_adding_reservation,
 nights, amount_already_paid, accommodation_paid, amount_to_pay)
VALUES (1, uuid_generate_v4(), 'Cristian', 'Ronald', 3, '2050-01-01', '2050-01-10',
        '2030-01-01', 9, 0, false, 300);

INSERT INTO reservation_details
(id, uuid, phone_number, language, extra_information, email, reservation_type, arrived, reservation_id)
VALUES (1, uuid_generate_v4(), '500593123', 'PL', 'CYCE', 'ala@paa.gom', 'INTERNET_RESERVATION',
        false, 1);

INSERT INTO product_room
    (id, uuid, number_of_people, room_number, room_id, reservation_id)
VALUES (1, uuid_generate_v4(), 3, 10, 1, 1);

INSERT INTO unavailable_term
(id, uuid, reservation_day_start, reservation_day_end, room_id, reservation_id)
VALUES (1, uuid_generate_v4(), '2050-01-01', '2050-01-10', 1, 1);

INSERT INTO reservation_rooms
    (reservation_id, room_id)
VALUES (1, 1);

INSERT INTO reservation
(id, uuid, first_name, last_name, people, reservation_day_start, reservation_day_end, date_of_adding_reservation,
 nights, amount_already_paid, accommodation_paid, amount_to_pay)
VALUES (2, uuid_generate_v4(), 'Iwona', 'Wieczorek', 3, '2051-01-01', '2052-01-10',
        '2030-02-01', 19, 0, false, 100);

INSERT INTO reservation_details
(id, uuid, phone_number, language, extra_information, email, reservation_type, arrived, reservation_id)
VALUES (2, uuid_generate_v4(), '600321324', 'PL', 'DUPA', 'alpaka@paa.gom', 'CALL_RESERVATION',
        false, 2);

INSERT INTO product_room
(id, uuid, number_of_people, room_number, room_id, reservation_id)
VALUES (2, uuid_generate_v4(), 3, 10, 2, 2);

INSERT INTO unavailable_term
(id, uuid, reservation_day_start, reservation_day_end, room_id, reservation_id)
VALUES (2, uuid_generate_v4(), '2050-01-01', '2050-01-10', 2, 2);

INSERT INTO reservation_rooms
(reservation_id, room_id)
VALUES (2, 2);

INSERT INTO reservation
(id, uuid, first_name, last_name, people, reservation_day_start, reservation_day_end, date_of_adding_reservation,
 nights, amount_already_paid, accommodation_paid, amount_to_pay)
VALUES (3, uuid_generate_v4(), 'Joan', 'Pauleta', 10, '2052-01-01', '2052-03-15',
        '2040-02-01', 11, 0, false, 90);

INSERT INTO reservation_details
(id, uuid, phone_number, language, extra_information, email, reservation_type, arrived, reservation_id)
VALUES (3, uuid_generate_v4(), '532432134', 'PL', 'SEX', 'sexualnaniebezpieczna@paa.gom', 'INTERNET_RESERVATION',
        false, 3);

INSERT INTO product_room
(id, uuid, number_of_people, room_number, room_id, reservation_id)
VALUES (3, uuid_generate_v4(), 3, 10, 2, 3);

INSERT INTO unavailable_term
(id, uuid, reservation_day_start, reservation_day_end, room_id, reservation_id)
VALUES (3, uuid_generate_v4(), '2052-01-01', '2052-03-15', 3, 3);

INSERT INTO reservation_rooms
(reservation_id, room_id)
VALUES (3, 3);