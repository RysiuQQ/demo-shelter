CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO "user"
(id, uuid, first_name, last_name, username, password, notification, role)
VALUES
    (nextval('hibernate_sequence'), uuid_generate_v4(), 'Janek','Dzbanek', 'janko', 'dupa123', 0, 'ROLE_ADMIN'),
    (nextval('hibernate_sequence'), uuid_generate_v4(), 'Marta','Dupodajka', 'martens', 'dupa123', 2, 'ROLE_ADMIN');
