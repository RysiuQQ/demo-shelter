package lynx.code.wielkaracza.user.domain;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.user.UserFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RequiredArgsConstructor
class UserServiceTest {

    @Mock
    private UserQueryRepository userQueryRepository;

    @InjectMocks
    private UserService userService;

    private User user;
    private UUID uuid;


    @BeforeEach
    void init() {
        user = createUser();
        uuid = randomUUID();
    }

    @Nested
    class getMethods {

        @Test
        @DisplayName("Should return all users")
        void getUsersShouldReturnAllUsers() {
            // given
            final var expectedResult = List.of(UserMapper.toDTO(user));
            when(userQueryRepository.findAll()).thenReturn(List.of(user));

            // when
            final var result = userService.getUsers();

            // then
            assertThat(result).isEqualTo(expectedResult);
        }

        @Test
        @DisplayName("Should return user")
        void getUserShouldReturnUserWhenUserExists() {
            // given
            user.setUuid(uuid);
            final var userDTO = UserMapper.toDTO(user);
            when(userQueryRepository.findByUuid(uuid)).thenReturn(Optional.of(user));

            // when
            final var result = userService.getUser(uuid);

            // then
            assertThat(result).isEqualTo(userDTO);
        }
    }

    private User createUser() {
        return User.builder()
                .username(random(10))
                .password(random(10))
                .firstName(random(10))
                .lastName(random(10))
                .build();
    }
}