package lynx.code.wielkaracza.room.domain;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.common.exception.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.UUID.randomUUID;
import static lynx.code.wielkaracza.room.RoomCRUDFacade.ArrivalForm;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomUtils.nextBoolean;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RequiredArgsConstructor
class RoomServiceTest {

    @Mock
    private RoomQueryRepository roomQueryRepository;

    @InjectMocks
    private RoomCRUDService roomService;

    private Room room;
    private UUID uuid;


    @BeforeEach
    void init() {
        room = createRoom();
        uuid = randomUUID();
    }

    @Nested
    class getMethods {

        @Test
        @DisplayName("Should return all rooms")
        void getRoomsShouldReturnAllRooms() {
            // given
            final var expectedResult = List.of(RoomMapper.toDTO(room));
            when(roomQueryRepository.findAll()).thenReturn(List.of(room));

            // when
            final var result = roomService.getRooms();

            // then
            verify(roomQueryRepository, times(1)).findAll();
            assertThat(result).isEqualTo(expectedResult);
        }

        @Test
        @DisplayName("Should return room when the room exists")
        void getRoomWhenRoomExists() {
            // given
            final var expectedResult = RoomMapper.toDTO(room);
            when(roomQueryRepository.findByUuid(any())).thenReturn(Optional.of(room));

            // when
            final var result = roomService.getRoom(randomUUID());

            // then
            verify(roomQueryRepository, times(1)).findByUuid(any());
            assertThat(result).isEqualTo(expectedResult);
        }

        @Test
        @DisplayName("Should throw an exception when the room does not exist")
        void getRoomShouldThrowExceptionWhenRoomNotExist() {
            //given
            when(roomQueryRepository.findByUuid(uuid)).thenReturn(Optional.empty());

            //when and then
            assertThatThrownBy(() -> roomService.getRoom(uuid))
                    .isInstanceOf(NotFoundException.class)
                    .hasMessage(String.format(RoomCRUDService.ErrorMessages.ROOM_DOES_NOT_EXIST, uuid));
        }
    }

    @Nested
    class patchMethods {

        @Test
        @DisplayName("Should change the price of the room")
        void changeRoomPriceWhenRoomExistsThenChangeThePriceOfTheRoom() {
            final var price = BigDecimal.valueOf(nextInt(0, 100));
            when(roomQueryRepository.findByUuid(uuid)).thenReturn(Optional.of(room));
            roomService.changeRoomPrice(uuid, price);
            verify(roomQueryRepository, times(1)).findByUuid(uuid);
            assertThat(room.getPrice()).isEqualTo(price);
        }
    }

    private Room createRoom() {
        return Room.builder()
                .descriptionPL(randomAlphanumeric(10))
                .descriptionEN(randomAlphanumeric(10))
                .titlePL(randomAlphanumeric(10))
                .titleEN(randomAlphanumeric(10))
                .maxPeople(nextInt(0, 100))
                .roomNumber(nextInt(0, 100))
                .bathroom(nextBoolean())
                .price(BigDecimal.valueOf(nextInt(0, 100)))
                .build();
    }

    private ArrivalForm createArrivalForm() {
        return ArrivalForm.builder()
                .reservationDayStart(LocalDate.of(2023, nextInt(1, 12), nextInt(1, 28)))
                .reservationDayEnd(LocalDate.of(2023, nextInt(1, 12), nextInt(1, 28)))
                .numberOfPeople(nextInt(0, 100))
                .build();
    }
}