<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div style="text-align: center">
    <img style=" width: 200px;" src="http://serwer194411.lh.pl/common/images/logo-wielka-racza.png">
</div>

<div style="width: 100%; margin-top: 15px">
    <p><span style="color: red; font-weight: bold;">*</span> Prosím, neodpovedajte na uvedenú správu. Emailový kontakt je: <b>wielka.racza@op.pl</b></p>
    <div style="width: 98%; padding: 1px 5px 20px 5px; border-radius: 5px; text-align: left; color: #000; background-color: #ffdddd; border-color: #ececec;">
        <span style="font-size: 20px; font-weight: 600">Vážená Pani/vážený Pán ${fullName}</span><br><br>
        <span>Tvoja rezervácia s ID: <b>${reservationId}</b> bola zrušená, pretože sme nezaznamenali platbu v našom systéme.</span><br>
        <span>Prosím, založte si novú rezerváciu alebo nás kontaktujte na útulku.</span><br>
        <span>Dátum rezervácie: <b>${reservationDate}</b></span>
    </div>

    <div style="padding: 10px 5px 20px 5px;">
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">VÁŠ ZRUŠENÝ VÝBER</span><br>
            <span style="font-size: 16px;">Celkový počet nocí: <span><b>${amountOfNights}</b></span></span><br><br>
            <#list rooms as current>
                <span>Izba: <b>${current.titleSK()}</b></span><br>
                <span>Popis: <b>${current.descriptionSK()}</b></span><br>
                <span>Vyhradené miesta v ňom: <b>${current.numberOfPeople()}</b></span><br>
                <span>Číslo izby: <b>${current.roomNumber()}</b></span><br><br>
            </#list>

        </div>
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">CESTOVNÉ OBDOBIE</span><br>
            <span>Dátum príchodu: <b>${arrivalDate}</b>&nbsp;<span style="color: gray">(čas: 15:00)</span></span><br>
            <span>Dátum odhlásenia: <b>${departureDate}</b>&nbsp;<span  style="color: gray">(čas: 10:00)</span></span><br><br>
        </div>
    </div>
</div>
</body>
</html>
