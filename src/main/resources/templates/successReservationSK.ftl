<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div style="text-align: center">
    <img style=" width: 200px;" src="http://serwer194411.lh.pl/common/images/logo-wielka-racza.png">
</div>

<div style="width: 100%; margin-top: 15px">
    <p><span style="color: red; font-weight: bold;">*</span> Prosím, neodpovedajte na uvedenú správu. Emailový kontakt je: <b>wielka.racza@op.pl</b></p>
    <div style="width: 98%; padding: 1px 5px 20px 5px; border-radius: 5px; text-align: left; color: #155724; background-color: #d4edda; border-color: #c3e6cb;">
        <span style="font-size: 20px; font-weight: 600">Vážená Pani/vážený Pán ${fullName}</span><br><br>
        <span>Gratulujeme k rezervácii izby. Dúfame, že sa vám bude páčiť.</span><br>
        <span>Dátum rezervácie: <b>${reservationDate}</b></span>
        <span>Id rezervácie: <b>${reservationId}</b></span>
    </div>

    <div style="padding: 10px 5px 20px 5px;">
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">TVOJA VOĽBA</span><br>
            <span style="font-size: 16px;">Celkový počet nocí: <span><b>${amountOfNights}</b></span></span><br><br>
            <#list rooms as current>
                <span>Izba: <b>${current.titleSK()}</b></span><br>
                <span>Popis: <b>${current.descriptionSK()}</b></span><br>
                <span>Vyhradené miesta v ňom: <b>${current.numberOfPeople()}</b></span><br>
                <span>Číslo izby: <b>${current.roomNumber()}</b></span><br><br>
            </#list>

        </div>
        <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
            <span style="font-size: 18px; font-weight: 700">CESTOVNÉ OBDOBIE</span><br>
            <span>Dátum príchodu: <b>${arrivalDate}</b>&nbsp;<span style="color: gray">(čas: 15:00)</span></span><br>
            <span>Dátum odhlásenia: <b>${departureDate}</b>&nbsp;<span  style="color: gray">(čas: 10:00)</span></span><br><br>
        </div>
        <div>
            <span style="font-size: 18px; font-weight: 700">PLATBY</span><br>
            <span>Celková suma, ktorá sa má zaplatiť: <b>${amountToPay} PLN</b></span><br>
            <span>Záloha sa platí: <b>${deposit} PLN</b></span><br>
        </div>
    </div>


    <div style="width: 100%; text-align: left;">
        <h4 style="color: red">Podmienky horskej chaty PTTK Wielka Racza</h4>
        <h5 style="color: darkred">Ďakujeme za rezerváciu v útulku Wielka Racza, nižšie sú dôležité podmienky rezervácie a informácie: </h5>
        <p><b>1.</b> V tomto poriadku sú uvedené zásady poskytovania služieb, zodpovednosti a pobytu v chate.
            Za neoddeliteľnú súčasť zmluvy sa považuje registrácia, ako aj rezervácia alebo platba zálohy. Týmto hosť potvrdzuje,
            že sa oboznámil s obsahom týchto pravidiel a súhlasí s ich podmienkami.
        </p>
        <p>
            <b>2.</b> Tento prevádzkový poriadok platí pre všetkých hostí ubytovaných v turistickej ubytovni PTTK Wielka Racza.
        </p>
        <p>
            <b>3.</b> Hosť turistického  horskiej chaty Wielka Racza je povinný pri registrácii predložiť recepčnému pracovníkovi doklad totožnosti s fotografiou.
            V prípade odmietnutia predložiť doklad je recepčný povinný odmietnuť vydať kľúč od izby.
        </p>
        <p>
            <b>4.</b> Registrácia spočíva v zápise základných registračných údajov do registračnej knihy na recepcii, ktorý vykonáva recepčná.
            Izba v hosteli sa prenajíma na celý deň. Hotelový deň trvá od <b>15:00</b> v deň prenájmu do <b>10:00</b> v
            deň odhlásenia. Dĺžka pobytu sa predpokladá na základe rezervácie alebo preferencií hosťa, inak sa predpokladá, že izba bola prenajatá na jednu noc.
        </p>
        <p>
            <b>5.</b> Ponechanie vecí po 10:00 bez predchádzajúceho upozornenia oprávňuje personál na ich premiestnenie do batožinového
            priestoru bez súhlasu hosťa. Ak hosť zostane v izbe po skončení prenocovania, bude mu účtovaný poplatok za ďalšie prenocovanie.
        </p>
        <p>
            <b>6.</b> Ak hosť zostane v izbe po skončení prenocovania, bude mu účtovaný poplatok za ďalšie prenocovanie.
        </p>
        <p>
            <b>7.</b> Žiadosť o predĺženie pobytu je možné podať najneskôr do zatvorenia recepcie v deň pred odhlásením.
            Hostel môže zamietnuť žiadosti o predĺženie pobytu z dôvodu vyčerpania všetkých lôžok alebo v prípade hostí, ktorí nedodržiavajú platné pravidlá.
        </p>
        <p>
            <b>8.</b> Hosť, ktorý si prenajíma izbu, ju nesmie prenajať iným osobám.
        </p>
        <p>
            <b>9.</b> Neregistrované osoby môžu zostať v areáli do 22.00 h. Pobyt neregistrovaných osôb po 22.00 h sa rovná ich súhlasu
            s ubytovaním v najlacnejšom možnom ubytovacom zariadení v daný deň. V prípade nedostatočnej kapacity sa účtuje poplatok 30 PLN
            na osobu. To zodpovedá miestu na podlahe.
        </p>
        <p>
            <b>10.</b> Deti do 5 rokov majú pobyt v detskej postieľke s opatrovníkom zdarma.
            V prípade manželských postelí sa účtuje poplatok Za prípadné škody spôsobené deťmi zodpovedajú zákonní zástupcovia.
        </p>
        <p>
            <b>11.</b> Chatu si môžete za určitých podmienok rezervovať aj so psom:
        <ul>
            <li>Klient si rezervuje celú izbu, nie je možné rezervovať izbu, ktorá je čiastočne obsadená inými osobami.</li>
            <li>Klient preberá plnú zodpovednosť za akékoľvek škody spôsobené psom v celej budove.</li>
            <li>Klient platí ďalšie poplatky uvedené v cenníku na webovej stránke Horskej chaty ubytovne PTTK Wielka Racza.</li>
        </ul>
        </p>
        <p>
            <b>1.</b> Rezervácia vykonaná klientom je platná do 19:30 v deň príchodu; po prekročení tohto času bude rezervácia zrušená.
        </p>
        <p>
            <b>2.</b> V priestoroch ubytovne, vrátane - v izbách ubytovne - v súlade so zákonom z 8. apríla 2010, ktorým sa mení a dopĺňa
            zákon o ochrane zdravia pred následkami užívania tabaku a tabakových výrobkov a zákon o Štátnej hygienickej inšpekcii
            (Zbierka zákonov č. 81, položka 529), platí úplný zákaz fajčenia cigariet a tabakových výrobkov, zákaz zahŕňa aj používanie
            elektronických cigariet. Porušenie vyššie uvedeného zákazu sa rovná súhlasu nájomcu s úhradou nákladov na odmodernizovanie miestnosti vo výške 500 PLN.
        </p>
        <p>
            <b>3.</b> V ubytovni platí zákaz vychádzania od 22.00 do 6.00 nasledujúceho dňa.
        </p>
        <p>
            <b>4.</b> Počas zákazu vychádzania nesmú byť ostatní hostia rušení.
            Personál  horskiej chata má právo zabaviť všetky hudobné prehrávače, ktoré rušia nočný pokoj. Zariadenia sa vrátia pri odhlásení.
        </p>
        <p>
            <b>5.</b> Hosť nesie plnú finančnú a právnu zodpovednosť za akékoľvek poškodenie alebo zničenie zariadenia a vybavenia
            horskiej chata spôsobené ním alebo jeho návštevníkmi
        </p>
        <p>
            <b>6.</b> V priestoroch  horskiej chata je zakázané používať: ohrievače,
            žehličky a iné elektrické spotrebiče. Uvedené sa nevzťahuje na nabíjačky elektrických spotrebičov a sušiče vlasov v hygienických zariadeniach.
        </p>
        <p>
            <b>7.</b> Hostia by mali oznámiť recepcii  horskiej chata akékoľvek poškodenie hneď, ako ho zistia.
        </p>
        <p>
            <b>8.</b> V priestoroch  horskiej chata je zakázané robiť nadmerný hluk bez predchádzajúcej dohody s personálom  horskiej chata.
        </p>
        <p>
            <b>9.</b> Hostel umožňuje hosťom ponechať si svoje veci na špeciálne vyhradenom mieste, okrem toho hostel nenesie žiadnu zodpovednosť za veci, ktoré tam zostali.
        </p>
        <p>
            <b>10.</b> V prípade porušenia vyššie uvedených predpisov. Ubytovňa môže odmietnuť poskytnúť osobe, ktorá sa dopustila priestupku, ďalšie služby. Osoba,
            ktorá poruší vyššie uvedené predpisy, je povinná vyhovieť požiadavkám personálu ubytovne a uhradiť prípadnú škodu alebo zničenie
        </p>
    </div>
</div>
</body>
</html>
