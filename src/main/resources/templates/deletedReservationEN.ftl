<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div style="text-align: center">
    <img style=" width: 200px;" src="http://serwer194411.lh.pl/common/images/logo-wielka-racza.png">
</div>

    <div style="width: 100%; margin-top: 15px">
        <p><span style="color: red; font-weight: bold;">*</span> Please do not respond for this message. If you want to contact write to: <b>wielka.racza@op.pl</b></p>
        <div style="width: 98%; padding: 1px 5px 20px 5px; border-radius: 5px; text-align: left; color: #000; background-color: #ffdddd; border-color: #ececec;">
            <span style="font-size: 20px; font-weight: 600">Dear ${fullName}</span><br><br>
            <span>Your reservation with ID: <b>${reservationId}</b> has been canceled because we have not received any payment in our system.</span><br>
            <span>Please kindly resubmit your reservation or contact with the shelter.</span><br>
            <span>Reservation date: <b>${reservationDate}</b></span>
        </div>

        <div style="padding: 10px 5px 20px 5px;">
            <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
                <span style="font-size: 18px; font-weight: 700">YOUR CHOICE WHICH HAS BEEN CANCELED</span><br>
                <span style="font-size: 16px;">The total number of nights: <span><b>${amountOfNights}</b></span></span><br><br>
                <#list rooms as current>
                    <span>Room: <b>${current.titleEN()}</b></span><br>
                    <span>Description: <b>${current.descriptionEN()}</b></span><br>
                    <span>Reserved beds in it: <b>${current.numberOfPeople()}</b></span><br>
                    <span>Room number: <b>${current.roomNumber()}</b></span><br><br>
                </#list>

            </div>
            <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
                <span style="font-size: 18px; font-weight: 700">TRAVEL PERIOD</span><br>
                <span>Check-in date: <b>${arrivalDate}</b>&nbsp;<span style="color: gray">(g: 15:00)</span></span><br>
                <span>Check-out date: <b>${departureDate}</b>&nbsp;<span  style="color: gray">(g: 10:00)</span></span><br><br>
            </div>
        </div>
    </div>
</body>
</html>
