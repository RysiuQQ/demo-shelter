<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div style="text-align: center">
    <img style=" width: 200px;" src="http://serwer194411.lh.pl/common/images/logo-wielka-racza.png">
</div>

    <div style="width: 100%; margin-top: 15px">
        <p><span style="color: red; font-weight: bold;">*</span> Please do not respond for this message. If you want to contact write to: <b>wielka.racza@op.pl</b></p>
        <div style="width: 98%; padding: 1px 5px 20px 5px; border-radius: 5px; text-align: left; color: #155724; background-color: #d4edda; border-color: #c3e6cb;">
            <span style="font-size: 20px; font-weight: 600">Dear ${fullName}</span><br><br>
            <span>Congratulations on your reservation. We hope that it will be a pleasant stay for you.</span><br>
            <span>Reservation date: <b>${reservationDate}</b></span>
            <span>Reservation ID: <b>${reservationId}</b></span>
        </div>

        <div style="padding: 10px 5px 20px 5px;">
            <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
                <span style="font-size: 18px; font-weight: 700">YOUR CHOICE</span><br>
                <span style="font-size: 16px;">The total number of nights: <span><b>${amountOfNights}</b></span></span><br><br>
                <#list rooms as current>
                    <span>Room: <b>${current.titleEN()}</b></span><br>
                    <span>Description: <b>${current.descriptionEN()}</b></span><br>
                    <span>Reserved beds in it: <b>${current.numberOfPeople()}</b></span><br>
                    <span>Room number: <b>${current.roomNumber()}</b></span><br><br>
                </#list>

            </div>
            <div style="border-bottom: 1px solid lightgray; margin-bottom: 15px;">
                <span style="font-size: 18px; font-weight: 700">TRAVEL PERIOD</span><br>
                <span>Check-in date: <b>${arrivalDate}</b>&nbsp;<span style="color: gray">(g: 15:00)</span></span><br>
                <span>Check-out date: <b>${departureDate}</b>&nbsp;<span  style="color: gray">(g: 10:00)</span></span><br><br>
            </div>
            <div>
                <span style="font-size: 18px; font-weight: 700">PAYMENTS</span><br>
                <span>Total amount to pay: <b>${amountToPay} PLN</b></span><br>
                <span>Half of the total amount (deposit): <b>${deposit} PLN</b></span><br>
            </div>
        </div>


        <div style="width: 100%; text-align: left;">
            <h4 style="color: red">PTTK Mountain Shelter Terms and Conditions Wielka Racza</h4>
            <h5 style="color: darkred">Thank you for making reservation in our shelter, we attached our rules below which are important during your stay at Wielka Racza Shelter.</h5>
            <p><b>1.</b> The Regulations specify the rules for the provision of services, liability and stay at the mountain shelter.
                Checking in as well as making a reservation or paying a deposit is considered an integral part of the contract.
                By performing the above actions, the Guest confirms that he/she has read and accepts the terms of the Regulations.
            </p>
            <p>
                <b>2.</b> The Rules and Regulations apply to all Guests staying at PTTK Great Racza mountain mountain shelter.
            </p>
            <p>
                <b>3.</b> The Guest of the mountain shelter is obliged to show to the Reception employee a document with a photo
                confirming identity during check-in. In case of refusal to show the document, the receptionist is obliged to refuse to issue a key to the room.
            </p>
            <p>
                <b>4.</b> Check-in takes place on the basis of writing down the basic registration data in the registration book at the reception
                area by the receptionist. A room in the mountain shelter is rented for a day. A hotel day lasts from <b>15:00</b> on the day of rental until <b>10:00</b>
                on the day of check-out. The check-in period is taken on the basis of the guest's reservation or preference, otherwise it is assumed that the room was rented for one night.
            </p>
            <p>
                <b>5.</b> Leaving belongings after 10:00 a.m. without prior information, entitles the staff to transfer these belongings to the
                luggage room without the guest's consent. If a guest remains in the room after the end of the overnight stay, a charge will be made for the next night's stay.
            </p>
            <p>
                <b>6.</b> If a guest remains in the room after the end of the overnight stay, a charge will be made for the next overnight stay.
            </p>
            <p>
                <b>7.</b> A request for an extension of the night can be made no later than the closing of the reception
                before the day of check-out. The mountain shelter may disregard the request to extend the stay due to the use of
                all beds or in the case of guests who do not comply with the rules in force.
            </p>
            <p>
                <b>8.</b> Guest renting a room, may not transfer the room to other persons.
            </p>
            <p>
                <b>9.</b> Non-registered guests are allowed to stay on the premises until 10:00 p.m.
                Stay of non-registered guests after 10:00 p.m. is tantamount to their agreement to make accommodation in the cheapest possible accommodation
                option for the indicated day. In case of lack of places there is a charge of 30 PLN/person. Corresponding to the place on the floor.
            </p>
            <p>
                <b>10.</b> Children under 5 years of age stay free of charge with a guardian on the bed.
                In the case of a separate bed, a fee will be charged For any damage caused by children, legal guardians are responsible.
            </p>
            <p>
                <b>11.</b> At the mountain shelter it is possible to make a reservation with a dog, under certain conditions:
            <ul>
                <li>The client makes a reservation for the entire room, it is not possible to make a reservation in a room partially occupied by other people.</li>
                <li>The client takes full responsibility for any damage caused by the dog throughout the building.</li>
                <li>The client takes full responsibility for any damage caused by the dog throughout the building.</li>
            </ul>
            </p>
            <p>
                <b>1.</b> The client takes full responsibility for any damage caused by the dog throughout the building.
            </p>
            <p>
                <b>2.</b> On the premises of the mountain shelter, including - in the rooms of the mountain shelter
                in accordance with the Act of April 8, 2010 on amending the Act on Health Protection
                against the Consequences of the Use of Tobacco and Tobacco Products and the Act on State Sanitary
                Inspection (Journal of Laws No. 81, item 529) - there is a complete ban on smoking cigarettes and tobacco products,
                the ban includes the use of e-cigarettes. Violation of the above prohibition is tantamount to the hirer's agreement
                to cover the cost of dearomatization of the room in the amount of PLN 500.
            </p>
            <p>
                <b>3.</b> The mountain shelter is obliged to maintain curfew from 10 pm to 6 am the next day.
            </p>
            <p>
                <b>4.</b> During the hours of curfew it is not allowed to disturb other guests. mountain shelter staff has the right to
                requisition any music players disturbing the quiet of the night. Devices will be returned upon check-out.
            </p>
            <p>
                <b>5.</b> The guest bears full financial and legal responsibility for
                any kind of damage or destruction of items of equipment and devices of the mountain shelter caused by him or her or visitors.
            </p>
            <p>
                <b>6.</b> The use of: heaters, irons and other electrical appliances is prohibited on the mountain shelter premises.
                The above does not apply to chargers for electrical appliances and hair dryers in the sanitary facilities.
            </p>
            <p>
                <b>7.</b> Guests should notify the reception of the mountain shelter of the occurrence of damage immediately upon discovery.
            </p>
            <p>
                <b>8.</b> It is forbidden to make excessive noise in the mountain shelter area without prior arrangement with the mountain shelter staff.
            </p>
            <p>
                <b>9.</b> The mountain shelter allows you to leave your belongings in a specially designated place, moreover,
                the mountain shelter does not take any responsibility for the things left in this place.
            </p>
            <p>
                <b>10.</b> In case of violation of the above regulations. The mountain shelter may refuse to provide further
                services to the person who violates them. A person who violates the above rules and regulations is
                obliged to comply with the demands of the mountain shelter staff and to pay for any damage or destruction done.
            </p>
        </div>
    </div>
</body>
</html>
