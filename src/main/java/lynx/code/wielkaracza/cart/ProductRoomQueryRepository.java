package lynx.code.wielkaracza.cart;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRoomQueryRepository extends UuidAwareJpaQueryRepository<ProductRoom, Long> {

}
