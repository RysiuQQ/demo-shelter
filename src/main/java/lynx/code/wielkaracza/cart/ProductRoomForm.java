package lynx.code.wielkaracza.cart;

import java.util.UUID;

public record ProductRoomForm(int numberOfPeople,
                              int roomNumber,
                              UUID uuid) {
}