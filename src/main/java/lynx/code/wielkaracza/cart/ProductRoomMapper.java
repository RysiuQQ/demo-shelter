package lynx.code.wielkaracza.cart;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.room.domain.Room;

@UtilityClass
public class ProductRoomMapper {

    public ProductRoom toEntity(final ProductRoomForm productRoomForm, final Room room) {
        return ProductRoom.builder()
                .roomNumber(productRoomForm.roomNumber())
                .numberOfPeople(productRoomForm.numberOfPeople())
                .room(room)
                .build();
    }
}