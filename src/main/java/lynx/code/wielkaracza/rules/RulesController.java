package lynx.code.wielkaracza.rules;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.rules.RulesFacade.RulesDTO;

@RestController
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class RulesController {

    private final RulesFacade rulesFacade;

    @GetMapping("/api/rules")
    public List<RulesDTO> getRules() {
        return rulesFacade.getAllRules();
    }

    @GetMapping("/api/rules/{uuid}")
    public RulesDTO getAmenity(@PathVariable final UUID uuid) {
        return rulesFacade.getRule(uuid);
    }

    @PostMapping("/api/rules")
    public void createRules(@RequestBody final RulesDTO rulesDTO) {
        rulesFacade.createRule(rulesDTO);
    }

    @PutMapping("/api/rules/{uuid}")
    public void updateRule(@PathVariable final UUID uuid, @RequestBody final RulesDTO rulesDTO) {
        rulesFacade.updateRule(uuid, rulesDTO);
    }

    @DeleteMapping("/api/rules/{uuid}")
    public void deleteRule(@PathVariable final UUID uuid) {
        rulesFacade.deleteRule(uuid);
    }
}
