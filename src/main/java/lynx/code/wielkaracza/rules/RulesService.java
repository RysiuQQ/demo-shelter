package lynx.code.wielkaracza.rules;

import lombok.AllArgsConstructor;
import lynx.code.wielkaracza.common.exception.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class RulesService implements RulesFacade {

    private final RulesRepository rulesRepository;
    private final RulesQueryRepository rulesQueryRepository;

    @Override
    public List<RulesDTO> getAllRules() {
        return rulesQueryRepository.findAll().stream()
                .map(rules -> new RulesDTO(rules.getName(), rules.getDescription(), rules.getIcon()))
                .toList();
    }

    @Override
    public RulesDTO getRule(final UUID uuid) {
        return rulesQueryRepository.findByUuid(uuid)
                .map(rules -> new RulesDTO(rules.getName(), rules.getDescription(), rules.getIcon()))
                .orElseThrow(() -> new RuntimeException("Rule not found"));
    }

    @Override
    public void createRule(final RulesDTO rulesDTO) {
        rulesRepository.save(new Rules(rulesDTO.name(), rulesDTO.description(), rulesDTO.icon()));
    }

    @Override
    @Transactional
    public void updateRule(UUID uuid, RulesDTO rulesDTO) {
        rulesQueryRepository.findByUuid(uuid)
                .ifPresentOrElse(rules -> {
                    rules.setName(rulesDTO.name());
                    rules.setDescription(rulesDTO.description());
                    rules.setIcon(rulesDTO.icon());
                    rulesRepository.save(rules);
                }, () -> {
                    throw new NotFoundException("Rule not found");
                });
    }

    @Override
    @Transactional
    public void deleteRule(UUID uuid) {
        rulesRepository.deleteByUuid(uuid);
    }
}
