package lynx.code.wielkaracza.rules;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "rules")
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class Rules extends AbstractUUIDEntity {
    private String name;
    private String description;
    private String icon;
}
