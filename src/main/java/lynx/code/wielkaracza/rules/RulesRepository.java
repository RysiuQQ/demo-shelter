package lynx.code.wielkaracza.rules;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaRepository;

public interface RulesRepository extends UuidAwareJpaRepository<Rules, Long> {
}
