package lynx.code.wielkaracza.rules;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;

public interface RulesQueryRepository extends UuidAwareJpaQueryRepository<Rules, Long> {
}
