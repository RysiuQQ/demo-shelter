package lynx.code.wielkaracza.rules;

import java.util.List;
import java.util.UUID;

public interface RulesFacade {

        List<RulesDTO> getAllRules();

        RulesDTO getRule(final UUID uuid);

        void createRule(final RulesDTO rulesDTO);

        void updateRule(final UUID uuid, final RulesDTO rulesDTO);

        void deleteRule(final UUID uuid);

        record RulesDTO(String name, String description, String icon) {
        }
}
