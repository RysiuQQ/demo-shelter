package lynx.code.wielkaracza.reservation;

import lynx.code.wielkaracza.reservation.domain.ReservationType;
import lynx.code.wielkaracza.reservation.domain.ReservationWithPaginationDTO;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

public interface ReservationSearchingFacade {

    ReservationWithPaginationDTO getReservationBySearchParameters(final Integer page,
                                                                  final Integer size,
                                                                  final ReservationSearchParametersDTO reservationSearchParametersDTO);

    int getPeopleByMonth(final ReservationMonthAndYearForm reservationMonthAndYearForm);

    double getIncomeByMonth(final ReservationMonthAndYearForm reservationMonthAndYearForm);

    record ReservationSearchParametersDTO(@Size(max = 50)
                                          String firstName,

                                          @Size(max = 50)
                                          String lastName,

                                          @DateTimeFormat(pattern = "yyyy-MM-dd")
                                          LocalDate reservationDayStart,

                                          @DateTimeFormat(pattern = "yyyy-MM-dd")
                                          LocalDate reservationDayEnd,

                                          @DateTimeFormat(pattern = "yyyy-MM-dd")
                                          LocalDate dateOfAddingReservation,

                                          @Size(max = 12)
                                          String phoneNumber,

                                          @Size(max = 100)
                                          String email,

                                          Boolean deleted,

                                          ReservationType reservationType,

                                          Boolean isAccommodationPaid) {

    }

    record ReservationMonthAndYearForm(@NotNull
                                       Month month,
                                       @NotNull
                                       Year year) {

    }
}
