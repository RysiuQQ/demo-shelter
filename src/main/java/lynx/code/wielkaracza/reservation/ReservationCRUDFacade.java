package lynx.code.wielkaracza.reservation;

import lombok.Builder;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.cart.ProductRoomForm;
import lynx.code.wielkaracza.common.enums.Language;
import lynx.code.wielkaracza.common.validation.SelfValidator.SelfValidation;
import lynx.code.wielkaracza.payments.dto.PaymentsWrapperDto;
import lynx.code.wielkaracza.reservation.domain.ReservationType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static lynx.code.wielkaracza.common.validation.SelfValidator.SelfValidate;
import static lynx.code.wielkaracza.room.RoomCRUDFacade.ProductRoomDTO;
import static org.apache.commons.lang3.BooleanUtils.isFalse;

public interface ReservationCRUDFacade {

    @UtilityClass
    final class ValidationRules {
        private static final String DAY_START_NOT_BEFORE_DAY_END =
                "Reservation day start must be before reservation day end";
        private static final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        private static final String WRONG_LANGUAGE = "Wrong language - possible values PL / EN / SK";
    }


    List<ReservationDTO> getReservations();

    ReservationDTO getReservation(final UUID uuid);

    PaymentsWrapperDto createReservation(final ReservationForm reservationForm, final HttpServletRequest ipAddress);

    ReservationDTO createAdminReservation(final ReservationForm reservationForm);

    void editReservation(final UUID uuid, final ReservationForm reservationForm);

    void deleteReservation(final UUID uuid);

    void changeArrivedStatus(final UUID uuid);

    void changePaymentAmount(final UUID uuid, final BigDecimal amount);

    void confirmPayment(final UUID uuid);

    @Builder
    @SelfValidation
    record ReservationForm(@Size(min = 2, max = 50)
                           @NotBlank(message = "first name is required")
                           String firstName,

                           @Size(min = 2, max = 50)
                           @NotBlank(message = "last name is required")
                           String lastName,

                           @Min(value = 1, message = "number of guests should not be less than 1")
                           @Max(value = 76, message = "number of guests should not be greater than 105")
                           int people,

                           @DateTimeFormat(pattern = "yyyy-MM-dd")
                           @NotNull(message = "day of start reservation is required")
                           @FutureOrPresent(message = "date must be at least current-day")
                           LocalDate reservationDayStart,

                           @DateTimeFormat(pattern = "yyyy-MM-dd")
                           @NotNull(message = "day of end reservation is required")
                           @FutureOrPresent(message = "date must be at least current-day")
                           LocalDate reservationDayEnd,

                           @NotBlank(message = "phone number is required")
                           @Size(min = 9, max = 12)
                           String phoneNumber,

                           String extraInformation,

                           boolean dogs,

                           @Email(message = "correct email address is required", regexp = ValidationRules.EMAIL_PATTERN)
                           String email,

                           @NotNull(message = "language is required, possible values PL / EN / SK")
                           String language,

                           @NotEmpty(message = "you need have at least 1 room in cart")
                           Set<ProductRoomForm> rooms) implements SelfValidate {
        @Override
        public boolean validate(final ConstraintValidatorContext context) {
            boolean isValid = true;

            if (isFalse(reservationDayStart.isBefore(reservationDayEnd))) {
                context.buildConstraintViolationWithTemplate
                        (ValidationRules.DAY_START_NOT_BEFORE_DAY_END).addConstraintViolation();

                isValid = false;
            }

            final var includesLanguage = EnumSet.allOf(Language.class).stream()
                    .anyMatch(languageEnum -> languageEnum.toString().equals(language));

            if (isFalse(includesLanguage)) {
                context.buildConstraintViolationWithTemplate
                        (ValidationRules.WRONG_LANGUAGE).addConstraintViolation();

                isValid = false;
            }


            return isValid;
        }
    }

    @Builder
    record ReservationDTO(UUID uuid,
                          String firstName,
                          String lastName,
                          int people,
                          LocalDate reservationDayStart,
                          boolean dogs,
                          LocalDate reservationDayEnd,
                          LocalDateTime dateOfAddingReservation,
                          int nights,
                          BigDecimal amountAlreadyPaid,
                          BigDecimal amountToPay,
                          boolean accommodationPaid,
                          List<Integer> roomNumbers,
                          String phoneNumber,
                          String extraInformation,
                          String email,
                          ReservationType reservationType,
                          List<ProductRoomDTO> productRoomsDTO,
                          Language language,
                          boolean deleted,
                          boolean arrived) {
    }
}