package lynx.code.wielkaracza.reservation.controller;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.date.DateDTO;
import lynx.code.wielkaracza.reservation.ReservationOverviewFacade;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.List;

import static lynx.code.wielkaracza.reservation.ReservationOverviewFacade.ReservationBasicInfo;
import static lynx.code.wielkaracza.reservation.ReservationOverviewFacade.ReservationGuestDTO;
import static lynx.code.wielkaracza.reservation.ReservationOverviewFacade.ReservationOverviewDTO;
import static lynx.code.wielkaracza.reservation.controller.ReservationOverview.Routes.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin(originPatterns = "*")
class ReservationOverview {

    @UtilityClass
    static final class Routes {
        static final String ROOT = "/api/reservations";
        static final String OVERVIEW = ROOT + "/overview";
        static final String COMING = ROOT + "/comings";
        static final String TODAY = ROOT + "/today";
        static final String GUESTS = ROOT + "/guests";
        static final String ONGOING = ROOT + "/ongoing";
    }

    private final ReservationOverviewFacade reservationOverviewFacade;

    @PostMapping(OVERVIEW)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ReservationOverviewDTO getReservationsOverview(@Valid @RequestBody final DateDTO providedDate) {
        return reservationOverviewFacade.getReservationOverview(providedDate);
    }

    @PostMapping(COMING)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    List<ReservationBasicInfo> getComingReservations(@Valid @RequestBody final DateDTO providedDate) {
        return reservationOverviewFacade.getReservationComings(providedDate);
    }

    @PostMapping(TODAY)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    List<ReservationBasicInfo> getTodayReservations(@Valid @RequestBody final DateDTO providedDate) {
        return reservationOverviewFacade.getReservationForToday(providedDate);
    }

    @PostMapping(ONGOING)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    List<ReservationBasicInfo> getOngoingReservations(@Valid @RequestBody final DateDTO providedDate) {
        return reservationOverviewFacade.getOngoingReservations(providedDate);
    }

    @PostMapping(GUESTS)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    List<ReservationGuestDTO> getCurrentGuests(@Valid @RequestBody final DateDTO providedDate) {
        return reservationOverviewFacade.getCurrentGuests(providedDate);
    }
}
