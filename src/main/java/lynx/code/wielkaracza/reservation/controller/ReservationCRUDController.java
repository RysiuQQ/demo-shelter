package lynx.code.wielkaracza.reservation.controller;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.payments.dto.PaymentsWrapperDto;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade.ReservationDTO;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade.ReservationForm;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.reservation.controller.ReservationCRUDController.Routes.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin(originPatterns = "*")
class ReservationCRUDController {

    @UtilityClass
    static final class Routes {
        static final String ROOT = "/api/reservations";
        static final String ADMIN_RESERVATION = "/api/reservations/admin";
        static final String SINGLE_RESERVATION = ROOT + "/{uuid}";
        static final String ARRIVED_STATUS = ROOT + "/arrivedStatus/{uuid}";
        static final String PAYMENT_AMOUNT = ROOT + "/payment/amount/{uuid}";
        static final String PAYMENT_STATUS = ROOT + "/payment/status/{uuid}";
    }

    private final ReservationCRUDFacade reservationCRUDFacade;

    @GetMapping(ROOT)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    List<ReservationDTO> getReservations() {
        return reservationCRUDFacade.getReservations();
    }

    @GetMapping(SINGLE_RESERVATION)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    ReservationDTO getReservation(@PathVariable final UUID uuid) {
        return reservationCRUDFacade.getReservation(uuid);
    }

    @PostMapping(ROOT)
    @ResponseStatus(HttpStatus.CREATED)
    PaymentsWrapperDto createReservation(@Valid @RequestBody final ReservationForm reservationForm,
                                         final HttpServletRequest request) {
        return reservationCRUDFacade.createReservation(reservationForm, request);
    }

    @PostMapping(ADMIN_RESERVATION)
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    ReservationDTO createReservationByAdmin(@Valid @RequestBody final ReservationForm reservationForm) {
        return reservationCRUDFacade.createAdminReservation(reservationForm);
    }

    @DeleteMapping(SINGLE_RESERVATION)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN')")
    void deleteReservation(@PathVariable final UUID uuid) {
        reservationCRUDFacade.deleteReservation(uuid);
    }

    @PatchMapping(ARRIVED_STATUS)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    void changeArrivedStatus(@PathVariable final UUID uuid) {
        reservationCRUDFacade.changeArrivedStatus(uuid);
    }


    @PatchMapping(PAYMENT_AMOUNT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    void changePaymentAmount(@PathVariable final UUID uuid, @RequestBody final BigDecimal amount) {
        reservationCRUDFacade.changePaymentAmount(uuid, amount);
    }

    @PatchMapping(PAYMENT_STATUS)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    void changePaymentStatus(@PathVariable final UUID uuid) {
        reservationCRUDFacade.confirmPayment(uuid);
    }

    @PutMapping(SINGLE_RESERVATION)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN') or hasRole('OWNER')")
    void updateReservation(@PathVariable final UUID uuid, @Valid @RequestBody final ReservationForm reservationForm) {
        reservationCRUDFacade.editReservation(uuid, reservationForm);
    }
}