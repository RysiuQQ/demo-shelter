package lynx.code.wielkaracza.reservation.controller;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.reservation.ReservationSearchingFacade;
import lynx.code.wielkaracza.reservation.domain.ReservationWithPaginationDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static lynx.code.wielkaracza.reservation.ReservationSearchingFacade.*;
import static lynx.code.wielkaracza.reservation.ReservationSearchingFacade.ReservationSearchParametersDTO;
import static lynx.code.wielkaracza.reservation.controller.ReservationSearchingController.Routes.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin(originPatterns = "*")
class ReservationSearchingController {

    @UtilityClass
    static final class Routes {
        static final String ROOT = "/api/reservations/search";
        static final String PEOPLE = "/api/reservations/search/people";
        static final String INCOME = "/api/reservations/search/income";
    }

    private final ReservationSearchingFacade reservationSearchingFacade;

    @PostMapping(ROOT)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    ReservationWithPaginationDTO getReservationsBySearchParameters
            (@Valid @RequestBody final ReservationSearchParametersDTO reservationSearchParametersDTO,
             final @RequestParam(defaultValue = "0") Integer page,
             final @RequestParam(defaultValue = "10") Integer size) {
        return reservationSearchingFacade.getReservationBySearchParameters(page, size, reservationSearchParametersDTO);
    }

    @PostMapping(PEOPLE)
    @PreAuthorize("hasRole('ADMIN')")
    int getPeopleByMonth(@Valid @RequestBody final ReservationMonthAndYearForm reservationMonthAndYearForm) {
         return reservationSearchingFacade.getPeopleByMonth(reservationMonthAndYearForm);
    }

    @PostMapping(INCOME)
    @PreAuthorize("hasRole('ADMIN')")
    double getIncomeByMonth(@Valid @RequestBody final ReservationMonthAndYearForm reservationMonthAndYearForm) {
        return reservationSearchingFacade.getIncomeByMonth(reservationMonthAndYearForm);
    }
}
