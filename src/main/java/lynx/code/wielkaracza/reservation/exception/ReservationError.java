package lynx.code.wielkaracza.reservation.exception;

public enum ReservationError {
    INCORRECT_RESERVATION_TYPE("Provided reservation type is incorrect!"),

    INCORRECT_IS_ACCOMMODATION_PAID("IsAccommodationPaid can contain only true or false value"),

    INCORRECT_RESERVATION_ID("Reservation does not exist with this id"),

    INCORRECT_RESERVATION_TO_DELETE("Deleting past reservations is impossible"),

    INCORRECT_RESERVATION_TO_EDIT("Editing past and ongoing reservations is impossible"),

    LACK_ADMIN_PERMISSIONS("Cannot make admin reservation with no permissions"),

    INCORRECT_AMOUNT_OF_GUESTS("Number of people is not equal with amount of people declared in reservation"),

    ERROR_PAYMENT_LINK_GENERATOR("There was an error during payment link generation"),

    TIME_ERROR("Reservation on the same date should be made before 12PM"),

    FORBIDDEN_DATE("Reservation on that date cannot be made");


    private String message;

    ReservationError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
