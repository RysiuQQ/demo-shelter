package lynx.code.wielkaracza.reservation.exception;



import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.common.exception.ErrorInfo;
import lynx.code.wielkaracza.security.jwt.AuthTokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@RequiredArgsConstructor
public class ReservationExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    @ExceptionHandler(ReservationException.class)
    @ResponseBody
    public ResponseEntity<ErrorInfo> handleException(final ReservationException e) {

        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if (ReservationError.INCORRECT_RESERVATION_TYPE.equals(e.getReservationError())) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        if (ReservationError.INCORRECT_IS_ACCOMMODATION_PAID.equals(e.getReservationError())) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        if (ReservationError.INCORRECT_RESERVATION_ID.equals(e.getReservationError())) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        if (ReservationError.INCORRECT_RESERVATION_TO_EDIT.equals(e.getReservationError())) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        if (ReservationError.TIME_ERROR.equals(e.getReservationError())) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        if (ReservationError.ERROR_PAYMENT_LINK_GENERATOR.equals(e.getReservationError())) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        if (ReservationError.FORBIDDEN_DATE.equals(e.getReservationError())) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        logger.error("Unknown error occurred");
        logger.error("HttpStatus {}", httpStatus);
        logger.error("Exception: {}", e.getReservationError());

        return ResponseEntity.status(httpStatus).body(new ErrorInfo(e.getReservationError().getMessage()));
    }
}
