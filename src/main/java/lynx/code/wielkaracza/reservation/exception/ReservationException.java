package lynx.code.wielkaracza.reservation.exception;

import lombok.Data;

@Data
public class ReservationException extends RuntimeException {

    private ReservationError reservationError;

    public ReservationException(ReservationError reservationError) {
        this.reservationError = reservationError;
    }
}
