package lynx.code.wielkaracza.reservation.domain;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.cart.ProductRoomForm;
import lynx.code.wielkaracza.cart.ProductRoomMapper;
import lynx.code.wielkaracza.common.exception.NotFoundException;
import lynx.code.wielkaracza.email.EmailHandlerService;
import lynx.code.wielkaracza.payments.dto.PaymentsWrapperDto;
import lynx.code.wielkaracza.payments.service.PaymentsService;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade;
import lynx.code.wielkaracza.reservation.exception.ReservationError;
import lynx.code.wielkaracza.reservation.exception.ReservationException;
import lynx.code.wielkaracza.room.domain.RoomQueryRepository;
import lynx.code.wielkaracza.security.jwt.AuthTokenFilter;
import lynx.code.wielkaracza.user.UserFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import static org.apache.commons.lang3.BooleanUtils.isFalse;

@Service
@RequiredArgsConstructor
class ReservationCRUDService implements ReservationCRUDFacade {

    @UtilityClass
    static final class ErrorMessages {
        static final String RESERVATION_DOES_NOT_EXIST = "Reservation with UUID: %s does not exist!";
        static final String ROOM_DOES_NOT_EXIST = "Room with UUID: %s does not exist!";
    }

    private final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);
    private final ReservationQueryRepository reservationQueryRepository;
    private final ReservationRepository reservationRepository;
    private final RoomQueryRepository roomQueryRepository;
    private final PaymentsService paymentsService;
    private final EmailHandlerService emailHandlerService;
    private final ReservationLogic reservationLogic;
    private final UserFacade userFacade;

    @Override
    public List<ReservationDTO> getReservations() {
        return reservationQueryRepository.getReservations().stream()
                .map(ReservationMapper::toDTO)
                .toList();
    }

    @Override
    public ReservationDTO getReservation(final UUID uuid) {
        return ReservationMapper.toDTO(getReservationOrThrow(uuid));
    }

    @Override
    @Transactional
    public PaymentsWrapperDto createReservation(final ReservationForm reservationForm, final HttpServletRequest request) {
        final var numberOfNights = getNumberOfNights(reservationForm);
        final var productRooms = prepareProductRooms(reservationForm);
        final var amountToPay = countAmountToPay(productRooms, numberOfNights);
        final var reservation = ReservationMapper.toEntity(reservationForm, numberOfNights,
                amountToPay, ReservationType.INTERNET_RESERVATION);

        reservationLogic.checkRoomsAvailability(productRooms, reservation, reservation);

        reservation.setSessionId(UUID.randomUUID().toString());
        final String paymentUrl = paymentsService.getPaymentUrl(reservation);
        if (!"".equals(paymentUrl)) {

            reservationRepository.save(reservation);
            userFacade.addNotification();

            logger.info("Reservation with uuid '{}' has been created by {} {} at {}",
                    reservation.getUuid(), reservation.getFirstName(), reservation.getLastName(), LocalDate.now());

            return new PaymentsWrapperDto(ReservationMapper.toDTO(reservation), paymentUrl);

        } else {
            logger.info("[PAYMENTS] Something went wrong! Payment with session id: '{}' has not been proceeded by {} {}",
                    reservation.getSessionId(), reservation.getFirstName(), reservation.getLastName());

            emailHandlerService.sendFailedEmailNotification(ReservationMapper.toDTO(reservation));
            throw new ReservationException(ReservationError.ERROR_PAYMENT_LINK_GENERATOR);
        }
    }

    @Override
    public ReservationDTO createAdminReservation(final ReservationForm reservationForm) {
        final var numberOfNights = getNumberOfNights(reservationForm);
        final var productRooms = prepareProductRooms(reservationForm);
        final var amountToPay = countAmountToPay(productRooms, numberOfNights);
        final var reservation = ReservationMapper.toEntity(reservationForm, numberOfNights,
                amountToPay, ReservationType.CALL_RESERVATION);

        reservationLogic.checkRoomsAvailability(productRooms, reservation, reservation);
        reservationRepository.save(reservation);
        userFacade.addNotification();

        logger.info("Admin reservation with uuid '{}' has been created by {} {} at {}",
                reservation.getUuid(), reservation.getFirstName(), reservation.getLastName(), LocalDate.now());

        return ReservationMapper.toDTO(reservation);
    }

    @Override
    @Transactional
    public void editReservation(final UUID uuid, final ReservationForm reservationForm) {
        final var reservation = reservationQueryRepository
                .findByUuid(uuid)
                .orElseThrow(() -> new ReservationException(ReservationError.INCORRECT_RESERVATION_ID));

        final var reservationType = reservation.getReservationDetails().getReservationType();
        final var alreadyPaid = reservation.getAmountAlreadyPaid();
        final var accommodationPaid = reservation.isAccommodationPaid();

        makeReservationsFuture(reservation);
        final var roomProducts = getRoomProducts(reservationForm.rooms());

        final Reservation reservationNew = ReservationMapper.toEntity(reservationForm, getNumberOfNights(reservationForm),
                countAmountToPay(roomProducts, getNumberOfNights(reservationForm)), reservationType);

        try {
            updateReservation(reservationNew, reservation, roomProducts, reservationType, alreadyPaid, accommodationPaid);

            logger.info("Reservation with uuid '{}' has been edited by {} {} at {}",
                    reservation.getUuid(), reservation.getFirstName(), reservation.getLastName(), LocalDate.now());
        } catch (final Exception e) {
            rollbackIfUpdateFails(reservation);
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional
    public void deleteReservation(final UUID uuid) {
        Reservation reservation = getReservationOrThrow(uuid);
        reservation.setDeleted(Boolean.TRUE);
        emailHandlerService.sendReservationDeletedEmail(ReservationMapper.toDTO(reservation));
        logger.info("Reservation with uuid '{}' has been deleted at {}", uuid, LocalDate.now());
    }

    @Override
    @Transactional
    public void changeArrivedStatus(final UUID uuid) {
        final var reservation = getReservationOrThrow(uuid);
        reservation.getReservationDetails().setArrived(isFalse(reservation.getReservationDetails().isArrived()));

        logger.info("Arrived status of reservation with uuid '{}' has been changed by {} {} at {}",
                reservation.getUuid(), reservation.getFirstName(), reservation.getLastName(), LocalDate.now());
    }

    @Override
    @Transactional
    public void changePaymentAmount(final UUID uuid, final BigDecimal amount) {
        final var reservation = getReservationOrThrow(uuid);
        reservation.setAmountAlreadyPaid(amount);

        logger.info("Payment amount of reservation with uuid '{}' has been changed by {} {} at {}",
                reservation.getUuid(), reservation.getFirstName(), reservation.getLastName(), LocalDate.now());
    }

    @Override
    @Transactional
    public void confirmPayment(final UUID uuid) {
        final var reservation = getReservationOrThrow(uuid);
        reservation.setAccommodationPaid(isFalse(reservation.isAccommodationPaid()));

        logger.info("Payment of reservation with uuid '{}' has been confirmed by {} {} at {}",
                reservation.getUuid(), reservation.getFirstName(), reservation.getLastName(), LocalDate.now());
    }

    private List<ProductRoom> prepareProductRooms(final ReservationForm reservationForm) {
        return reservationForm.rooms().stream()
                .map(this::parseProductRoom)
                .toList();
    }

    private ProductRoom parseProductRoom(final ProductRoomForm productRoomForm) {
        final var room = roomQueryRepository.findByUuid(productRoomForm.uuid())
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ROOM_DOES_NOT_EXIST, productRoomForm.uuid()));
        return ProductRoomMapper.toEntity(productRoomForm, room);
    }

    private int getNumberOfNights(final ReservationForm reservationForm) {
        return Period.between(reservationForm.reservationDayStart(), reservationForm.reservationDayEnd()).getDays();
    }

    private BigDecimal countAmountToPay(final List<ProductRoom> rooms, final int nights) {
        final AtomicReference<Double> amount = new AtomicReference<>(0d);

        rooms.forEach(product -> amount.updateAndGet
                (v -> v + product.getRoom().getPrice().doubleValue() * nights * product.getNumberOfPeople()));

        return BigDecimal.valueOf(amount.get());
    }

    private void makeReservationsFuture(final Reservation reservation) {
        reservation.setReservationDayStart(reservation.getReservationDayStart().plusYears(300));
        reservation.setReservationDayEnd(reservation.getReservationDayEnd().plusYears(300));
        reservation.getUnavailableTerms().forEach(unavailableTerm -> {
            unavailableTerm.setReservationDayStart(reservation.getReservationDayStart().plusYears(300));
            unavailableTerm.setReservationDayEnd(reservation.getReservationDayEnd().plusYears(300));
        });
    }

    private List<ProductRoom> getRoomProducts(final Set<ProductRoomForm> rooms) {
        return rooms.stream()
                .map(productRoomForm -> {
                    final var room = roomQueryRepository.findByUuid(productRoomForm.uuid())
                            .orElseThrow(() -> new NotFoundException(ErrorMessages.ROOM_DOES_NOT_EXIST, productRoomForm.uuid()));
                    return ProductRoomMapper.toEntity(productRoomForm, room);
                })
                .toList();
    }

    private void updateReservation(final Reservation reservationNew, Reservation reservation, final List<ProductRoom> productRooms,
                                   final ReservationType reservationType, final BigDecimal alreadyPaid, final boolean accommodationPaid) {
        reservationLogic.checkRoomsAvailability(productRooms, reservationNew, reservation);
        reservationNew.getReservationDetails().setReservationType(reservationType);
        reservationNew.setAmountAlreadyPaid(alreadyPaid);
        reservationNew.setAccommodationPaid(accommodationPaid);
        reservationRepository.save(reservationNew);
    }

    private void rollbackIfUpdateFails(final Reservation reservation) {
        reservation.setReservationDayStart(reservation.getReservationDayStart().minusYears(300));
        reservation.setReservationDayEnd(reservation.getReservationDayEnd().minusYears(300));
        reservation.getUnavailableTerms().forEach(unavailableTerm -> {
            unavailableTerm.setReservationDayStart(reservation.getReservationDayStart().minusYears(300));
            unavailableTerm.setReservationDayEnd(reservation.getReservationDayEnd().minusYears(300));
        });
    }

    private Reservation getReservationOrThrow(final UUID uuid) {
        return reservationQueryRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.RESERVATION_DOES_NOT_EXIST, uuid.toString()));
    }
}