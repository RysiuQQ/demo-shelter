package lynx.code.wielkaracza.reservation.domain;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.common.exception.NoEnoughPlacesException;
import lynx.code.wielkaracza.common.exception.RoomsDuplicatedException;
import lynx.code.wielkaracza.common.searchingengine.Path;
import lynx.code.wielkaracza.common.searchingengine.SearchingUtils;
import lynx.code.wielkaracza.room.domain.Room;
import lynx.code.wielkaracza.room.domain.RoomQueryRepository;
import lynx.code.wielkaracza.unavailableterm.UnavailableQueryRepository;
import lynx.code.wielkaracza.unavailableterm.UnavailableTerm;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.BooleanUtils.isFalse;

@RequiredArgsConstructor
@Component
class ReservationLogic {

    @UtilityClass
    static final class ErrorMessages {
        static final String ROOMS_DUPLICATION = "Rooms are duplicated!";
        static final String NO_PLACES = "There is no enough places in that room";
        static final String NO_PLACES_DOG = "There is no enough places in that room for reservation with dog";
    }

    private final RoomQueryRepository roomQueryRepository;
    private final UnavailableQueryRepository unavailableQueryRepository;

    public void checkRoomsAvailability(final List<ProductRoom> productRooms, final Reservation reservation,
                                       final Reservation reservationOld) {
        final LocalDate reservationDayStart = reservation.getReservationDayStart();
        final LocalDate reservationDayEnd = reservation.getReservationDayEnd();


        if (areRoomsDuplicated(productRooms)) throw new RoomsDuplicatedException(ErrorMessages.ROOMS_DUPLICATION);

        productRooms.forEach(productRoom -> {
            final var unavailableTerms =
                    unavailableQueryRepository.getFutureDates(reservationDayStart, productRoom.getRoom().getUuid(),
                            reservationDayEnd);


            if (nonNull(reservationOld)) {
                final var reservationOldUnavailableTerms = reservationOld.getUnavailableTerms().stream().toList();
                ListIterator<UnavailableTerm> iterator = unavailableTerms.listIterator();

                while (iterator.hasNext()) {
                    UnavailableTerm term = iterator.next();
                    boolean removed = false;
                    for (int j = 0; j < reservationOldUnavailableTerms.size(); j++) {
                        if (term.getUuid().equals(reservationOldUnavailableTerms.get(j).getUuid())) {
                            iterator.remove();
                            removed = true;
                        }
                    }
                    if (removed) {
                        iterator = unavailableTerms.listIterator(iterator.nextIndex());
                    }
                }
            }

            final List<UnavailableTerm> dateToCompare = new ArrayList<>();

            if ((productRoom.getNumberOfPeople() - productRoom.getRoom().getMaxPeople()) > 0) {
                throw new NoEnoughPlacesException(ErrorMessages.NO_PLACES);
            }

            checkCrossingTerms(unavailableTerms, reservationDayStart, reservationDayEnd, dateToCompare);

            final UnavailableTerm unavailableTerm = createUnavailableTerm(reservationDayStart, reservationDayEnd);

            if (isFalse(dateToCompare.isEmpty())) {
                final var dataPaths = prepareDataPaths(dateToCompare, productRoom);
                final var freePlaces = SearchingUtils.countAmountOfPeople(dataPaths, productRoom.getRoom());
                final boolean arePlacesAvailable =
                        arePlacesAvailable(productRoom.getNumberOfPeople(), freePlaces);
                final boolean isRoomEmptyIfReservationIncludesDog =
                        isRoomEmptyIfReservationIncludesDog(productRoom, freePlaces, reservation);

                if (isFalse(arePlacesAvailable)) throw new NoEnoughPlacesException(ErrorMessages.NO_PLACES);
                if (isFalse(isRoomEmptyIfReservationIncludesDog))
                    throw new NoEnoughPlacesException(ErrorMessages.NO_PLACES_DOG);
            }

            setDataForRoom(unavailableTerm, productRoom.getRoom());
            setDataForReservation(reservation, unavailableTerm, productRoom);
        });
    }

    private boolean areRoomsDuplicated(final List<ProductRoom> productRooms) {
        final Set<Integer> setToCheck = new HashSet<>();
        return productRooms.stream()
                .map(x -> x.getRoom().getId())
                .anyMatch(roomId -> isFalse(setToCheck.add(roomId)));
    }

    private void checkCrossingTerms(final List<UnavailableTerm> unavailableTerms, final LocalDate reservationDayStart,
                                    final LocalDate reservationDayEnd, final List<UnavailableTerm> dateToCompare) {

        for (final UnavailableTerm unavailableTerm : unavailableTerms) {

            if (unavailableTerm.getReservationDayStart().isAfter(reservationDayStart)) {

                if (reservationDayEnd.isEqual(unavailableTerm.getReservationDayStart())) {
                    continue;
                }

                if (isFalse(reservationDayEnd.isBefore(unavailableTerm.getReservationDayStart()))) {
                    dateToCompare.add(unavailableTerm);
                }
            }
            if (unavailableTerm.getReservationDayStart().isBefore(reservationDayStart)) {
                if (reservationDayStart.isEqual(unavailableTerm.getReservationDayEnd())) {
                    continue;
                }

                if (isFalse(reservationDayStart.isAfter(unavailableTerm.getReservationDayEnd()))) {
                    dateToCompare.add(unavailableTerm);
                }
            }

            if (unavailableTerm.getReservationDayStart().isEqual(reservationDayStart)) {
                dateToCompare.add(unavailableTerm);
            }
        }
    }

    private UnavailableTerm createUnavailableTerm(final LocalDate reservationDayStart, final LocalDate reservationDayEnd) {
        final var unavailableTerm = new UnavailableTerm();
        unavailableTerm.setReservationDayStart(reservationDayStart);
        unavailableTerm.setReservationDayEnd(reservationDayEnd);
        return unavailableTerm;
    }

    private List<Path> prepareDataPaths(final List<UnavailableTerm> dateToCompare, final ProductRoom productRoom) {

        final var dataPaths = new ArrayList<>(dateToCompare.stream()
                .map(date -> SearchingUtils.prepareDataPaths(date, dateToCompare))
                .toList());

        dataPaths.forEach(path -> {
            final var unavailableTermsToRemove = new ArrayList<UnavailableTerm>();
            for (int i = 0; i < path.getUnavailableTerms().size() - 1; i++) {
                final var room = productRoom.getRoom();
                SearchingUtils.eliminateNotAssociatedData(room, unavailableTermsToRemove,
                        path.getUnavailableTerms().get(i), path.getUnavailableTerms().get(i + 1));
            }
            path.getUnavailableTerms().removeAll(unavailableTermsToRemove);
        });

        return dataPaths;
    }

    private boolean arePlacesAvailable(final int numberOfPeople, final int freePlaces) {
        return freePlaces - numberOfPeople >= 0;
    }

    private boolean isRoomEmptyIfReservationIncludesDog(final ProductRoom productRoom, final int freePlaces,
                                                        final Reservation reservation) {
        if (reservation.getReservationDetails().isDogs()) {
            return productRoom.getRoom().getMaxPeople() == freePlaces;
        }
        return true;
    }

    private void setDataForRoom(final UnavailableTerm unavailableTerm, final Room room) {
        roomQueryRepository.findByUuid(room.getUuid()).ifPresent(roomFromDataBase -> {
            roomFromDataBase.getUnavailableTerms().add(unavailableTerm);
            unavailableTerm.setRoom(roomFromDataBase);
        });
    }

    private void setDataForReservation(final Reservation reservation, final UnavailableTerm unavailableTerm,
                                       final ProductRoom roomProduct) {
        unavailableTerm.setReservation(reservation);
        roomProduct.setReservation(reservation);
        reservation.getUnavailableTerms().add(unavailableTerm);
        reservation.getRooms().add(roomProduct.getRoom());
        reservation.getProductRooms().add(roomProduct);
        reservation.getReservationDetails().setReservation(reservation);
    }
}