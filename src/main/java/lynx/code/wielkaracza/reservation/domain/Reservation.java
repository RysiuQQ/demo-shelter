package lynx.code.wielkaracza.reservation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;
import lynx.code.wielkaracza.room.domain.Room;
import lynx.code.wielkaracza.unavailableterm.UnavailableTerm;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "reservation")
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class Reservation extends AbstractUUIDEntity {

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private int people;

    @Column(nullable = false)
    private LocalDate reservationDayStart;

    @Column(nullable = false)
    private LocalDate reservationDayEnd;

    @Column(nullable = false)
    private LocalDateTime dateOfAddingReservation;

    @Column(nullable = false)
    private int nights;

    @Column(nullable = false)
    private BigDecimal amountAlreadyPaid;

    @Size(max = 40)
    private String sessionId;

    @Column(nullable = false)
    private boolean accommodationPaid;

    @Column(nullable = false)
    private BigDecimal amountToPay;

    @Column(nullable = false)
    private boolean deleted;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = ReservationDetails.Fields.reservation)
    private ReservationDetails reservationDetails;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = UnavailableTerm.Fields.reservation)
    private Set<UnavailableTerm> unavailableTerms;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = ProductRoom.Fields.reservation)
    private Set<ProductRoom> productRooms;

    @ManyToMany
    @JoinTable(name = "reservation_rooms", joinColumns = {@JoinColumn(name = "reservation_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "room_id", referencedColumnName = "id")})
    private Set<Room> rooms;
}