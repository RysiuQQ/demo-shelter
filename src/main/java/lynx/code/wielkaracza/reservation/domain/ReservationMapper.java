package lynx.code.wielkaracza.reservation.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.common.enums.Language;
import lynx.code.wielkaracza.common.pagination.Pagination;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade.ReservationDTO;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade.ReservationForm;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static lynx.code.wielkaracza.room.RoomCRUDFacade.ProductRoomDTO;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@UtilityClass
public class ReservationMapper {

    public ReservationDTO toDTO(final Reservation reservation) {
        return ReservationDTO.builder()
                .uuid(reservation.getUuid())
                .firstName(reservation.getFirstName())
                .lastName(reservation.getLastName())
                .people(reservation.getPeople())
                .nights(reservation.getNights())
                .dogs(reservation.getReservationDetails().isDogs())
                .reservationDayStart(reservation.getReservationDayStart())
                .reservationDayEnd(reservation.getReservationDayEnd())
                .amountAlreadyPaid(reservation.getAmountAlreadyPaid())
                .amountToPay(reservation.getAmountToPay())
                .accommodationPaid(reservation.isAccommodationPaid())
                .phoneNumber(reservation.getReservationDetails().getPhoneNumber())
                .extraInformation(reservation.getReservationDetails().getExtraInformation())
                .dateOfAddingReservation(reservation.getDateOfAddingReservation())
                .email(reservation.getReservationDetails().getEmail())
                .productRoomsDTO(toProductRoomsDTO(reservation.getProductRooms()))
                .language(reservation.getReservationDetails().getLanguage())
                .reservationType(reservation.getReservationDetails().getReservationType())
                .arrived(reservation.getReservationDetails().isArrived())
                .roomNumbers(getRoomNumbers(reservation))
                .deleted(reservation.isDeleted())
                .build();
    }

    private List<ProductRoomDTO> toProductRoomsDTO(final Set<ProductRoom> productRooms) {
        return productRooms.stream()
                .map(ReservationMapper::prepareProductRoom)
                .toList();

    }

    private ProductRoomDTO prepareProductRoom(final ProductRoom productRoom) {
        return ProductRoomDTO.builder()
                .uuid(productRoom.getRoom().getUuid())
                .descriptionPL(productRoom.getRoom().getDescriptionPL())
                .descriptionEN(productRoom.getRoom().getDescriptionEN())
                .descriptionSK(productRoom.getRoom().getDescriptionSK())
                .titlePL(productRoom.getRoom().getTitlePL())
                .titleEN(productRoom.getRoom().getTitleEN())
                .titleSK(productRoom.getRoom().getTitleSK())
                .numberOfPeople(productRoom.getNumberOfPeople())
                .roomNumber(productRoom.getRoom().getRoomNumber())
                .numberOfNormalBeds(productRoom.getRoom().getNumberOfNormalBeds())
                .numberOfDoubleBeds(productRoom.getRoom().getNumberOfDoubleBeds())
                .build();
    }

    ReservationWithPaginationDTO toDTOWithPagination(final Page<Reservation> reservationsFromDb) {
        final List<ReservationDTO> reservationDTOS = new ArrayList<>();
        final Pagination pagination = new Pagination(reservationsFromDb.getSize(),
                reservationsFromDb.getTotalElements(),
                reservationsFromDb.getTotalPages(),
                reservationsFromDb.getNumber());

        reservationsFromDb.getContent().forEach(reservationDb -> reservationDTOS.add(toDTO(reservationDb)));
        return new ReservationWithPaginationDTO(reservationDTOS, pagination);
    }

    Reservation toEntity(final ReservationForm reservationForm, final int nights, final BigDecimal amountToPay,
                         final ReservationType reservationType) {

        final var reservationDetails = buildReservationDetails(reservationForm, reservationType);
        ZoneId warsawZone = ZoneId.of("Europe/Warsaw");
        return Reservation.builder()
                .firstName(reservationForm.firstName())
                .lastName(reservationForm.lastName())
                .people(reservationForm.people())
                .reservationDayStart(reservationForm.reservationDayStart())
                .reservationDayEnd(reservationForm.reservationDayEnd())
                .dateOfAddingReservation(ZonedDateTime.now(warsawZone).toLocalDateTime())
                .amountAlreadyPaid(
                        isTrue(Objects.equals(reservationType.name(), ReservationType.INTERNET_RESERVATION.name())) ?
                                amountToPay.divide(BigDecimal.valueOf(2), RoundingMode.HALF_UP) : BigDecimal.ZERO)
                .amountToPay(amountToPay)
                .accommodationPaid(Boolean.FALSE)
                .nights(nights)
                .productRooms(new HashSet<>())
                .rooms(new HashSet<>())
                .unavailableTerms(new HashSet<>())
                .reservationDetails(reservationDetails)
                .build();
    }

    ReservationDetails buildReservationDetails(final ReservationForm reservationForm,
                                               final ReservationType reservationType) {
        return ReservationDetails.builder()
                .phoneNumber(reservationForm.phoneNumber())
                .arrived(Boolean.FALSE)
                .language(Language.valueOf(reservationForm.language()))
                .reservationType(reservationType)
                .email(reservationForm.email())
                .dogs(reservationForm.dogs())
                .extraInformation(reservationForm.extraInformation())
                .build();
    }

    private List<Integer> getRoomNumbers(final Reservation reservation) {
        return reservation.getProductRooms().stream()
                .map(ProductRoom::getRoomNumber)
                .toList();
    }
}
