package lynx.code.wielkaracza.reservation.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.room.domain.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.nonNull;
import static lynx.code.wielkaracza.reservation.ReservationOverviewFacade.ReservationBasicInfo;

@UtilityClass
public class ReservationComingMapper {

    ReservationBasicInfo toDTO(final Reservation reservation) {
        return ReservationBasicInfo.builder()
                .uuid(reservation.getUuid())
                .firstLetterOfSurname(reservation.getLastName().charAt(0))
                .firstName(reservation.getFirstName())
                .lastName(reservation.getLastName())
                .howManyPeople(reservation.getPeople())
                .reservationStart(reservation.getReservationDayStart())
                .reservationEnd(reservation.getReservationDayEnd())
                .reservationType(Objects.isNull(reservation.getReservationDetails()) ? ReservationType.CALL_RESERVATION : reservation.getReservationDetails().getReservationType())
                .roomNumbers(getRoomNumbers(reservation))
                .build();

    }

    private List<Integer> getRoomNumbers(final Reservation reservation) {
        if (nonNull(reservation.getRooms())) {
            return reservation.getRooms().stream()
                    .map(Room::getRoomNumber)
                    .toList();
        }
        return new ArrayList<>();
    }
}
