package lynx.code.wielkaracza.reservation.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaRepository;

public interface ReservationRepository extends UuidAwareJpaRepository<Reservation, Long> {
}