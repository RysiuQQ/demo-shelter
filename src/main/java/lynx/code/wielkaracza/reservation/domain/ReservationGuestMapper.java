package lynx.code.wielkaracza.reservation.domain;


import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.reservation.ReservationOverviewFacade.ReservationGuestDTO;
import lynx.code.wielkaracza.room.domain.Room;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

@UtilityClass
class ReservationGuestMapper {

    ReservationGuestDTO toDTO(final Reservation reservation) {
        return ReservationGuestDTO.builder()
                .uuid(reservation.getUuid())
                .firstName(reservation.getFirstName())
                .lastName(reservation.getLastName())
                .startDate(reservation.getReservationDayStart())
                .finishDate(reservation.getReservationDayEnd())
                .arrived(reservation.getReservationDetails().isArrived())
                .alreadyPaid(reservation.getAmountAlreadyPaid())
                .reservationType(reservation.getReservationDetails().getReservationType())
                .roomNumbers(getRoomNumbers(reservation))
                .amountToPay(reservation.getAmountToPay())
                .reservationUuid(reservation.getUuid())
                .numberOfCompanies(reservation.getPeople())
                .number(reservation.getReservationDetails().getPhoneNumber())
                .build();

    }

    private List<Integer> getRoomNumbers(final Reservation reservation) {
        List<Integer> roomNumbers = new ArrayList<>();
        if (nonNull(reservation.getRooms())) {
            roomNumbers = reservation.getRooms().stream()
                    .map(Room::getRoomNumber)
                    .toList();
        }
        return roomNumbers;
    }
}
