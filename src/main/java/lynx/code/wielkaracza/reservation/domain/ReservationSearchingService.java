package lynx.code.wielkaracza.reservation.domain;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.common.utils.Utils;
import lynx.code.wielkaracza.reservation.ReservationSearchingFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static lynx.code.wielkaracza.reservation.domain.ReservationType.ACTIVE_RESERVATION;
import static lynx.code.wielkaracza.reservation.domain.ReservationType.INACTIVE_RESERVATION;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Service
@RequiredArgsConstructor
public class ReservationSearchingService implements ReservationSearchingFacade {

    private final ReservationQueryRepository reservationQueryRepository;

    @Override
    public ReservationWithPaginationDTO getReservationBySearchParameters(final Integer page, final Integer size,
                                                                         final ReservationSearchParametersDTO reservationSearchParametersDTO) {

        final String aDateOfAddingReservation = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.dateOfAddingReservation()));
        final String aFirstName = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.firstName()));
        final String aLastName = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.lastName()));
        final String aReservationDayEnd = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.reservationDayEnd()));
        final String aReservationDayStart = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.reservationDayStart()));
        final String aEmail = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.email()));
        final String aPhoneNumber = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.phoneNumber()));
        String aReservationType = Utils.provideLikeParameter(String.valueOf(reservationSearchParametersDTO.reservationType()));
        Boolean aIsAccommodationPaid = reservationSearchParametersDTO.isAccommodationPaid();

        Date date = new Date();
        LocalDate aCurrentDate = Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();

        final PageRequest pageRequest = PageRequest.of(page, size);
        Page<Reservation> reservationsFromDb;

        if (isTrue(reservationSearchParametersDTO.deleted())) {
            reservationsFromDb = reservationQueryRepository.getDeletedReservationsBySearchParameters(pageRequest,
                    aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                    aEmail, aPhoneNumber, aReservationType);
        } else if (reservationSearchParametersDTO.reservationType() == INACTIVE_RESERVATION) {
            aReservationType = "%%"; // we do NOT have this type of reservation in db, so we need to skip

            if (Boolean.TRUE.equals(aIsAccommodationPaid)) {
                reservationsFromDb = reservationQueryRepository.getInactivePaidReservationsBySearchParameters(pageRequest, aCurrentDate,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            } else if (Boolean.FALSE.equals(aIsAccommodationPaid)) {
                reservationsFromDb = reservationQueryRepository.getInactiveUnpaidReservationsBySearchParameters(pageRequest, aCurrentDate,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            } else {
                reservationsFromDb = reservationQueryRepository.getInactiveReservationsBySearchParameters(pageRequest, aCurrentDate,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            }

        } else if (reservationSearchParametersDTO.reservationType() == ACTIVE_RESERVATION) {
            aReservationType = "%%"; // we do NOT have this type of reservation in db, so we need to skip
            if (Boolean.TRUE.equals(aIsAccommodationPaid)) {
                reservationsFromDb = reservationQueryRepository.getActivePaidReservationsBySearchParameters(pageRequest, aCurrentDate,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            } else if (Boolean.FALSE.equals(aIsAccommodationPaid)) {
                reservationsFromDb = reservationQueryRepository.getActiveUnpaidReservationsBySearchParameters(pageRequest, aCurrentDate,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            } else {
                reservationsFromDb = reservationQueryRepository.getActiveReservationsBySearchParameters(pageRequest, aCurrentDate,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            }
        } else {
            if (Boolean.TRUE.equals(aIsAccommodationPaid)) {
                reservationsFromDb = reservationQueryRepository.getPaidReservationsBySearchParameters(pageRequest,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            } else if (Boolean.FALSE.equals(aIsAccommodationPaid)) {
                reservationsFromDb = reservationQueryRepository.getUnpaidReservationsBySearchParameters(pageRequest,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            } else {
                reservationsFromDb = reservationQueryRepository.getReservationsBySearchParameters(pageRequest,
                        aDateOfAddingReservation, aFirstName, aLastName, aReservationDayEnd, aReservationDayStart,
                        aEmail, aPhoneNumber, aReservationType);
            }
        }

        return ReservationMapper.toDTOWithPagination(reservationsFromDb);
    }

    // DWIE METODY, UWAGI DLA MICHALA
    // JEŚLI TO JEST AKTUALNY MIESIAC ALBO PRZYSZLY OZNACZAC NA FRONCIE JAKO ESTIMATED
    // ALBO PRZEWIDYWANE, NAWET GDY TO 28 CZY 29.

    @Override
    public int getPeopleByMonth(final ReservationMonthAndYearForm reservationMonthAndYearForm) {
        final var reservationsBetweenDates =
                reservationQueryRepository.findReservationsBetweenDates(reservationMonthAndYearForm.year().getValue(),
                        reservationMonthAndYearForm.month().getValue());

        return reservationsBetweenDates.stream()
                .mapToInt(Reservation::getPeople)
                .sum();
    }

    @Override
    public double getIncomeByMonth(final ReservationMonthAndYearForm reservationMonthAndYearForm) {
        final var reservationsBetweenDates = reservationQueryRepository.findReservationsBetweenDates(reservationMonthAndYearForm.year().getValue(),
                reservationMonthAndYearForm.month().getValue());

        return reservationsBetweenDates.stream()
                .mapToDouble(reservation -> reservation.getAmountToPay().doubleValue())
                .sum();
    }
}
