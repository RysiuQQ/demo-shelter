package lynx.code.wielkaracza.reservation.domain;

import lynx.code.wielkaracza.common.pagination.Pagination;

import java.util.List;

import static lynx.code.wielkaracza.reservation.ReservationCRUDFacade.ReservationDTO;

public record ReservationWithPaginationDTO(List<ReservationDTO> reservations,
                                           Pagination pagination) {
}
