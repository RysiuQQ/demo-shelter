package lynx.code.wielkaracza.reservation.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ReservationGroup {
    SMALL_GROUP(12), MEDIUM_GROUP(20),
    LARGE_GROUP(75);

    private final int numberOfPeopleTopBorder;
}
