package lynx.code.wielkaracza.reservation.domain;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.common.date.DateDTO;
import lynx.code.wielkaracza.common.date.DateUtils;
import lynx.code.wielkaracza.reservation.ReservationOverviewFacade;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static java.util.Objects.isNull;


@Service
@RequiredArgsConstructor
public class ReservationOverviewService implements ReservationOverviewFacade {

    private final ReservationQueryRepository reservationQueryRepository;

    @Override
    public ReservationOverviewDTO getReservationOverview(final DateDTO providedDate) {
        final Long newReservations = reservationQueryRepository.getNumberOfNewReservations(DateUtils.getCurrentDate(providedDate));
        final Long todayReservations = reservationQueryRepository.getNumberOfTodayReservations(DateUtils.getCurrentDate(providedDate));
        final Long outReservations = reservationQueryRepository.getNumberOfOutReservations(DateUtils.getCurrentDate(providedDate));
        Long numberOfPeoplePerDay = reservationQueryRepository.getNumberOfPeoplePerDay(DateUtils.getCurrentDate(providedDate));

        if (isNull(numberOfPeoplePerDay)) numberOfPeoplePerDay = 0L;

        return new ReservationOverviewDTO(newReservations, todayReservations, numberOfPeoplePerDay, outReservations);
    }

    @Override
    public List<ReservationBasicInfo> getReservationComings(final DateDTO date) {
        // instead of limit clause in mySql which is not accepted by jpql
        return reservationQueryRepository.getComingReservations(DateUtils.getCurrentDate(date)).stream()
                .map(ReservationComingMapper::toDTO)
                .toList();
    }

    @Override
    public List<ReservationBasicInfo> getReservationForToday(final DateDTO date) {
        return reservationQueryRepository.getTodayReservations(DateUtils.getCurrentDate(date)).stream()
                .map(ReservationComingMapper::toDTO)
                .toList();
    }

    @Override
    public List<ReservationBasicInfo> getOngoingReservations(final DateDTO date) {
        return reservationQueryRepository.getOngoingReservations(DateUtils.getCurrentDate(date)).stream()
                .map(ReservationComingMapper::toDTO)
                .toList();
    }

    @Override
    public List<ReservationGuestDTO> getCurrentGuests(final DateDTO date) {
        return reservationQueryRepository.getCurrentGuests(DateUtils.getCurrentDate(date)).stream()
                .map(ReservationGuestMapper::toDTO)
                .toList();
    }

    @Override
    public List<Reservation> getReservationByDatesAndRoomNumber(LocalDate dateFrom, LocalDate dateTo, Integer roomNumber) {
        List<Reservation> reservationsByDate =
                reservationQueryRepository.findReservationsByDateBetween(dateFrom, dateTo);

        return reservationsByDate.stream()
                .filter(reservation -> checkIfRoomIsAssignedToReservation(reservation, roomNumber))
                .toList();
    }

    private boolean checkIfRoomIsAssignedToReservation(Reservation reservation, int roomNumber) {
        List<Integer> roomNumbers = reservation.getProductRooms().stream()
                .map(ProductRoom::getRoomNumber)
                .toList();

        if (!roomNumbers.isEmpty()) {
            return roomNumbers.contains(roomNumber);
        }

        return false;
    }
}

