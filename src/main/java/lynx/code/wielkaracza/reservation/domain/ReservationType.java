package lynx.code.wielkaracza.reservation.domain;

public enum ReservationType {
    INTERNET_RESERVATION,
    CALL_RESERVATION,
    INACTIVE_RESERVATION,
    ACTIVE_RESERVATION
}

