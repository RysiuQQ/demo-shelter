package lynx.code.wielkaracza.reservation.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

public interface ReservationQueryRepository extends UuidAwareJpaQueryRepository<Reservation, Long>, JpaSpecificationExecutor<Reservation> {

    @Query("SELECT r FROM Reservation r WHERE r.deleted = false")
    List<Reservation> getReservations();

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.deleted = false AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getReservationsBySearchParameters(Pageable pageable,
                                                        @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                        @Param("firstName") String aFirstName,
                                                        @Param("lastName") String aLastName,
                                                        @Param("reservationDayEnd") String aReservationDayEnd,
                                                        @Param("reservationDayStart") String aReservationDayStart,
                                                        @Param("email") String aEmail,
                                                        @Param("phoneNumber") String aPhoneNumber,
                                                        @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.deleted = false AND " +
            " r.accommodationPaid = true AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getPaidReservationsBySearchParameters(Pageable pageable,
                                                        @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                        @Param("firstName") String aFirstName,
                                                        @Param("lastName") String aLastName,
                                                        @Param("reservationDayEnd") String aReservationDayEnd,
                                                        @Param("reservationDayStart") String aReservationDayStart,
                                                        @Param("email") String aEmail,
                                                        @Param("phoneNumber") String aPhoneNumber,
                                                        @Param("reservationType") String aReservationType);
    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.deleted = false AND " +
            " r.accommodationPaid = false AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getUnpaidReservationsBySearchParameters(Pageable pageable,
                                                        @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                        @Param("firstName") String aFirstName,
                                                        @Param("lastName") String aLastName,
                                                        @Param("reservationDayEnd") String aReservationDayEnd,
                                                        @Param("reservationDayStart") String aReservationDayStart,
                                                        @Param("email") String aEmail,
                                                        @Param("phoneNumber") String aPhoneNumber,
                                                        @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.reservationDayEnd < :currentDate AND" +
            " r.deleted = false AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getInactiveReservationsBySearchParameters(Pageable pageable,
                                                                @Param("currentDate") LocalDate aCurrentDate,
                                                                @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                                @Param("firstName") String aFirstName,
                                                                @Param("lastName") String aLastName,
                                                                @Param("reservationDayEnd") String aReservationDayEnd,
                                                                @Param("reservationDayStart") String aReservationDayStart,
                                                                @Param("email") String aEmail,
                                                                @Param("phoneNumber") String aPhoneNumber,
                                                                @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.reservationDayEnd < :currentDate AND" +
            " r.deleted = false AND " +
            " r.accommodationPaid = true AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getInactivePaidReservationsBySearchParameters(Pageable pageable,
                                                                @Param("currentDate") LocalDate aCurrentDate,
                                                                @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                                @Param("firstName") String aFirstName,
                                                                @Param("lastName") String aLastName,
                                                                @Param("reservationDayEnd") String aReservationDayEnd,
                                                                @Param("reservationDayStart") String aReservationDayStart,
                                                                @Param("email") String aEmail,
                                                                @Param("phoneNumber") String aPhoneNumber,
                                                                @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.reservationDayEnd < :currentDate AND" +
            " r.deleted = false AND " +
            " r.accommodationPaid = false AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getInactiveUnpaidReservationsBySearchParameters(Pageable pageable,
                                                                    @Param("currentDate") LocalDate aCurrentDate,
                                                                    @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                                    @Param("firstName") String aFirstName,
                                                                    @Param("lastName") String aLastName,
                                                                    @Param("reservationDayEnd") String aReservationDayEnd,
                                                                    @Param("reservationDayStart") String aReservationDayStart,
                                                                    @Param("email") String aEmail,
                                                                    @Param("phoneNumber") String aPhoneNumber,
                                                                    @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.reservationDayEnd >= :currentDate AND" +
            " r.deleted = false AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getActiveReservationsBySearchParameters(Pageable pageable,
                                                              @Param("currentDate") LocalDate aCurrentDate,
                                                              @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                              @Param("firstName") String aFirstName,
                                                              @Param("lastName") String aLastName,
                                                              @Param("reservationDayEnd") String aReservationDayEnd,
                                                              @Param("reservationDayStart") String aReservationDayStart,
                                                              @Param("email") String aEmail,
                                                              @Param("phoneNumber") String aPhoneNumber,
                                                              @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.reservationDayEnd >= :currentDate AND" +
            " r.deleted = false AND " +
            " r.accommodationPaid = true AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getActivePaidReservationsBySearchParameters(Pageable pageable,
                                                              @Param("currentDate") LocalDate aCurrentDate,
                                                              @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                              @Param("firstName") String aFirstName,
                                                              @Param("lastName") String aLastName,
                                                              @Param("reservationDayEnd") String aReservationDayEnd,
                                                              @Param("reservationDayStart") String aReservationDayStart,
                                                              @Param("email") String aEmail,
                                                              @Param("phoneNumber") String aPhoneNumber,
                                                              @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.reservationDayEnd >= :currentDate AND" +
            " r.deleted = false AND " +
            " r.accommodationPaid = false AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getActiveUnpaidReservationsBySearchParameters(Pageable pageable,
                                                                  @Param("currentDate") LocalDate aCurrentDate,
                                                                  @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                                  @Param("firstName") String aFirstName,
                                                                  @Param("lastName") String aLastName,
                                                                  @Param("reservationDayEnd") String aReservationDayEnd,
                                                                  @Param("reservationDayStart") String aReservationDayStart,
                                                                  @Param("email") String aEmail,
                                                                  @Param("phoneNumber") String aPhoneNumber,
                                                                  @Param("reservationType") String aReservationType);

    @Query("SELECT r FROM Reservation r INNER JOIN ReservationDetails rd ON r.id = rd.reservation.id" +
            " WHERE " +
            " r.deleted = true AND " +
            " CAST(DATE(r.dateOfAddingReservation) AS string) LIKE :dateOfAddingReservation AND " +
            " UPPER(r.firstName) LIKE UPPER(:firstName) AND " +
            " UPPER(r.lastName) LIKE UPPER(:lastName) AND " +
            " CAST(r.reservationDayEnd AS string) LIKE :reservationDayEnd AND " +
            " CAST(r.reservationDayStart AS string) LIKE :reservationDayStart AND " +
            " rd.email LIKE :email AND " +
            " rd.phoneNumber LIKE :phoneNumber AND " +
            " CAST(rd.reservationType AS string) LIKE :reservationType " +
            " ORDER BY r.reservationDayStart ASC")
    Page<Reservation> getDeletedReservationsBySearchParameters(Pageable pageable,
                                                               @Param("dateOfAddingReservation") String aDateOfAddingReservation,
                                                               @Param("firstName") String aFirstName,
                                                               @Param("lastName") String aLastName,
                                                               @Param("reservationDayEnd") String aReservationDayEnd,
                                                               @Param("reservationDayStart") String aReservationDayStart,
                                                               @Param("email") String aEmail,
                                                               @Param("phoneNumber") String aPhoneNumber,
                                                               @Param("reservationType") String aReservationType);

    // **************************************************** METHODS FOR DISPLAYING OVERVIEW ******************************************* //

    @Query(value = "SELECT COUNT(*) FROM reservation " +
            "WHERE DATE(reservation.date_of_adding_reservation) = :currentDate AND reservation.deleted = FALSE",
            nativeQuery = true)
    Long getNumberOfNewReservations(@Param("currentDate") final LocalDate aCurrentDate);


    @Query(value = "SELECT COUNT(*) FROM reservation " +
            "WHERE reservation.reservation_day_start = :currentDate AND reservation.deleted = FALSE",
            nativeQuery = true)
    Long getNumberOfTodayReservations(@Param("currentDate") final LocalDate aCurrentDate);

    @Query(value = "SELECT SUM(people) FROM reservation " +
            "WHERE :currentDate >= reservation.reservation_day_start AND :currentDate <  reservation.reservation_day_end " +
            "AND reservation.deleted = FALSE;",
            nativeQuery = true)
    Long getNumberOfPeoplePerDay(@Param("currentDate") final LocalDate aCurrentDate);

    @Query(value = "SELECT COUNT(*) FROM reservation " +
            "WHERE reservation.reservation_day_end = :currentDate " +
            "AND reservation.deleted = FALSE",
            nativeQuery = true)
    Long getNumberOfOutReservations(@Param("currentDate") final LocalDate aCurrentDate);


    // **************************************************** METHODS FOR DISPLAYING DASHBOARD ******************************************* //

    @Query("SELECT r FROM Reservation r WHERE r.reservationDayStart = :currentDate " +
            "AND r.deleted = FALSE" +
            " ORDER BY r.id")
    List<Reservation> getComingReservations(@Param("currentDate") LocalDate aCurrentDate);

    @Query("SELECT r FROM Reservation r WHERE DATE(r.dateOfAddingReservation) = :currentDate " +
            "AND r.deleted = FALSE" +
            " ORDER BY r.id")
    List<Reservation> getTodayReservations(@Param("currentDate") LocalDate aCurrentDate);

    @Query(value = "SELECT r FROM Reservation r WHERE :currentDate BETWEEN r.reservationDayStart AND reservation_end - INTERVAL 1 DAY " +
            "AND reservation.deleted = FALSE",
            nativeQuery = true)
    List<Reservation> getOngoingReservations(@Param("currentDate") LocalDate currentDate);

    @Query(value = "SELECT * FROM reservation WHERE :currentDate BETWEEN reservation_day_start AND reservation_day_end - INTERVAL '1 DAY' " +
            "AND reservation.deleted = FALSE",
            nativeQuery = true)
    List<Reservation> getCurrentGuests(@Param("currentDate") LocalDate currentDate);

    // **************************************************** PAYMENT PURPOSES ******************************************* //

    Reservation findAllBySessionId(final String sessionId);

    // **************************************************** REPORT PURPOSES ******************************************* //

    @Query("SELECT r FROM Reservation r WHERE :reportDay = r.dateOfAddingReservation AND r.deleted = false AND " +
            "r.reservationDetails.reservationType = :reservationType")
    List<Reservation> getReservationsForReport(final LocalDate reportDay, final String reservationType);

    // **************************************************** UTILS ******************************************* //

    @Query(value = "SELECT * FROM reservation WHERE :currentDate BETWEEN reservation_day_start AND reservation_day_end - INTERVAL '1 DAY' " +
            "AND reservation.deleted = FALSE",
            nativeQuery = true)
    List<Reservation> getReservationsOnShelter(final LocalDate currentDate);


    @Query(value = "SELECT * FROM reservation WHERE" +
            " ( :dateFrom >= reservation_day_start AND :dateFrom < reservation_day_end ) OR" +
            " ( :dateTo >= reservation_day_start AND :dateTo < reservation_day_end )",
    nativeQuery = true)
    List<Reservation> findReservationsByDateBetween(@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateFrom,
                                                    @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateTo);


    @Query("SELECT r FROM Reservation r WHERE "
            + "FUNCTION('YEAR', r.reservationDayStart) = :year "
            + "AND FUNCTION('MONTH', r.reservationDayStart) = :month "
            + "AND FUNCTION('DAY', r.reservationDayStart) >= 1 "
            + "AND FUNCTION('DAY', r.reservationDayStart) <= FUNCTION('LAST_DAY', r.reservationDayStart)")
    List<Reservation> findReservationsBetweenDates(final int year, final int month);
}