package lynx.code.wielkaracza.reservation;

import lombok.Builder;
import lynx.code.wielkaracza.common.date.DateDTO;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.reservation.domain.ReservationType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface ReservationOverviewFacade {

    ReservationOverviewDTO getReservationOverview(final DateDTO date);

    List<ReservationBasicInfo> getReservationComings(final DateDTO date);

    List<ReservationBasicInfo> getReservationForToday(final DateDTO date);

    List<ReservationBasicInfo> getOngoingReservations(final DateDTO date);

    List<ReservationGuestDTO> getCurrentGuests(final DateDTO dto);

    List<Reservation> getReservationByDatesAndRoomNumber(LocalDate dateFrom, LocalDate dateTo, Integer roomNumber);

    record ReservationOverviewDTO(Long newReservations,
                                  Long todayReservations,
                                  Long numberOfPeoplePerDay,
                                  Long outReservations) {

    }

    @Builder
    record ReservationBasicInfo(UUID uuid,
                                Character firstLetterOfSurname,
                                String firstName,
                                String lastName,
                                int howManyPeople,
                                List<Integer> roomNumbers,
                                LocalDate reservationStart,
                                LocalDate reservationEnd,
                                ReservationType reservationType) {
    }

    @Builder
    record ReservationGuestDTO(UUID uuid,
                               String firstName,
                               String lastName,
                               int numberOfCompanies,
                               String number,
                               LocalDate startDate,
                               LocalDate finishDate,
                               List<Integer> roomNumbers,
                               Boolean arrived,
                               UUID reservationUuid,
                               ReservationType reservationType,
                               BigDecimal amountToPay,
                               BigDecimal alreadyPaid) {
    }
}
