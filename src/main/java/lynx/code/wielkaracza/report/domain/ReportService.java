package lynx.code.wielkaracza.report.domain;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.exception.NotFoundException;
import lynx.code.wielkaracza.report.ReportFacade;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.reservation.domain.ReservationQueryRepository;
import lynx.code.wielkaracza.reservation.domain.ReservationType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
class ReportService implements ReportFacade {

    @UtilityClass
    static final class ErrorMessages {
        static final String REPORT_DOES_NOT_EXIST = "Report with UUID: %s does not exist!";
    }

    private final ReportQueryRepository reportQueryRepository;
    private final ReportRepository reportRepository;
    private final ReservationQueryRepository reservationQueryRepository;

    @Override
    public List<ReportDTO> getReports() {
        return reportQueryRepository.findAll().stream()
                .map(ReportMapper::toDTO)
                .toList();
    }

    @Override
    public ReportDTO getReport(final UUID uuid) {
        return reportQueryRepository.findByUuid(uuid)
                .map(ReportMapper::toDTO)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.REPORT_DOES_NOT_EXIST));
    }

    @Override
    @Scheduled(cron = "0 0 4 * * *")
    public void saveReport() {
        final var reportDay = LocalDate.now().minusDays(1);
        final var reservations = reservationQueryRepository
                .getReservationsForReport(reportDay, ReservationType.INTERNET_RESERVATION.name());
        final var cash = sumCashFromReservations(reservations);

        final var report =
                Report.of(reportDay, reportDay.getDayOfWeek(), cash, cash.divide(BigDecimal.valueOf(4), RoundingMode.HALF_UP));

        reportRepository.save(report);
    }

    private BigDecimal sumCashFromReservations(final List<Reservation> reservations) {
        return BigDecimal.valueOf(reservations.stream()
                .map(reservation ->
                        reservation.getAmountToPay().divide(BigDecimal.valueOf(2), RoundingMode.HALF_UP))
                .map(BigDecimal::doubleValue)
                .mapToDouble(Double::doubleValue)
                .sum());
    }
}
