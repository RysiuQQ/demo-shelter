package lynx.code.wielkaracza.report.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.report.ReportFacade.ReportDTO;

@UtilityClass
class ReportMapper {

    ReportDTO toDTO(final Report report) {
        return ReportDTO.builder()
                .date(report.getDate())
                .dayOfWeek(report.getDayOfWeek())
                .cash(report.getCash())
                .cashPerPerson(report.getCashPerPerson())
                .build();
    }
}
