package lynx.code.wielkaracza.report.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;

interface ReportQueryRepository extends UuidAwareJpaQueryRepository<Report, Long> {
}
