package lynx.code.wielkaracza.report.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaRepository;

interface ReportRepository extends UuidAwareJpaRepository<Report, Long> {
}
