package lynx.code.wielkaracza.report.controller;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.report.ReportFacade;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.report.ReportFacade.ReportDTO;
import static lynx.code.wielkaracza.report.controller.ReportController.Routes.ROOT;
import static lynx.code.wielkaracza.report.controller.ReportController.Routes.SINGLE_REPORT;

@RestController
@RequiredArgsConstructor
@CrossOrigin(originPatterns = "*")
class ReportController {

    @UtilityClass
    static final class Routes {
        static final String ROOT = "/api/reports";
        static final String SINGLE_REPORT = "/api/reports/{uuid}";
    }

    private final ReportFacade reportFacade;

    @GetMapping(ROOT)
    @PreAuthorize("hasRole('ADMIN')")
    List<ReportDTO> getReports() {
        return reportFacade.getReports();
    }

    @GetMapping(SINGLE_REPORT)
    @PreAuthorize("hasRole('ADMIN')")
    ReportDTO getReports(@PathVariable final UUID uuid) {
        return reportFacade.getReport(uuid);
    }
}
