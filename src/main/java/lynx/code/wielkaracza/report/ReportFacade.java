package lynx.code.wielkaracza.report;

import lombok.Builder;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface ReportFacade {

    List<ReportDTO> getReports();

    ReportDTO getReport(final UUID uuid);

    void saveReport();

    @Builder
    record ReportDTO(LocalDate date,
                     DayOfWeek dayOfWeek,
                     BigDecimal cash,
                     BigDecimal cashPerPerson) {

    }
}

