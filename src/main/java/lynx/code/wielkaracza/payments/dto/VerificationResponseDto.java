package lynx.code.wielkaracza.payments.dto;

import lombok.Data;

@Data
public class VerificationResponseDto {
    private DataVerificationStatusDto data;
    private String responseCode;
}
