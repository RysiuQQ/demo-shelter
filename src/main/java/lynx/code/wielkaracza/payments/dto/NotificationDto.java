package lynx.code.wielkaracza.payments.dto;

import lombok.Data;

@Data
public class NotificationDto {

    private Long merchantId;
    private Long posId;
    private String sessionId;
    private Long amount;
    private Long originAmount;
    private String currency;
    private Long orderId;
    private Long methodId;
    private String statement;
    private String sign;
}
