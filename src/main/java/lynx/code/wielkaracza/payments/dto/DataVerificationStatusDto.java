package lynx.code.wielkaracza.payments.dto;

import lombok.Data;

@Data
public class DataVerificationStatusDto {
    private String status;
}
