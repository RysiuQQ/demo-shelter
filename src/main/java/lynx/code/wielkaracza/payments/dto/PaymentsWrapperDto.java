package lynx.code.wielkaracza.payments.dto;

import lombok.Data;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade;

@Data
public class PaymentsWrapperDto {
    private ReservationCRUDFacade.ReservationDTO reservationDto;
    private String paymentUrl;

    public PaymentsWrapperDto(ReservationCRUDFacade.ReservationDTO reservationDto, String paymentUrl) {
        this.reservationDto = reservationDto;
        this.paymentUrl = paymentUrl;
    }
}
