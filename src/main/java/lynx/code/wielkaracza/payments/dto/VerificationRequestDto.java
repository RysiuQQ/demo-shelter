package lynx.code.wielkaracza.payments.dto;

import lombok.Data;

@Data
public class VerificationRequestDto {
    private Long merchantId;
    private Long posId;
    private String sessionId;
    private Long amount;
    private String currency;
    private Long orderId;
    private String sign;

    public VerificationRequestDto(NotificationDto notificationDto) {
        this.merchantId = notificationDto.getMerchantId();
        this.posId = notificationDto.getPosId();
        this.sessionId = notificationDto.getSessionId();
        this.amount = notificationDto.getAmount();
        this.currency = notificationDto.getCurrency();
        this.orderId = notificationDto.getOrderId();
    }
}
