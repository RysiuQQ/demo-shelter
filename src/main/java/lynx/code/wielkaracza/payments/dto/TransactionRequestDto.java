package lynx.code.wielkaracza.payments.dto;

import lombok.Data;

@Data
public class TransactionRequestDto {

    private Long merchantId;
    private Long posId;
    private String crc;
    private String sessionId;
    private Long amount;
    private String currency;
    private String description;
    private String email;
    private String client;  // max 40
    private String country;
    private String language;
    private String urlReturn;
    private String urlStatus;
    private Long timeLimit;
    private Long channel;
    private Boolean waitForResult;
    private String transferLabel;
    private String sign;
}
