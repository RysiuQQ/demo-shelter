package lynx.code.wielkaracza.payments.dto;

import lombok.Data;

@Data
public class DataTokenResponseDto {
    private String token;
}
