package lynx.code.wielkaracza.payments.dto;

import lombok.Data;

@Data
public class TokenResponseDto {
    private DataTokenResponseDto data;
    private String responseCode;
}
