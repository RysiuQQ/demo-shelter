package lynx.code.wielkaracza.payments.service;

import lynx.code.wielkaracza.payments.dto.NotificationDto;
import lynx.code.wielkaracza.payments.dto.TransactionRequestDto;
import lynx.code.wielkaracza.reservation.domain.Reservation;

public interface PaymentsService {
    String getPaymentUrl(Reservation reservation);
    TransactionRequestDto transactionRequestDtoBuilder(Reservation reservation);
    void handlePaymentNotification(NotificationDto notificationDto);
}