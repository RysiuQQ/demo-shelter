package lynx.code.wielkaracza.payments.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.email.EmailHandlerService;
import lynx.code.wielkaracza.payments.config.Przelewy24Configuration;
import lynx.code.wielkaracza.payments.dto.NotificationDto;
import lynx.code.wielkaracza.payments.dto.TokenResponseDto;
import lynx.code.wielkaracza.payments.dto.TransactionRequestDto;
import lynx.code.wielkaracza.payments.dto.VerificationRequestDto;
import lynx.code.wielkaracza.payments.dto.VerificationResponseDto;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.reservation.domain.ReservationMapper;
import lynx.code.wielkaracza.reservation.domain.ReservationQueryRepository;
import lynx.code.wielkaracza.reservation.domain.ReservationRepository;
import lynx.code.wielkaracza.security.jwt.AuthTokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class PaymentsServiceImpl implements PaymentsService {
    private final Przelewy24Configuration config;
    private final EmailHandlerService emailHandlerService;
    private final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);
    private final String PRODUCTION_URL = "https://secure.przelewy24.pl";
    private final String TEST_URL = "https://sandbox.przelewy24.pl";
    private final String URI_REGISTER = "/api/v1/transaction/register";
    private final String URI_REQUEST = "/trnRequest/";
    private final String URI_VERIFY = "/api/v1/transaction/verify";
    private final ReservationQueryRepository reservationQueryRepository;
    private final ReservationRepository reservationRepository;

    @Override
    public String getPaymentUrl(Reservation reservation) {
        try {
            TransactionRequestDto transactionRequestDto = transactionRequestDtoBuilder(reservation);
            logger.info("[PAYMENTS] [INFO] Payments info transactionRequestDto: {}", transactionRequestDto);

            String environmentUrl = getEnvironmentUrl(config);

            String token = getToken(transactionRequestDto, config, environmentUrl);

            if (token != null && token.length() != 0) {
                logger.info("[PAYMENTS] Payment link has been generated successfully for reservation [session id] =  {}", reservation.getSessionId());
                return getPaymentLink(token, environmentUrl);
            } else {
                logger.error("[PAYMENTS] We could not get token!");
                throw new Exception();
            }

        } catch (Exception e) {
            logger.error("[PAYMENTS] [ERROR]: Error with payment link generation:  {}", e);
            return "";
        }
    }

    private String getPaymentLink(String token, String environmentUrl) {
        return environmentUrl + URI_REQUEST + token;
    }

    @Override
    public TransactionRequestDto transactionRequestDtoBuilder(Reservation reservation) {
        TransactionRequestDto transactionRequestDto = new TransactionRequestDto();

        transactionRequestDto.setCrc(config.getCrc());
        transactionRequestDto.setMerchantId(config.getMerchantId());
        transactionRequestDto.setPosId(config.getPosId());
        transactionRequestDto.setUrlReturn(config.getUrlReturn());
        transactionRequestDto.setUrlStatus(config.getUrlStatus());

        transactionRequestDto.setSessionId(reservation.getSessionId());
        transactionRequestDto.setAmount((reservation.getAmountToPay().longValue() * 100) / 2);
        transactionRequestDto.setCurrency("PLN");
        transactionRequestDto.setDescription(String.format("Rezerwacja[id]=%s, Session[id]=%s", reservation.getUuid().toString(), reservation.getSessionId()));
        transactionRequestDto.setEmail(reservation.getReservationDetails().getEmail());
        transactionRequestDto.setCountry("PL");
        transactionRequestDto.setLanguage("pl");
        transactionRequestDto.setTimeLimit(10L);
        transactionRequestDto.setChannel(64L);
        transactionRequestDto.setWaitForResult(false);
        transactionRequestDto.setTransferLabel(config.getTransferLabel());
        transactionRequestDto.setSign(getSign(transactionRequestDto));

        return transactionRequestDto;
    }

    @Override
    public void handlePaymentNotification(NotificationDto notificationDto) {
        logger.info("[PAYMENTS] [INFO]: Payment notification has been received form przelewy24: {}", notificationDto);

        String environmentUrl = getEnvironmentUrl(config);
        Reservation reservationToUpdate = reservationQueryRepository.findAllBySessionId(notificationDto.getSessionId());

        if (reservationToUpdate != null) {
            try {
                VerificationRequestDto verificationRequestDto = new VerificationRequestDto(notificationDto);
                verificationRequestDto.setSign(getSign(verificationRequestDto, config.getCrc()));

                logger.info("[PAYMENTS] Verification request has been created: {}", verificationRequestDto);

                String getVerificationUrl = environmentUrl + URI_VERIFY;
                String credentials = Base64.getEncoder().encodeToString(((String.format("%d:%s", config.getMerchantId(), config.getKeyToReports()))).getBytes());

                ObjectMapper mapper = new ObjectMapper();
                String requestBody = mapper.writeValueAsString(verificationRequestDto);

                HttpClient client = HttpClient.newHttpClient();
                HttpRequest request = HttpRequest
                        .newBuilder(URI.create(getVerificationUrl))
                        .header("Content-Type", "application/json")
                        .PUT(HttpRequest.BodyPublishers.ofString(requestBody))
                        .setHeader("Authorization", "Basic " + credentials)
                        .build();

                VerificationResponseDto verificationResponseDto = client.send(request, new JsonBodyHandler<>(VerificationResponseDto.class)).body();

                logger.info("[PAYMENTS] Verification response has been received: {}", verificationResponseDto);

                if ("success".equals(verificationResponseDto.getData().getStatus())) {
                    reservationToUpdate.setAccommodationPaid(true);
                    reservationRepository.save(reservationToUpdate);

                    logger.info("[PAYMENTS] The payment with amount {}PLN has been detected in our system ", reservationToUpdate.getAmountToPay().longValue()/2);

                    ReservationCRUDFacade.ReservationDTO reservationDTOToSend = ReservationMapper.toDTO(reservationToUpdate);
                    emailHandlerService.sendSuccessEmailReservation(reservationDTOToSend);
                } else {
                    logger.error("[PAYMENTS] [ERROR]: We couldn't confirm payment. Response: ", verificationResponseDto);
                }

            } catch (Exception e) {
                logger.error("[PAYMENTS] [CRITICAL ERROR]: Problem with payments confirmation: ", e);
            }
        } else {
            logger.error("[PAYMENTS] [CRITICAL ERROR]: reservationToUpdate equals null!! ");
        }
    }


    private String getEnvironmentUrl(Przelewy24Configuration config) {
        if (config.isProduction()) {
            return PRODUCTION_URL;
        } else {
            return TEST_URL;
        }
    }

    private String getSign(TransactionRequestDto transactionRequestDto) {
        String signToHash = String.format("{\"sessionId\":\"%s\",\"merchantId\":%d,\"amount\":%d,\"currency\":\"%s\",\"crc\":\"%s\"}",
                transactionRequestDto.getSessionId(),
                transactionRequestDto.getMerchantId(),
                transactionRequestDto.getAmount(),
                transactionRequestDto.getCurrency(),
                transactionRequestDto.getCrc());

        return hashSHA384(signToHash);
    }

    private String getSign(VerificationRequestDto verificationRequestDto, String crc) {
        String signToHash = String.format("{\"sessionId\":\"%s\",\"orderId\":%d,\"amount\":%d,\"currency\":\"%s\",\"crc\":\"%s\"}",
                verificationRequestDto.getSessionId(),
                verificationRequestDto.getOrderId(),
                verificationRequestDto.getAmount(),
                verificationRequestDto.getCurrency(),
                crc);

        return hashSHA384(signToHash);
    }

    public static String hashSHA384(String input) {
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-384");
            crypt.update(input.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = crypt.digest();
            BigInteger bi = new BigInteger(1, bytes);
            return String.format("%0" + (bytes.length << 1) + "x", bi);

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String getToken(TransactionRequestDto transactionRequestDto, Przelewy24Configuration config, String environmentUrl) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String requestBody = mapper.writeValueAsString(transactionRequestDto);

        String getTokenUrl = environmentUrl + URI_REGISTER;
        String credentials = Base64.getEncoder().encodeToString(((String.format("%d:%s", config.getMerchantId(), config.getKeyToReports()))).getBytes());

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest
                .newBuilder(URI.create(getTokenUrl))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .setHeader("Authorization", "Basic " + credentials)
                .build();

        TokenResponseDto tokenResponseDto = client.send(request, new JsonBodyHandler<>(TokenResponseDto.class)).body();
        return tokenResponseDto.getData().getToken();
    }
}
