package lynx.code.wielkaracza.payments.controller;

import lynx.code.wielkaracza.payments.dto.NotificationDto;
import lynx.code.wielkaracza.payments.service.PaymentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/payments")
public class PaymentsController {
    private PaymentsService paymentsService;

    @Autowired
    public PaymentsController(PaymentsService paymentsService) {
        this.paymentsService = paymentsService;
    }

    @PostMapping("/notification")
    public void getPaymentNotification(@RequestBody NotificationDto notificationDto) {
        paymentsService.handlePaymentNotification(notificationDto);
    }
}