package lynx.code.wielkaracza.payments.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:payments.properties")
@Data
public class Przelewy24Configuration {

    @Value("${payment.przelewy24.crc}")
    private String crc;
    @Value("${payment.przelewy24.merchant-id}")
    private Long merchantId;
    @Value("${payment.przelewy24.pos-id}")
    private Long posId;
    @Value("${payment.przelewy24.is-production}")
    private boolean isProduction = false;
    @Value("${payment.przelewy24.url-return}")
    private String urlReturn;
    @Value("${payment.przelewy24.url-status}")
    private String urlStatus;
    @Value("${payment.przelewy24.key-to-reports}")
    private String keyToReports;
    @Value("${payment.przelewy24.transfer-label}")
    private String transferLabel;

}
