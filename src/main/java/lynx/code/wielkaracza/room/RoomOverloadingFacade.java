package lynx.code.wielkaracza.room;

import lombok.Builder;
import lynx.code.wielkaracza.common.date.DateDTO;
import lynx.code.wielkaracza.reservation.domain.ReservationType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface RoomOverloadingFacade {

    List<RoomOverloadingDTO> getRoomsOverloading(final DateDTO date);

    int getShelterOverloading(final DateDTO date);

    List<RoomFutureAvailabilityDTO> getFutureAvailability(final UUID roomUuid, final DateDTO date);

    @Builder
    record RoomOverloadingDTO(int roomNumber,
                              int maxPeople,
                              String name,
                              int numberOfPeople,
                              LocalDate reservationDayStart,
                              LocalDate reservationDayEnd,
                              String phoneNumber,
                              BigDecimal amountToPay,
                              BigDecimal amountAlreadyPaid,
                              ReservationType reservationType,
                              UUID reservationUuid,
                              UUID roomUuid) {
    }

    record RoomFutureAvailabilityDTO(LocalDate date,
                                     int takenPlaces,
                                     boolean isAvailable) {
    }
}
