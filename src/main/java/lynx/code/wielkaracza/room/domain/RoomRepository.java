package lynx.code.wielkaracza.room.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface RoomRepository extends UuidAwareJpaRepository<Room, Long> {
}
