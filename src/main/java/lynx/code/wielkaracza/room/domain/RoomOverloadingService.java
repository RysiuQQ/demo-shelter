package lynx.code.wielkaracza.room.domain;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.common.date.DateDTO;
import lynx.code.wielkaracza.common.date.DateFormatter;
import lynx.code.wielkaracza.common.date.DateUtils;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.reservation.domain.ReservationQueryRepository;
import lynx.code.wielkaracza.room.RoomCRUDFacade;
import lynx.code.wielkaracza.room.RoomOverloadingFacade;
import lynx.code.wielkaracza.unavailableterm.UnavailableQueryRepository;
import lynx.code.wielkaracza.unavailableterm.UnavailableTerm;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.BooleanUtils.isFalse;

@Service
@RequiredArgsConstructor
class RoomOverloadingService implements RoomOverloadingFacade {

    private final RoomQueryRepository roomQueryRepository;
    private final ReservationQueryRepository reservationQueryRepository;
    private final UnavailableQueryRepository unavailableQueryRepository;
    private final RoomCRUDFacade roomCRUDFacade;

    @Override
    public List<RoomOverloadingDTO> getRoomsOverloading(final DateDTO date) {
        final var roomOverloadingDTOs = new ArrayList<RoomOverloadingDTO>();
        roomQueryRepository.findAll()
                .forEach(room ->
                        unavailableQueryRepository.getExactDates(LocalDate.now(), room.getId(),
                                        DateFormatter.formatStringToDate(date.providedDate()))
                                .stream()
                                .map(UnavailableTerm::getReservation)
                                .filter(reservation -> isFalse(reservation.isDeleted()))
                                .flatMap(reservation -> reservation.getProductRooms().stream())
                                .distinct()
                                .map(RoomOverloadingMapper::toDTO)
                                .forEach(roomOverloadingDTOs::add));

        return filterDuplicates(roomOverloadingDTOs);
    }

    @Override
    public int getShelterOverloading(final DateDTO date) {
        return reservationQueryRepository.getReservationsOnShelter(DateUtils.getCurrentDate(date))
                .stream()
                .map(Reservation::getProductRooms)
                .flatMap(Set::stream)
                .map(ProductRoom::getNumberOfPeople)
                .mapToInt(Integer::intValue)
                .sum();
    }

    @Override
    public List<RoomFutureAvailabilityDTO> getFutureAvailability(final UUID roomUuid, final DateDTO date) {
        final var room = roomCRUDFacade.getRoomOrThrow(roomUuid);
        return IntStream.range(0, 31)
                .mapToObj(i -> createRoomFutureAvailabilityDTO(room, i, date))
                .toList();
    }

    private List<RoomOverloadingDTO> filterDuplicates(final List<RoomOverloadingDTO> roomOverloadingDTOs) {
        List<RoomOverloadingDTO> uniqueReservations = new ArrayList<>();
        for (final RoomOverloadingDTO reservation : roomOverloadingDTOs) {
            boolean isDuplicate = false;
            for (final RoomOverloadingDTO uniqueReservation : uniqueReservations) {
                if (reservation.roomUuid() == uniqueReservation.roomUuid() &&
                        reservation.reservationUuid().equals(uniqueReservation.reservationUuid())) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                uniqueReservations.add(reservation);
            }
        }
        return uniqueReservations;
    }

    private RoomFutureAvailabilityDTO createRoomFutureAvailabilityDTO(final Room room, final int daysToAdd,
                                                                      final DateDTO date) {
        final var formattedDate = DateFormatter.formatStringToDate(date.providedDate());
        final var takenPlaces = countTakenPlaces(room, daysToAdd, formattedDate);
        return new RoomFutureAvailabilityDTO(formattedDate.plusDays(daysToAdd), takenPlaces,
                takenPlaces < room.getMaxPeople());
    }

    private int countTakenPlaces(final Room room, final int daysToAdd, final LocalDate formattedDate) {
        return unavailableQueryRepository.getExactDates(LocalDate.now(), room.getId(),
                        formattedDate.plusDays(daysToAdd))
                .stream()
                .map(UnavailableTerm::getReservation)
                .filter(reservation -> isFalse(reservation.isDeleted()))
                .flatMap(reservation -> reservation.getProductRooms().stream())
                .filter(productRoom -> productRoom.getRoom().getRoomNumber() == room.getRoomNumber())
                .mapToInt(ProductRoom::getNumberOfPeople)
                .sum();
    }
}