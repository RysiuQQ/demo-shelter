package lynx.code.wielkaracza.room.domain;

import lombok.*;
import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;
import lynx.code.wielkaracza.room.RoomCRUDFacade;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "room_blocked_date")
public class RoomBlockedDate extends AbstractUUIDEntity {

    @Column(name = "date_from", nullable = false)
    @NotNull(message = "dateFrom is required")
    @FutureOrPresent(message = "dateFrom must be at least current day")
    LocalDate dateFrom;

    @NotNull(message = "dateFrom is required")
    @Column(name = "date_to", nullable = false)
    LocalDate dateTo;

    @Column(name = "creation_date", nullable = false)
    LocalDateTime creationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;

    public RoomBlockedDate(RoomCRUDFacade.BlockRoomRequestDto dto, Room room) {
        this.dateFrom = dto.dateFrom();
        this.dateTo = dto.dateTo();
        this.creationDate = LocalDateTime.now();
        this.room = room;
    }
}