package lynx.code.wielkaracza.room.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.unavailableterm.UnavailableTerm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "room")
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Room extends AbstractUUIDEntity {

    @Column(name = "description_pl", nullable = false, length = 200)
    private String descriptionPL;

    @Column(name = "description_en", nullable = false, length = 200)
    private String descriptionEN;

    @Column(name = "description_sk", nullable = false, length = 200)
    private String descriptionSK;

    @Column(name = "title_pl", nullable = false, length = 200)
    private String titlePL;

    @Column(name = "title_en", nullable = false, length = 200)
    private String titleEN;

    @Column(name = "title_sk", nullable = false, length = 200)
    private String titleSK;

    @Column(nullable = false)
    private int maxPeople;

    @Column(nullable = false)
    private int roomNumber;

    @Column(nullable = false)
    private boolean bathroom;

    @Column(nullable = false)
    private BigDecimal price;

    @Column(nullable = false)
    private Integer numberOfNormalBeds;

    @Column(nullable = false)
    private Integer numberOfDoubleBeds;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private RoomPriority priority;

    @OneToMany(mappedBy = UnavailableTerm.Fields.room)
    private Set<UnavailableTerm> unavailableTerms;

    @OneToMany(mappedBy = ProductRoom.Fields.room)
    private Set<ProductRoom> productRooms;

    @ManyToMany(mappedBy = Reservation.Fields.rooms)
    private Set<Reservation> reservations;

    @OneToMany(mappedBy = UnavailableTerm.Fields.room)
    private List<RoomBlockedDate> blockedDates;
}