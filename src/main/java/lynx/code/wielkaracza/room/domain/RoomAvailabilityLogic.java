package lynx.code.wielkaracza.room.domain;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.common.searchingengine.Path;
import lynx.code.wielkaracza.common.searchingengine.SearchingUtils;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.unavailableterm.UnavailableQueryRepository;
import lynx.code.wielkaracza.unavailableterm.UnavailableTerm;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.util.Objects.nonNull;
import static lynx.code.wielkaracza.room.RoomCRUDFacade.ArrivalForm;

@Component
@RequiredArgsConstructor
public class RoomAvailabilityLogic {

    private final RoomQueryRepository roomQueryRepository;
    private final UnavailableQueryRepository unavailableQueryRepository;

    public Map<Room, Integer> findAvailableRooms(final ArrivalForm arrivalForm, final Reservation reservation) {
        final var rooms = roomQueryRepository.findAll();
        final Map<Room, Integer> roomWithAvailablePlaces = new HashMap<>();

        final var unavailableTermListReservation = nonNull(reservation)
                ? reservation.getUnavailableTerms() : null;

        for (final Room room : rooms) {
            final var datesToCheck = new ArrayList<UnavailableTerm>();
            final var unavailableTerms = unavailableQueryRepository.getFutureDates(arrivalForm.reservationDayStart(), room.getUuid(),
                    arrivalForm.reservationDayEnd());

            if (nonNull(reservation)) {
                unavailableTerms.forEach(unavailableTerm ->
                        addYearsToEditedReservationUnavailableTerms(unavailableTerm, unavailableTermListReservation));
            }

            final List<RoomBlockedDate> blockedDates = new ArrayList<>();

            checkBlockedDates(blockedDates, arrivalForm, room.getBlockedDates());

            if (!blockedDates.isEmpty()) continue;

            if (unavailableTerms.isEmpty()) {
                roomWithAvailablePlaces.put(room, room.getMaxPeople());
                continue;
            }

            checkSpecificUnavailableTerm(datesToCheck, arrivalForm, unavailableTerms);

            if (datesToCheck.isEmpty()) {
                roomWithAvailablePlaces.put(room, room.getMaxPeople());
                continue;
            }

            final var paths = prepareDataPaths(datesToCheck, room);
            final var freePlaces = SearchingUtils.countAmountOfPeople(paths, room);

            if (freePlaces > 0) {
                roomWithAvailablePlaces.put(room, freePlaces);
            }
        }

        removeNotEmptyRoomWhenReservationIncludesDog(roomWithAvailablePlaces, arrivalForm);

        return roomWithAvailablePlaces;
    }

    private void addYearsToEditedReservationUnavailableTerms(final UnavailableTerm unavailableTerm,
                                                             final Set<UnavailableTerm> unavailableTermListReservation) {
        for (final UnavailableTerm term : unavailableTermListReservation) {
            if (unavailableTerm.getUuid().equals(term.getUuid())) {
                unavailableTerm.setReservationDayEnd(unavailableTerm.getReservationDayEnd().plusYears(300));
                unavailableTerm.setReservationDayStart(unavailableTerm.getReservationDayStart().plusYears(300));
            }
        }
    }

    private void checkSpecificUnavailableTerm(final List<UnavailableTerm> datesToCheck, final ArrivalForm arrivalForm,
                                              final List<UnavailableTerm> unavailableTerms) {
        final var reservationDayStart = arrivalForm.reservationDayStart();
        final var reservationDayEnd = arrivalForm.reservationDayEnd();

        for (final UnavailableTerm unavailableTerm : unavailableTerms) {
            if (unavailableTerm.getReservationDayStart().isAfter(reservationDayStart)) {

                if (reservationDayEnd.isEqual(unavailableTerm.getReservationDayStart())) {
                    continue;
                }

                if (!reservationDayEnd.isBefore(unavailableTerm.getReservationDayStart())) {
                    datesToCheck.add(unavailableTerm);
                }
            }
            if (unavailableTerm.getReservationDayStart().isBefore(reservationDayStart)) {

                if (reservationDayStart.isEqual(unavailableTerm.getReservationDayEnd())) {
                    continue;
                }

                if (!reservationDayStart.isAfter(unavailableTerm.getReservationDayEnd())) {
                    datesToCheck.add(unavailableTerm);
                }
            }

            if (unavailableTerm.getReservationDayStart().isEqual(reservationDayStart)) {
                datesToCheck.add(unavailableTerm);
            }
        }
    }

    private void checkBlockedDates(final List<RoomBlockedDate> datesToCheck, final ArrivalForm arrivalForm,
                                   final List<RoomBlockedDate> blockedDates) {
        final var reservationDayStart = arrivalForm.reservationDayStart();
        final var reservationDayEnd = arrivalForm.reservationDayEnd();

        for (final RoomBlockedDate blockedDate : blockedDates) {
            if (blockedDate.getDateFrom().isAfter(reservationDayStart)) {

                if (reservationDayEnd.isEqual(blockedDate.getDateFrom())) {
                    continue;
                }

                if (!reservationDayEnd.isBefore(blockedDate.getDateFrom())) {
                    datesToCheck.add(blockedDate);
                }
            }
            if (blockedDate.getDateFrom().isBefore(reservationDayStart)) {

                if (reservationDayStart.isEqual(blockedDate.getDateTo())) {
                    continue;
                }

                if (!reservationDayStart.isAfter(blockedDate.getDateTo())) {
                    datesToCheck.add(blockedDate);
                }
            }

            if (blockedDate.getDateFrom().isEqual(reservationDayStart)) {
                datesToCheck.add(blockedDate);
            }
        }
    }

    private List<Path> prepareDataPaths(final List<UnavailableTerm> dateToCompare, final Room room) {

        final var dataPaths = new ArrayList<>(dateToCompare.stream()
                .map(date -> SearchingUtils.prepareDataPaths(date, dateToCompare))
                .toList());

        dataPaths.forEach(path -> {
            final var unavailableTermsToRemove = new ArrayList<UnavailableTerm>();
            for (int i = 0; i < path.getUnavailableTerms().size() - 1; i++) {
                SearchingUtils.eliminateNotAssociatedData(room, unavailableTermsToRemove,
                        path.getUnavailableTerms().get(i), path.getUnavailableTerms().get(i + 1));
            }
            path.getUnavailableTerms().removeAll(unavailableTermsToRemove);
        });

        return dataPaths;
    }

    private void removeNotEmptyRoomWhenReservationIncludesDog(final Map<Room, Integer> roomWithAvailablePlaces,
                                                              final ArrivalForm arrivalForm) {
        if (arrivalForm.dogs()) {
            roomWithAvailablePlaces.entrySet().removeIf(roomIntegerEntry ->
                    roomIntegerEntry.getKey().getMaxPeople() != roomIntegerEntry.getValue());
        }
    }
}

