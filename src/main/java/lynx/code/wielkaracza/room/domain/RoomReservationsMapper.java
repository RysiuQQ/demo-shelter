package lynx.code.wielkaracza.room.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.room.RoomCRUDFacade.RoomReservationsDTO;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@UtilityClass
public class RoomReservationsMapper {

    public RoomReservationsDTO toDto(List<Reservation> reservations, Integer roomNumber) {
        return RoomReservationsDTO.builder()
                .roomNumber(roomNumber)
                .reservations(toReservationDTO(reservations))
                .build();
    }

    public List<RoomReservationsDTO.Reservation> toReservationDTO(List<Reservation> reservations) {
        List<RoomReservationsDTO.Reservation> reservationsDTO = new ArrayList<>();

        reservations.forEach(r -> {
            RoomReservationsDTO.Reservation reservationDto = RoomReservationsDTO.Reservation.builder()
                    .firstName(r.getFirstName())
                    .lastName(r.getLastName())
                    .email(r.getReservationDetails().getEmail())
                    .reservationDayStart(r.getReservationDayStart())
                    .reservationDayEnd(r.getReservationDayEnd())
                    .dateOfAddingReservation(r.getDateOfAddingReservation())
                    .build();
            reservationsDTO.add(reservationDto);
        });

        return reservationsDTO.stream()
                .sorted(Comparator.comparing(RoomReservationsDTO.Reservation::reservationDayStart).reversed())
                .toList();
    }

}
