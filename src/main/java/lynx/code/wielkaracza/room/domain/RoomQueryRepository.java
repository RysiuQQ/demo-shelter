package lynx.code.wielkaracza.room.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RoomQueryRepository extends UuidAwareJpaQueryRepository<Room, Long> {
    List<Room> findByUuidIn(List<UUID> UUIDs);
}
