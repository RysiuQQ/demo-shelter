package lynx.code.wielkaracza.room.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoomBlockedDateRepository extends UuidAwareJpaRepository<RoomBlockedDate, Long> {

    Optional<RoomBlockedDate> findByUuid(UUID uuid);
}