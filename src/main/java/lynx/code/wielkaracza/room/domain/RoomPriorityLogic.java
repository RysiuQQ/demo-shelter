package lynx.code.wielkaracza.room.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.exception.MoreThanPlacesException;
import lynx.code.wielkaracza.reservation.domain.ReservationGroup;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;

@Component
public class RoomPriorityLogic {

    @UtilityClass
    static final class ErrorMessages {
        static final String MORE_THAN_PLACES = "Shelter does not have this amount of places in general";
    }

    private final List<Integer> prioritizedRoomsNumbers = List.of(7, 2, 1, 5, 3);//rooms order from the highest priority to the lowest
    private final List<Integer> nonPrioritizedRoomsNumbers = List.of(4, 6, 8, 9);


    public Map<Room, Integer> displayRoomsByPriority(final Map<Room, Integer> roomWithFreePlaces,
                                                     final int peopleToAccommodate) {
        final Map<Room, Integer> roomWithFreePlacesToDisplayByPriority = new HashMap<>();

        if (peopleToAccommodate < ReservationGroup.SMALL_GROUP.getNumberOfPeopleTopBorder()) {
            checkPerfectMatch(roomWithFreePlaces, peopleToAccommodate, roomWithFreePlacesToDisplayByPriority);

            final var rooms = roomWithFreePlaces.keySet().stream()
                    .filter(room -> room.getRoomNumber() == prioritizedRoomsNumbers.get(0) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(1) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(2) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(3) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(4))
                    .toList();

            putSelectedRooms(rooms, roomWithFreePlaces, roomWithFreePlacesToDisplayByPriority, peopleToAccommodate);


        } else if (peopleToAccommodate < ReservationGroup.MEDIUM_GROUP.getNumberOfPeopleTopBorder()) {

            final var rooms = roomWithFreePlaces.keySet().stream()
                    .filter(room -> room.getRoomNumber() == prioritizedRoomsNumbers.get(0) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(1) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(2) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(3) ||
                            room.getRoomNumber() == prioritizedRoomsNumbers.get(4))
                    .toList();

            putSelectedRooms(rooms, roomWithFreePlaces, roomWithFreePlacesToDisplayByPriority, peopleToAccommodate);

        } else if (peopleToAccommodate < ReservationGroup.LARGE_GROUP.getNumberOfPeopleTopBorder()) {
            return roomWithFreePlaces;
        } else {
            throw new MoreThanPlacesException(ErrorMessages.MORE_THAN_PLACES);
        }

        addNonPrioritizedRooms(roomWithFreePlaces, roomWithFreePlacesToDisplayByPriority);

        return roomWithFreePlacesToDisplayByPriority;
    }

    private void addNonPrioritizedRooms(final Map<Room, Integer> roomWithFreePlaces,
                                        final Map<Room, Integer> roomWithFreePlacesToDisplayByPriority) {
        roomWithFreePlaces.keySet().stream()
                .filter(room -> nonPrioritizedRoomsNumbers.contains(room.getRoomNumber()))
                .forEach(room -> roomWithFreePlacesToDisplayByPriority.put(room, roomWithFreePlaces.get(room)));
    }

    private void checkPerfectMatch(final Map<Room, Integer> roomWithFreePlaces, final int peopleToAccommodate,
                                   final Map<Room, Integer> roomWithFreePlacesToDisplayByPriority) {
        final Map<Integer, Integer> peopleToRoomNumberMap = new HashMap<>();
        peopleToRoomNumberMap.put(8, 1);
        peopleToRoomNumberMap.put(10, 3);
        peopleToRoomNumberMap.put(11, 11);

        final Integer roomNumber = peopleToRoomNumberMap.get(peopleToAccommodate);
        if (nonNull(roomNumber)) {
            roomWithFreePlaces.keySet().stream()
                    .filter(room -> room.getRoomNumber() == roomNumber)
                    .findFirst()
                    .ifPresent(room ->
                            putPerfectMatch(roomWithFreePlaces, room, roomWithFreePlacesToDisplayByPriority));


            if (roomWithFreePlacesToDisplayByPriority.isEmpty()) {
                final Map<Integer, Integer> peopleToRoomNumberMapTwo = new HashMap<>();
                peopleToRoomNumberMapTwo.put(8, 5);

                final Integer roomNumberTwo = peopleToRoomNumberMapTwo.get(peopleToAccommodate);
                if (nonNull(roomNumberTwo)) {
                    roomWithFreePlaces.keySet().stream()
                            .filter(room -> room.getRoomNumber() == roomNumberTwo)
                            .findFirst()
                            .ifPresent(room ->
                                    putPerfectMatch(roomWithFreePlaces, room, roomWithFreePlacesToDisplayByPriority));
                }
            }
        }
    }

    private void putPerfectMatch(final Map<Room, Integer> roomWithFreePlaces, final Room room,
                                 final Map<Room, Integer> roomWithFreePlacesToDisplayByPriority) {
        if (roomWithFreePlaces.get(room) == room.getMaxPeople())
            roomWithFreePlacesToDisplayByPriority.put(room, room.getMaxPeople());
    }

    private void putSelectedRooms(final List<Room> rooms, final Map<Room, Integer> roomWithFreePlaces,
                                  final Map<Room, Integer> roomWithFreePlacesToDisplayByPriority,
                                  int peopleToAccommodate) {
        if (!rooms.isEmpty()) {
            final var prioritizedRooms = roomWithFreePlaces.keySet();
            int totalPeopleAccommodated = 0;

            for (var roomNumber : prioritizedRooms) {
                int freePlacesInRoom = roomWithFreePlaces.get(roomNumber);
                roomWithFreePlacesToDisplayByPriority.put(roomNumber, freePlacesInRoom);
                totalPeopleAccommodated += freePlacesInRoom;

                if (totalPeopleAccommodated >= peopleToAccommodate) {
                    break;
                }
            }
        }
    }
}
