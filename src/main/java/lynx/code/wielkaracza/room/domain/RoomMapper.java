package lynx.code.wielkaracza.room.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.room.RoomCRUDFacade.RoomDTO;

@UtilityClass
public class RoomMapper {

    RoomDTO toDTO(final Room room) {
        return createRoomDTO(room, -1);
    }

    RoomDTO toDTOWithPriority(final Room room, final int freePlaces) {
        return createRoomDTO(room, freePlaces);
    }

    private static RoomDTO createRoomDTO(final Room room, final Integer freePlaces) {
        return RoomDTO.builder()
                .uuid(room.getUuid())
                .descriptionPL(room.getDescriptionPL())
                .descriptionEN(room.getDescriptionEN())
                .descriptionSK(room.getDescriptionSK())
                .titlePL(room.getTitlePL())
                .titleEN(room.getTitleEN())
                .titleSK(room.getTitleSK())
                .roomNumber(room.getRoomNumber())
                .bathroom(room.isBathroom())
                .maxPeople(room.getMaxPeople())
                .price(room.getPrice())
                .freePlaces(freePlaces)
                .numberOfDoubleBeds(room.getNumberOfDoubleBeds())
                .numberOfNormalBeds(room.getNumberOfNormalBeds())
                .build();
    }
}
