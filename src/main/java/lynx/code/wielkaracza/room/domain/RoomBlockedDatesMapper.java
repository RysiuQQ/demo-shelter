package lynx.code.wielkaracza.room.domain;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.reservation.domain.ReservationOverviewService;
import lynx.code.wielkaracza.room.RoomCRUDFacade.RoomBlockedDatesDTO;
import lynx.code.wielkaracza.room.RoomCRUDFacade.RoomBlockedDatesDTO.BlockedDate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.time.LocalDate;

@Component
@RequiredArgsConstructor
public class RoomBlockedDatesMapper {
    private final ReservationOverviewService reservationOverviewService;

    public RoomBlockedDatesDTO toDTO(Room room) {
        LocalDate currentDate = LocalDate.now();
        List<BlockedDate> blockedDates = new ArrayList<>();
        room.getBlockedDates().forEach(rbd -> {
            if (currentDate.isBefore(rbd.getDateTo()) || currentDate.isEqual(rbd.getDateTo())) {
                List<Reservation> relatedReservations =
                        reservationOverviewService.getReservationByDatesAndRoomNumber(rbd.getDateFrom(), rbd.getDateTo(), room.getRoomNumber());

                BlockedDate blockedDate = BlockedDate.builder()
                        .blockadeUuid(rbd.getUuid())
                        .dateFrom(rbd.dateFrom)
                        .dateTo(rbd.dateTo)
                        .creationDate(rbd.creationDate)
                        .relatedReservations(RoomReservationsMapper.toReservationDTO(relatedReservations))
                        .build();

                blockedDates.add(blockedDate);
            }
        });

        return RoomBlockedDatesDTO.builder()
                .roomUuid(room.getUuid())
                .roomNumber(room.getRoomNumber())
                .roomName(room.getTitlePL())
                .blockedDates(blockedDates.stream()
                        .sorted(Comparator.comparing(BlockedDate::dateFrom))
                        .toList())
                .build();
    }
}