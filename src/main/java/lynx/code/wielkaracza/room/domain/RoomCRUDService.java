package lynx.code.wielkaracza.room.domain;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.exception.NotFoundException;
import lynx.code.wielkaracza.common.exception.ServiceException;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.reservation.domain.ReservationOverviewService;
import lynx.code.wielkaracza.reservation.domain.ReservationQueryRepository;
import lynx.code.wielkaracza.room.RoomCRUDFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.room.domain.RoomCRUDService.ErrorMessages.ROOM_DOES_NOT_EXIST;

@Service
@RequiredArgsConstructor
class RoomCRUDService implements RoomCRUDFacade {

    @UtilityClass
    static final class ErrorMessages {
        static final String ROOM_DOES_NOT_EXIST = "Room with UUID: %s does not exist!";
        static final String RESERVATION_DOES_NOT_EXIST = "Reservation with UUID: %s does not exist!";
        static final String ROOM_BLOCKADE_DOES_NOT_EXIST = "Room blockade with UUID: %s does not exist!";
        static final String ROOM_BLOCKADE_INCORRECT_DATES = "Incorrect dates! 'dateFrom' is higher than 'dateTo'";
        static final String ROOM_BLOCKADE_DATE_FROM_NEEDS_TO_BE_FUTURE = "Incorrect dates! 'dateFrom' needs to be at least current day!";
        static final String ROOM_BLOCKADE_ALREADY_EXISTS = "The blockade for specified terms already exists! Provide different dates.";
    }

    private final RoomQueryRepository roomQueryRepository;
    private final ReservationQueryRepository reservationQueryRepository;
    private final RoomAvailabilityLogic roomAvailabilityLogic;
    private final RoomPriorityLogic roomPriorityLogic;
    private final RoomBlockedDateRepository roomBlockedDateRepository;
    private final ReservationOverviewService reservationOverviewService;
    private final RoomBlockedDatesMapper roomBlockedDatesMapper;

    @Override
    @Transactional
    public RoomReservationsDTO blockRoomForSpecificDate(BlockRoomRequestDto request) {
        Room room = roomQueryRepository.findByUuid(request.roomUuid())
                .orElseThrow(() -> new NotFoundException(ROOM_DOES_NOT_EXIST, request.roomUuid().toString()));

        validateBlockRoomRequest(request, room.getBlockedDates());

        RoomBlockedDate roomBlockedDate = new RoomBlockedDate(request, room);
        roomBlockedDateRepository.save(roomBlockedDate);

        List<Reservation> relatedReservations = reservationOverviewService.getReservationByDatesAndRoomNumber(request.dateFrom(), request.dateTo(), room.getRoomNumber());

        if (!relatedReservations.isEmpty()) {
            return RoomReservationsMapper.toDto(relatedReservations, room.getRoomNumber());
        }

        return new RoomReservationsDTO(null, new ArrayList<>());
    }

    private void validateBlockRoomRequest(BlockRoomRequestDto request, List<RoomBlockedDate> blockedDates) {
        LocalDate currentDate = LocalDate.now();

        if(currentDate.isAfter(request.dateFrom())) {
            throw new ServiceException(ErrorMessages.ROOM_BLOCKADE_DATE_FROM_NEEDS_TO_BE_FUTURE);
        }

        if (request.dateFrom().isAfter(request.dateTo())) {
            throw new ServiceException(ErrorMessages.ROOM_BLOCKADE_INCORRECT_DATES);
        }

        List<RoomBlockedDate> roomBlockedFurtherDates = blockedDates.stream()
                .filter(r -> currentDate.isBefore(r.getDateTo()) || currentDate.isEqual(r.getDateTo()))
                .toList();

        if (!roomBlockedFurtherDates.isEmpty()) {
            for (RoomBlockedDate roomBlockedDate : roomBlockedFurtherDates) {
                if (((request.dateFrom().isAfter(roomBlockedDate.getDateFrom()) || request.dateFrom().equals(roomBlockedDate.getDateFrom())) && (request.dateFrom().isBefore(roomBlockedDate.getDateTo()) || request.dateFrom().equals(roomBlockedDate.getDateTo()))) ||
                        ((request.dateTo().isAfter(roomBlockedDate.getDateFrom()) || request.dateTo().equals(roomBlockedDate.getDateFrom())) && (request.dateTo().isBefore(roomBlockedDate.getDateTo())) || request.dateTo().equals(roomBlockedDate.getDateTo()))) {
                    throw new ServiceException(ErrorMessages.ROOM_BLOCKADE_ALREADY_EXISTS);
                }
            }
        }
    }

    @Override
    @Transactional
    public void deleteRoomBlockade(UUID blockadeUuid) {
        roomBlockedDateRepository.findByUuid(blockadeUuid)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.ROOM_BLOCKADE_DOES_NOT_EXIST, blockadeUuid.toString()));

        roomBlockedDateRepository.deleteByUuid(blockadeUuid);
    }

    @Override
    public RoomBlockedDatesDTO getRoomBlockedDates(UUID roomUuid) {
        Room room = roomQueryRepository.findByUuid(roomUuid).orElseThrow(() -> new NotFoundException(ROOM_DOES_NOT_EXIST, roomUuid.toString()));
        return roomBlockedDatesMapper.toDTO(room);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RoomDTO> getRooms() {
        return roomQueryRepository.findAll()
                .stream()
                .map(RoomMapper::toDTO)
                .toList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<RoomDTO> getAvailableRooms(final ArrivalForm arrivalForm) {
        final var availableRooms = roomAvailabilityLogic.findAvailableRooms(arrivalForm, null);
        final var roomsWithFreePlaces =
            roomPriorityLogic.displayRoomsByPriority(availableRooms, arrivalForm.numberOfPeople());

        return roomsWithFreePlaces.keySet().stream()
                .map(room -> RoomMapper.toDTOWithPriority(room, roomsWithFreePlaces.get(room)))
                .toList();
    }

    @Override
    @Transactional(readOnly = true)
    public RoomDTO getRoom(final UUID uuid) {
        return RoomMapper.toDTO(getRoomOrThrow(uuid));
    }

    @Override
    @Transactional
    public void changeRoomPrice(final UUID uuid, final BigDecimal price) {
        final var room = getRoomOrThrow(uuid);

        room.setPrice(price);
    }

    @Override
    public List<RoomDTO> getAvailableRoomsForEdition(final ArrivalForm arrivalForm, final UUID uuid) {
        final var reservation = reservationQueryRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException(ErrorMessages.RESERVATION_DOES_NOT_EXIST, uuid.toString()));

        final var availableRooms =
                roomAvailabilityLogic.findAvailableRooms(arrivalForm, reservation);

        return availableRooms.keySet().stream()
                .map(room -> RoomMapper.toDTOWithPriority(room, availableRooms.get(room)))
                .toList();
    }

    @Override
    public List<RoomDTO> getAvailableRoomsAdmin(final ArrivalForm arrivalForm) {
        final var availableRooms = roomAvailabilityLogic.findAvailableRooms(arrivalForm, null);

        return availableRooms.keySet().stream()
                .map(room -> RoomMapper.toDTOWithPriority(room, availableRooms.get(room)))
                .toList();
    }

    public Room getRoomOrThrow(final UUID uuid) {
        return roomQueryRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException(ROOM_DOES_NOT_EXIST, uuid.toString()));
    }
}