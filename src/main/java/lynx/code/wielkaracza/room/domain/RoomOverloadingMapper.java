package lynx.code.wielkaracza.room.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.cart.ProductRoom;

import static lynx.code.wielkaracza.room.RoomOverloadingFacade.RoomOverloadingDTO;

@UtilityClass
public class RoomOverloadingMapper {

    public RoomOverloadingDTO toDTO(final ProductRoom productRoom) {
        final var reservation = productRoom.getReservation();
        final var room = productRoom.getRoom();
        return RoomOverloadingDTO.builder()
                .name(String.format("%s %s", reservation.getFirstName(), reservation.getLastName()))
                .reservationDayStart(reservation.getReservationDayStart())
                .reservationDayEnd(reservation.getReservationDayEnd())
                .phoneNumber(reservation.getReservationDetails().getPhoneNumber())
                .amountToPay(reservation.getAmountToPay())
                .maxPeople(room.getMaxPeople())
                .roomNumber(room.getRoomNumber())
                .numberOfPeople(productRoom.getNumberOfPeople())
                .reservationType(reservation.getReservationDetails().getReservationType())
                .amountAlreadyPaid(reservation.getAmountAlreadyPaid())
                .reservationUuid(reservation.getUuid())
                .roomUuid(room.getUuid())
                .build();
    }
}
