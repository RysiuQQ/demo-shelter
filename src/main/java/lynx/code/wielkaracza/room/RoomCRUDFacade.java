package lynx.code.wielkaracza.room;

import lombok.Builder;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.validation.SelfValidator.SelfValidation;
import lynx.code.wielkaracza.room.domain.Room;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.common.validation.SelfValidator.SelfValidate;
import static org.apache.commons.lang3.BooleanUtils.isFalse;

public interface RoomCRUDFacade {

    RoomReservationsDTO blockRoomForSpecificDate(BlockRoomRequestDto blockRoomRequestDto);

    void deleteRoomBlockade(UUID blockadeUuid);

    @UtilityClass
    final class ValidationRules {
        private static final String DAY_START_NOT_BEFORE_DAY_END =
                "Reservation day start must be before reservation day end";
    }

    RoomBlockedDatesDTO getRoomBlockedDates(UUID roomUuid);
    List<RoomDTO> getRooms();

    List<RoomDTO> getAvailableRooms(final ArrivalForm arrivalForm);

    RoomDTO getRoom(final UUID uuid);

    void changeRoomPrice(final UUID uuid, final BigDecimal price);

    List<RoomDTO> getAvailableRoomsForEdition(final ArrivalForm arrivalForm, final UUID uuid);

    Room getRoomOrThrow(final UUID uuid);

    List<RoomDTO> getAvailableRoomsAdmin(ArrivalForm arrivalForm);

    @Builder
    record RoomDTO(UUID uuid,
                   String descriptionPL,
                   String descriptionEN,
                   String descriptionSK,
                   String titlePL,
                   String titleEN,
                   String titleSK,
                   int maxPeople,
                   int roomNumber,
                   boolean bathroom,
                   BigDecimal price,
                   int numberOfNormalBeds,
                   int numberOfDoubleBeds,
                   int freePlaces) {
    }

    @Builder
    record RoomBlockedDatesDTO(UUID roomUuid,
                               Integer roomNumber,
                               String roomName,
                               List<BlockedDate> blockedDates) {
        @Builder
        public record BlockedDate(
                UUID blockadeUuid,
                @DateTimeFormat(pattern = "yyyy-MM-dd")
                LocalDate dateFrom,
                @DateTimeFormat(pattern = "yyyy-MM-dd")
                LocalDate dateTo,

                LocalDateTime creationDate,

                List<RoomReservationsDTO.Reservation> relatedReservations) {

        }
    }

    @Builder
    record BlockRoomRequestDto(UUID roomUuid,
                               @DateTimeFormat(pattern = "yyyy-MM-dd")
                               LocalDate dateFrom,
                               @DateTimeFormat(pattern = "yyyy-MM-dd")
                               LocalDate dateTo
    ) {

    }

    @Builder
    record RoomReservationsDTO(Integer roomNumber,
                               List<Reservation> reservations) {
        @Builder
        public record Reservation(String firstName,
                           String lastName,
                           String email,
                           LocalDate reservationDayStart,
                           LocalDate reservationDayEnd,
                           LocalDateTime dateOfAddingReservation) {

        }
    }
    @Builder
    record ProductRoomDTO(UUID uuid,
                          String descriptionPL,
                          String descriptionEN,
                          String descriptionSK,
                          String titlePL,
                          String titleEN,
                          String titleSK,
                          int numberOfPeople,
                          int roomNumber,
                          int numberOfNormalBeds,
                          int numberOfDoubleBeds) {
    }

    @Builder
    @SelfValidation
    record ArrivalForm(@DateTimeFormat(pattern = "yyyy-MM-dd")
                       @NotNull(message = "day of start reservation is required")
                       @FutureOrPresent(message = "date must be at least current-day")
                       LocalDate reservationDayStart,

                       @DateTimeFormat(pattern = "yyyy-MM-dd")
                       @NotNull(message = "day of end reservation is required")
                       @FutureOrPresent(message = "date must be at least current-day")
                       LocalDate reservationDayEnd,

                       @Positive(message = "number Of People must be greater than 0")
                       int numberOfPeople,

                       @NotNull
                       boolean dogs) implements SelfValidate {
        @Override
        public boolean validate(final ConstraintValidatorContext context) {
            boolean isValid = true;

            if (isFalse(reservationDayStart.isBefore(reservationDayEnd))) {
                context.buildConstraintViolationWithTemplate
                        (RoomCRUDFacade.ValidationRules.DAY_START_NOT_BEFORE_DAY_END).addConstraintViolation();

                isValid = false;
            }
            return isValid;
        }
    }
}
