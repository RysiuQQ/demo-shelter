package lynx.code.wielkaracza.room.controller;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.date.DateDTO;
import lynx.code.wielkaracza.room.RoomOverloadingFacade;
import lynx.code.wielkaracza.room.RoomOverloadingFacade.RoomFutureAvailabilityDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.room.RoomOverloadingFacade.RoomOverloadingDTO;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
class RoomOverloadingController {

    @UtilityClass
    static final class Routes {
        static final String ROOT = "api/rooms";
        static final String FUTURE_AVAILABILITY = ROOT + "/future-availability/{uuid}";
        static final String ROOMS = ROOT + "/overloading/rooms";
        static final String SHELTER = ROOT + "/overloading/shelter";
    }

    private final RoomOverloadingFacade roomOverloadingFacade;

    @PostMapping(Routes.FUTURE_AVAILABILITY)
    List<RoomFutureAvailabilityDTO> getFutureAvailability(@PathVariable final UUID uuid,
                                                          @RequestBody final DateDTO date) {
        return roomOverloadingFacade.getFutureAvailability(uuid, date);
    }

    @PostMapping(Routes.ROOMS)
    List<RoomOverloadingDTO> getRoomsOverloading(@RequestBody final DateDTO date) {
        return roomOverloadingFacade.getRoomsOverloading(date);
    }

    @PostMapping(Routes.SHELTER)
    int getShelterOverloading(@RequestBody final DateDTO date) {
        return roomOverloadingFacade.getShelterOverloading(date);
    }
}