package lynx.code.wielkaracza.room.controller;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.room.RoomCRUDFacade;
import lynx.code.wielkaracza.room.RoomCRUDFacade.ArrivalForm;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.room.RoomCRUDFacade.RoomDTO;
import static lynx.code.wielkaracza.room.RoomCRUDFacade.BlockRoomRequestDto;
import static lynx.code.wielkaracza.room.RoomCRUDFacade.RoomBlockedDatesDTO;
import static lynx.code.wielkaracza.room.RoomCRUDFacade.RoomReservationsDTO;
import static lynx.code.wielkaracza.room.controller.RoomCRUDController.Routes.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequiredArgsConstructor
@CrossOrigin(originPatterns = "*")
class RoomCRUDController {

    @UtilityClass
    static final class Routes {
        static final String ROOT = "api/rooms";
        static final String EDITION = "api/rooms/edition/{uuid}";
        static final String AVAILABILITY = "api/rooms/availability";
        static final String AVAILABILITY_ADMIN = "api/rooms/availability/admin";
        static final String SINGLE_ROOM = ROOT + "/{uuid}";
        static final String PRICE = ROOT + "/{uuid}/price";
        static final String ROOM_BLOCKED_DATES = ROOT + "/blocked-dates";
    }

    private final RoomCRUDFacade roomFacade;

    @GetMapping(ROOT)
    List<RoomDTO> getRooms() {
        return roomFacade.getRooms();
    }

    @GetMapping(ROOM_BLOCKED_DATES)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    RoomBlockedDatesDTO getRoomBlockedDates(@RequestParam UUID roomUuid) {
        return roomFacade.getRoomBlockedDates(roomUuid);
    }

    @PostMapping(ROOM_BLOCKED_DATES)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    RoomReservationsDTO blockRoomForSpecificDate(@RequestBody BlockRoomRequestDto blockRoomRequestDto) {
        return roomFacade.blockRoomForSpecificDate(blockRoomRequestDto);
    }

    @DeleteMapping(ROOM_BLOCKED_DATES)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    void deleteRoomBlockade(@RequestParam UUID blockadeUuid) {
        roomFacade.deleteRoomBlockade(blockadeUuid);
    }

    @PostMapping(AVAILABILITY)
    List<RoomDTO> getAvailableRooms(@Valid @RequestBody ArrivalForm arrivalForm) {
        return roomFacade.getAvailableRooms(arrivalForm);
    }

    @PostMapping(AVAILABILITY_ADMIN)
    List<RoomDTO> getAvailableRoomsAdmin(@Valid @RequestBody ArrivalForm arrivalForm) {
        return roomFacade.getAvailableRoomsAdmin(arrivalForm);
    }

    @GetMapping(SINGLE_ROOM)
    RoomDTO getRoom(@PathVariable final UUID uuid) {
        return roomFacade.getRoom(uuid);
    }

    @PatchMapping(PRICE)
    @ResponseStatus(NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    void changePrice(@PathVariable final UUID uuid, @RequestBody final BigDecimal price) {
        roomFacade.changeRoomPrice(uuid, price);
    }

    @PostMapping(EDITION)
    List<RoomDTO> getRoomsForEdition(@PathVariable final UUID uuid, @RequestBody final ArrivalForm arrivalForm) {
        return roomFacade.getAvailableRoomsForEdition(arrivalForm, uuid);
    }
}
