package lynx.code.wielkaracza.email;

import lynx.code.wielkaracza.reservation.ReservationCRUDFacade;

public interface EmailHandlerService {
    void sendSuccessEmailReservation(ReservationCRUDFacade.ReservationDTO reservationDTO);

    void sendReservationDeletedEmail(ReservationCRUDFacade.ReservationDTO reservationDTO);

    void sendFailedEmailNotification(ReservationCRUDFacade.ReservationDTO reservationDTO);
}
