package lynx.code.wielkaracza.email;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.security.jwt.AuthTokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Service
@RequiredArgsConstructor
public class EmailSenderImpl implements EmailSender {

    private final JavaMailSender sender;

    private final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    @Value("${email.message.subject}")
    private String emailSubject;

    public void sendEmail(String email, String htmlTemplate) {
        try {
            if (!htmlTemplate.isEmpty()) {
                MimeMessage message = sender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

                helper.setTo(email);
                helper.setSubject(emailSubject);
                helper.setText(htmlTemplate, true);

                sender.send(message);
                logger.info("[EMAIL] Email to '{}' has been send successfully.", email);
                logger.info("[EMAIL] Email template: {}", htmlTemplate);
            }
        } catch (Exception e) {
            logger.error("[EMAIL] Error during sending email to '{}'", email);
            logger.info("[EMAIL] Email template: {}", htmlTemplate);
        }
    }


}
