package lynx.code.wielkaracza.email;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.common.enums.Language;
import lynx.code.wielkaracza.security.jwt.AuthTokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static lynx.code.wielkaracza.reservation.ReservationCRUDFacade.ReservationDTO;

@Service
@RequiredArgsConstructor
public class EmailHandlerServiceImpl implements EmailHandlerService {
    private EmailSender emailSender;
    private Configuration config;
    private final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    private final Map<Language, String> successTemplatesNames = Map.of(
            Language.PL, "successReservationPL.ftl",
            Language.EN, "successReservationEN.ftl",
            Language.SK, "successReservationSK.ftl"
    );

    private final Map<Language, String> failTemplatesNames = Map.of(
            Language.PL, "failedReservationPL.ftl",
            Language.EN, "failedReservationEN.ftl",
            Language.SK, "failedReservationSK.ftl"
    );

    private final Map<Language, String> deletedTemplatesNames = Map.of(
            Language.PL, "deletedReservationPL.ftl",
            Language.EN, "deletedReservationEN.ftl",
            Language.SK, "deletedReservationSK.ftl"
    );

    @Autowired
    public EmailHandlerServiceImpl(EmailSender emailSender, Configuration config) {
        this.emailSender = emailSender;
        this.config = config;
    }

    @Override
    public void sendSuccessEmailReservation(ReservationDTO reservationDTO) {
        Map<String, Object> emailData = getSuccessEmailModel(reservationDTO);
        logger.info("[EMAIL] ReservationDto to be send to the client: {}", reservationDTO);
        emailSender.sendEmail(reservationDTO.email(),
                getHTMLTemplateByTemplateName(successTemplatesNames.get(reservationDTO.language()), emailData));
        logger.info("[EMAIL] Email to '{}' has been sent successfully!", reservationDTO.email() );
    }

    @Override
    public void sendReservationDeletedEmail(ReservationDTO reservationDTO) {
        Map<String, Object> emailData = getSuccessEmailModel(reservationDTO);
        logger.info("[EMAIL] ReservationDto to be send to the client: {}", reservationDTO);
        emailSender.sendEmail(reservationDTO.email(),
                getHTMLTemplateByTemplateName(deletedTemplatesNames.get(reservationDTO.language()), emailData));
    }

    @Override
    public void sendFailedEmailNotification(ReservationDTO reservationDTO) {
        try {
            Map<String, Object> emailData = getBasicEmailModel(reservationDTO);
            emailSender.sendEmail(reservationDTO.email(),
                    getHTMLTemplateByTemplateName(failTemplatesNames.get(reservationDTO.language()), emailData));
        } catch (Exception e) {
            logger.error("[EMAIL] Error during sending email to: '{}'.", reservationDTO.email());
        }
    }

    private Map<String, Object> getSuccessEmailModel(ReservationDTO reservationDTO) {
        Map<String, Object> model = new HashMap<>();
        model.put("fullName", getFullName(reservationDTO));
        model.put("reservationDate", LocalDate.now());
        model.put("amountOfNights", reservationDTO.nights());
        model.put("rooms", reservationDTO.productRoomsDTO());
        model.put("arrivalDate", reservationDTO.reservationDayStart());
        model.put("dateOfAddingReservation", reservationDTO.dateOfAddingReservation());
        model.put("departureDate", reservationDTO.reservationDayEnd());
        model.put("amountToPay", reservationDTO.amountToPay());
        model.put("deposit", reservationDTO.amountAlreadyPaid());
        model.put("reservationId", reservationDTO.uuid());

        return model;
    }

    private Map<String, Object> getBasicEmailModel(ReservationDTO reservationDTO) {
        Map<String, Object> model = new HashMap<>();
        model.put("fullName", getFullName(reservationDTO));
        return model;
    }

    private String getFullName(ReservationDTO reservationDTO) {
        return reservationDTO.firstName() + " " + reservationDTO.lastName();
    }

    private String getHTMLTemplateByTemplateName(String templateName, Map<String, Object> model) {
        try {
            logger.info("[EMAIL] Email data-model: '{}'", model);
            logger.info("[EMAIL] Email template: '{}'", templateName);
            Template template = config.getTemplate("/" + templateName);
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        } catch (IOException e) {
            logger.error("[EMAIL] Template with name: '{}'. Does not exist!", templateName);
            return "";
        } catch (TemplateException e) {
            logger.error("[EMAIL] Template with name: '{}'. Could not be processed with provided model! ", templateName);
            return "";
        }
    }
}
