package lynx.code.wielkaracza.email;

public interface EmailSender {
    void sendEmail(String email, String htmlTemplate);
}
