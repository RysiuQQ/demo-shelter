package lynx.code.wielkaracza.security.services;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.user.domain.User;
import lynx.code.wielkaracza.user.domain.UserQueryRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    @UtilityClass
    static final class ErrorMessages {
        static final String USERNAME_NOT_EXISTS = "Username not found with that login!";
    }

    private final UserQueryRepository userQueryRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException {
        final User user = userQueryRepository.findUsersByUsername(login)
                .orElseThrow(() -> new UsernameNotFoundException(ErrorMessages.USERNAME_NOT_EXISTS));

        return UserDetailsImpl.build(user);
    }

}
