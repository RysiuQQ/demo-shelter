package lynx.code.wielkaracza.security.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lynx.code.wielkaracza.user.domain.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserDetailsImpl implements UserDetails {

    @Serial
    private static final long serialVersionUID = 1L;

    @EqualsAndHashCode.Include
    private final UUID uuid;

    private final String login;

    private final String firstName;

    private final String lastName;

    @JsonIgnore
    private final String password;

    private final Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(final UUID id, final String username, final String password,
                           final String firstName, final String lastName,
                           final Collection<? extends GrantedAuthority> authorities) {
        this.uuid = id;
        this.login = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.authorities = authorities;
    }

    public static UserDetailsImpl build(final User user) {
        final var authority = new SimpleGrantedAuthority(user.getRole().name());

        final List<GrantedAuthority> grantedAuthorities = List.of(authority);

        return new UserDetailsImpl(
                user.getUuid(),
                user.getUsername(),
                user.getPassword(),
                user.getFirstName(),
                user.getLastName(),
                grantedAuthorities);
    }

    public record UserDetailsSimple(UUID uuid, String username) {
        @Override
        public String toString() {
            return "Username: " + username + " uuid: " + uuid;
        }
    }

    public static UserDetailsSimple getUserDetailsSimple() {
        final var userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new UserDetailsSimple(userDetails.getUuid(), userDetails.getUsername());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
