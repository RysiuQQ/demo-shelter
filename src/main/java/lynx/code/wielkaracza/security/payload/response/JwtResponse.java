package lynx.code.wielkaracza.security.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class JwtResponse {
    private String token;
    private UUID uuid;
    private String type = "Bearer";
    private String username;
    private String firstName;
    private String lastName;
    private List<String> roles;
    private LocalDateTime expirationTime;

    public JwtResponse(final UUID uuid, final String accessToken, final String username, final String firstName,
                       final String lastName, final List<String> roles, final LocalDateTime expirationTime) {
        this.token = accessToken;
        this.username = username;
        this.roles = roles;
        this.firstName = firstName;
        this.lastName = lastName;
        this.expirationTime = expirationTime;
        this.uuid = uuid;
    }
}
