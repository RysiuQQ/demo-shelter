package lynx.code.wielkaracza.common.date;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.exception.DataTimeParseException;
import lynx.code.wielkaracza.common.exception.DateNullException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@UtilityClass
public class DateFormatter {

    @UtilityClass
    static final class ErrorMessages {
        static final String DATE_NULL = "Provided date is null!";
        static final String INCORRECT_DATE_FORMAT = "Date format is NOT correct";
    }

    public static LocalDate formatStringToDate(final String date) {
        try {
            final var formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
            return LocalDate.parse(date, formatter);
        } catch (final NullPointerException e) {
            throw new DateNullException(ErrorMessages.DATE_NULL);
        } catch (final DateTimeParseException e) {
            throw new DataTimeParseException(ErrorMessages.INCORRECT_DATE_FORMAT);
        }
    }
}