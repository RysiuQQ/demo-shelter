package lynx.code.wielkaracza.common.date;

import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static java.util.Objects.nonNull;
import static lynx.code.wielkaracza.common.date.DateFormatter.formatStringToDate;

@UtilityClass
public class DateUtils {

     public LocalDate getCurrentDate(final DateDTO providedDate) {
        if (nonNull(providedDate.providedDate())) return formatStringToDate(providedDate.providedDate());

        return Instant.ofEpochMilli(new Date().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
