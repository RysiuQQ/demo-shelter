package lynx.code.wielkaracza.common.date;

public record DateDTO(String providedDate) {
}