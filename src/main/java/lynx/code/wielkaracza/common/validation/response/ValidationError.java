package lynx.code.wielkaracza.common.validation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@AllArgsConstructor(staticName = "of")
public class ValidationError {
    private HttpStatus status;
    private List<String> messages;
}