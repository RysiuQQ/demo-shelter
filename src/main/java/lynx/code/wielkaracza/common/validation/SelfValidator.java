package lynx.code.wielkaracza.common.validation;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.ObjectUtils.isNotEmpty;


public class SelfValidator implements ConstraintValidator<SelfValidator.SelfValidation, SelfValidator.SelfValidate> {
    private static final String DEFAULT_MESSAGE = "error.self.validation.failed";

    @Inherited
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = SelfValidator.class)
    public @interface SelfValidation {
        String message() default DEFAULT_MESSAGE;

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    public interface SelfValidate {
        boolean validate(final ConstraintValidatorContext ctx);
    }

    @Override
    public void initialize(final SelfValidation selfValidation) { //NOSONAR
    }

    @Override
    public boolean isValid(final SelfValidate selfValidate, final ConstraintValidatorContext ctx) {
        boolean valid = selfValidate.validate(ctx);
        ctx.disableDefaultConstraintViolation();
        return valid;
    }

    public static void addViolation(final ConstraintValidatorContext ctx, final String field, final String message) {
        ctx.buildConstraintViolationWithTemplate(message)
                .addPropertyNode(field)
                .addConstraintViolation();
    }

    public static boolean areTargetsWithPositionValid(final Collection<List<Integer>> targetsWithPosition,
                                                      final ConstraintValidatorContext ctx, final String field, final String message) {

        final Set<Integer> duplicatedTargetsPositions = targetsWithPosition.stream()
                .filter(list -> list.size() > 1)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        if (isNotEmpty(duplicatedTargetsPositions)) {
            duplicatedTargetsPositions.forEach(position -> addViolation(ctx, String.format(field, position), message));
            return false;
        }
        return true;
    }
}