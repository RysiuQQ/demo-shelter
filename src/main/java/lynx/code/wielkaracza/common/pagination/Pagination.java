package lynx.code.wielkaracza.common.pagination;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Pagination {

    private Integer size;
    private Long totalElements;
    private Integer totalPages;
    private Integer number;

    public Pagination(Integer size, Long totalElements, Integer totalPages, Integer number) {
        this.size = size;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.number = number;
    }
}