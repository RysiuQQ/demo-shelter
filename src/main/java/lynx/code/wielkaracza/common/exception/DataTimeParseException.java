package lynx.code.wielkaracza.common.exception;

public class DataTimeParseException extends RuntimeException{

    public DataTimeParseException(final String message) {
        super(String.format(message));
    }
}
