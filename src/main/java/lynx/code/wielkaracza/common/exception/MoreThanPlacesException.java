package lynx.code.wielkaracza.common.exception;

public class MoreThanPlacesException extends RuntimeException {

    public MoreThanPlacesException(final String message) {
        super(message);
    }
}
