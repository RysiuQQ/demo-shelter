package lynx.code.wielkaracza.common.exception;

public class DateNullException extends RuntimeException {
    public DateNullException(final String message) {
        super(String.format(message));
    }
}
