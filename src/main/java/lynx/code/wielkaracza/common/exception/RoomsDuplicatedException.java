package lynx.code.wielkaracza.common.exception;

public class RoomsDuplicatedException extends RuntimeException {

    public RoomsDuplicatedException(final String message) {
        super(String.format(message));
    }
}