package lynx.code.wielkaracza.common.exception;

public class ServiceException extends RuntimeException {
    public ServiceException(final String message, final Object... args) {
        super(String.format(message, args));
    }

    public ServiceException(final String message) {
        super(message);
    }
}