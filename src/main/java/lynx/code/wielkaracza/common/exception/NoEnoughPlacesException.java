package lynx.code.wielkaracza.common.exception;

public class NoEnoughPlacesException extends RuntimeException {

    public NoEnoughPlacesException(final String message) {
        super(String.format(message));
    }
}