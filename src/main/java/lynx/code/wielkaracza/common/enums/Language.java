package lynx.code.wielkaracza.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Language {
    // BG("bg"),
    // CS("cs"),
    // DE("de"),
    EN("en"),
    // ES("es"),
    // FR("fr"),
    // HR("hr"),
    // HU("hu"),
    // IT("it"),
    // NL("nl"),
    PL("pl"),
    // SE("se"),
     SK("sk");

    private final String value;
}