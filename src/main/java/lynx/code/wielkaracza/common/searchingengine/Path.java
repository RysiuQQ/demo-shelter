package lynx.code.wielkaracza.common.searchingengine;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lynx.code.wielkaracza.unavailableterm.UnavailableTerm;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor(staticName = "of")
public class Path {
    private UnavailableTerm mainDate;
    private List<UnavailableTerm> unavailableTerms;
}