package lynx.code.wielkaracza.common.searchingengine;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.cart.ProductRoom;
import lynx.code.wielkaracza.room.domain.Room;
import lynx.code.wielkaracza.unavailableterm.UnavailableTerm;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.BooleanUtils.isFalse;

@UtilityClass
public class SearchingUtils {

    public Path prepareDataPaths(final UnavailableTerm checkingDate, final List<UnavailableTerm> unavailableTerms) {

        final List<UnavailableTerm> dateToCompare = new ArrayList<>();

        for (final UnavailableTerm unavailableTerm : unavailableTerms) {
            if (checkingDate.getUuid().equals(unavailableTerm.getUuid())) continue;

            if (unavailableTerm.getReservationDayStart().isAfter(checkingDate.getReservationDayStart())) {

                if (checkingDate.getReservationDayEnd().isEqual(unavailableTerm.getReservationDayStart())) {
                    continue;
                }

                if (!checkingDate.getReservationDayEnd().isBefore(unavailableTerm.getReservationDayStart())) {
                    dateToCompare.add(unavailableTerm);
                }
            }
            if (unavailableTerm.getReservationDayStart().isBefore(checkingDate.getReservationDayStart())) {
                if (checkingDate.getReservationDayStart().isEqual(unavailableTerm.getReservationDayEnd())) {
                    continue;
                }

                if (!checkingDate.getReservationDayStart().isAfter(unavailableTerm.getReservationDayEnd())) {
                    dateToCompare.add(unavailableTerm);
                }
            }

            if (unavailableTerm.getReservationDayStart().isEqual(checkingDate.getReservationDayStart())) {
                dateToCompare.add(unavailableTerm);
            }
        }

        return Path.of(checkingDate, dateToCompare);
    }

    public void eliminateNotAssociatedData(final Room room, final List<UnavailableTerm> unavailableTerms,
                                            final UnavailableTerm checkingDate, final UnavailableTerm unavailableTerm) {

        if (unavailableTerm.getReservationDayStart().isAfter(checkingDate.getReservationDayStart())) {

            if (checkingDate.getReservationDayEnd().isEqual(unavailableTerm.getReservationDayStart())) {
                removeTermWithLowerAmountOfPeople(checkingDate, unavailableTerm, room, unavailableTerms);
                return;
            }

            if (isFalse(checkingDate.getReservationDayEnd().isBefore(unavailableTerm.getReservationDayStart()))) {
                return;
            }
        }

        if (unavailableTerm.getReservationDayStart().isBefore(checkingDate.getReservationDayStart())) {
            if (checkingDate.getReservationDayStart().isEqual(unavailableTerm.getReservationDayEnd())) {
                removeTermWithLowerAmountOfPeople(checkingDate, unavailableTerm, room, unavailableTerms);
                return;
            }

            if (isFalse(checkingDate.getReservationDayStart().isAfter(unavailableTerm.getReservationDayEnd()))) {
                return;
            }
        }

        if (unavailableTerm.getReservationDayStart().isEqual(checkingDate.getReservationDayStart())) return;

        removeTermWithLowerAmountOfPeople(checkingDate, unavailableTerm, room, unavailableTerms);
    }

    public int countAmountOfPeople(final List<Path> paths, final Room room) {
        int reservedPlaces = 0;

        for (final Path path : paths) {
            int numberOfPeople = path.getMainDate().getReservation().getProductRooms().stream()
                    .filter(productRoomFromStream ->
                            Objects.equals(productRoomFromStream.getRoom().getUuid(),room.getUuid()))
                    .map(ProductRoom::getNumberOfPeople)
                    .flatMapToInt(IntStream::of)
                    .sum();


            for (final UnavailableTerm unavailableTerm : path.getUnavailableTerms()) {
                final var sum = unavailableTerm.getReservation().getProductRooms().stream()
                        .filter(productRoomFromStream ->
                                Objects.equals(productRoomFromStream.getRoom().getUuid(), room.getUuid()))
                        .map(ProductRoom::getNumberOfPeople)
                        .flatMapToInt(IntStream::of)
                        .sum();

                numberOfPeople += sum;
            }
            if (numberOfPeople > reservedPlaces) reservedPlaces = numberOfPeople;
        }

        return room.getMaxPeople() - reservedPlaces;
    }

    private void removeTermWithLowerAmountOfPeople(final UnavailableTerm checkingDate, final UnavailableTerm unavailableTerm,
                                                   final Room room, List<UnavailableTerm> unavailableTerms) {
        final var sumCheckingDate = checkingDate.getReservation().getProductRooms().stream()
                .filter(productRoomFromStream ->
                        Objects.equals(productRoomFromStream.getRoom().getUuid(), room.getUuid()))
                .map(ProductRoom::getNumberOfPeople)
                .flatMapToInt(IntStream::of)
                .sum();

        final var sumUnavailableTerm = unavailableTerm.getReservation().getProductRooms().stream()
                .filter(productRoomFromStream ->
                        Objects.equals(productRoomFromStream.getRoom().getUuid(), room.getUuid()))
                .map(ProductRoom::getNumberOfPeople)
                .flatMapToInt(IntStream::of)
                .sum();

        if (sumCheckingDate < sumUnavailableTerm) {
            unavailableTerms.add(unavailableTerm);
        } else {
            unavailableTerms.add(checkingDate);
        }
    }
}
