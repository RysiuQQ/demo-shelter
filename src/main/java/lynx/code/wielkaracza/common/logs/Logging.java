package lynx.code.wielkaracza.common.logs;

import lombok.SneakyThrows;
import lynx.code.wielkaracza.reservation.ReservationCRUDFacade.ReservationForm;
import lynx.code.wielkaracza.user.UserFacade.UserForm;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.UUID;

import static lynx.code.wielkaracza.security.services.UserDetailsImpl.getUserDetailsSimple;

@Aspect
@Component
@EnableAspectJAutoProxy
public class Logging {

    public static final Logger logger = LoggerFactory.getLogger(Logging.class);

    @Pointcut(value = "execution(* lynx.code.wielkaracza.common.controller.ExceptionHandlerController.*(*)) " +
            "&& args(exception)", argNames = "exception")
    public void exceptionLogging(final RuntimeException exception) {
    }

    @After(value = "exceptionLogging(exception)", argNames = "exception")
    public void afterThrowingException(final RuntimeException exception) {
        logger.error(exception.getMessage());
    }

    @SneakyThrows
    @Around(value = "execution(* lynx.code.wielkaracza.room.RoomCRUDFacade.changeRoomPrice(*, *))")
    public Object changingPrice(final ProceedingJoinPoint proceedingJoinPoint) {
        final var UUID = (UUID) Arrays.stream(proceedingJoinPoint.getArgs()).toList().get(0);
        logger.info("Changing price has been started for room with uuid: {} by user: {}", UUID, getUserDetailsSimple());
        final var proceed = proceedingJoinPoint.proceed();
        logger.info("Price of room changed for room with uuid: {} by user: {}", UUID, getUserDetailsSimple());
        return proceed;
    }
//    _______________________________________________

    @SneakyThrows
    @Around(value = "execution(* lynx.code.wielkaracza.user.UserFacade.createUser(*))")
    public Object creatingUser(final ProceedingJoinPoint proceedingJoinPoint) {
        final var userForm = (UserForm) Arrays.stream(proceedingJoinPoint.getArgs()).toList().get(0);
        logger.info("User is being created: {} by user: {}", userForm, getUserDetailsSimple());
        final var proceed = proceedingJoinPoint.proceed();
        logger.info("User has been created: {} by user: {}", userForm, getUserDetailsSimple());
        return proceed;
    }

    @SneakyThrows
    @Around(value = "execution(* lynx.code.wielkaracza.user.UserFacade.updateUser(*, *))")
    public Object updatingUser(final ProceedingJoinPoint proceedingJoinPoint) {
        final var userForm = (UserForm) Arrays.stream(proceedingJoinPoint.getArgs()).toList().get(1);
        final var UUID = (UUID) Arrays.stream(proceedingJoinPoint.getArgs()).toList().get(0);
        logger.info("User is being updated: {} - {} by user: {}", UUID, userForm, getUserDetailsSimple());
        final var proceed = proceedingJoinPoint.proceed();
        logger.info("User has been updated: {} - {} by user: {}", userForm, UUID, getUserDetailsSimple());
        return proceed;
    }

    @SneakyThrows
    @Around(value = "execution(* lynx.code.wielkaracza.user.UserFacade.deleteUser(*))")
    public Object deletingUser(final ProceedingJoinPoint proceedingJoinPoint) {
        final var UUID = (UUID) Arrays.stream(proceedingJoinPoint.getArgs()).toList().get(0);
        logger.info("User is being deleted: {} by user: {}", UUID, getUserDetailsSimple());
        final var proceed = proceedingJoinPoint.proceed();
        logger.info("User has been deleted: {} by user: {}", UUID, getUserDetailsSimple());
        return proceed;
    }

    //    _______________________________________________

    @SneakyThrows
    @Around(value = "execution(* lynx.code.wielkaracza.reservation.ReservationCRUDFacade.createReservation(*))")
    public Object creatingReservation(final ProceedingJoinPoint proceedingJoinPoint) {
        final var reservationForm = (ReservationForm) Arrays.stream(proceedingJoinPoint.getArgs()).toList().get(0);
        logger.info("Reservation is being created: {}", reservationForm);
        final var proceed = proceedingJoinPoint.proceed();
        logger.info("Reservation has been created: {}", reservationForm);
        return proceed;
    }
    @SneakyThrows
    @Around(value = "execution(* lynx.code.wielkaracza.reservation.ReservationCRUDFacade.deleteReservation(*))")
    public Object deletingReservation(final ProceedingJoinPoint proceedingJoinPoint) {
        final var UUID = (UUID) Arrays.stream(proceedingJoinPoint.getArgs()).toList().get(0);
        logger.info("Reservation is being deleted: {} by user: {}", UUID, getUserDetailsSimple());
        final var proceed = proceedingJoinPoint.proceed();
        logger.info("Reservation has been deleted: {} by user: {}", UUID, getUserDetailsSimple());
        return proceed;
    }
}