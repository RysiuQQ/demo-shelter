package lynx.code.wielkaracza.common.repository;

import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.UUID;

@NoRepositoryBean
public interface UuidAwareJpaRepository<T extends AbstractUUIDEntity, I> extends Repository<T, I> {
    T save(final T object);

    void deleteByUuid(final UUID uuid);
}
