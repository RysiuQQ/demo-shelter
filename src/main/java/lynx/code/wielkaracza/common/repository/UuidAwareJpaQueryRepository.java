package lynx.code.wielkaracza.common.repository;

import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@NoRepositoryBean
public interface UuidAwareJpaQueryRepository<T extends AbstractUUIDEntity, I> extends Repository<T, I> {

    List<T> findAll();

    Optional<T> findByUuid(final UUID uuid);

    boolean existsByUuid(final UUID uuid);
}
