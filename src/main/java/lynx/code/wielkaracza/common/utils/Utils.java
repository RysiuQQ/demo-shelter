package lynx.code.wielkaracza.common.utils;

public class Utils {

    public static String provideLikeParameter(String parameterToConvert){
        if(parameterToConvert!=null && parameterToConvert.isBlank() || "null".equals(parameterToConvert)){
            return "%%";
        } else {
            return "%" + (parameterToConvert != null ? parameterToConvert : "") + "%";
        }
    }

}
