package lynx.code.wielkaracza.unavailableterm;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface UnavailableQueryRepository extends UuidAwareJpaQueryRepository<UnavailableTerm, Long> {

    @Query("SELECT term FROM UnavailableTerm term WHERE term.reservationDayEnd >= :startDate" +
            " AND term.room.uuid = :roomUUID AND term.reservation.deleted = false AND " +
            " term.reservationDayStart <= :endDate")
    List<UnavailableTerm> getFutureDates(final LocalDate startDate, final UUID roomUUID, final LocalDate endDate);

    @Query(value = "SELECT * FROM unavailable_term WHERE unavailable_term.reservation_day_end >= :currentDate " +
            "AND unavailable_term.room_id = :id " +
            "AND :providedDate BETWEEN unavailable_term.reservation_day_start AND unavailable_term.reservation_day_end - INTERVAL '1 DAY'",
            nativeQuery = true)
    List<UnavailableTerm> getExactDates(@Param("currentDate") final LocalDate day,
                                        @Param("id") int id,
                                        @Param("providedDate") LocalDate providedDate);

}