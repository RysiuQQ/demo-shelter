package lynx.code.wielkaracza.unavailableterm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;
import lynx.code.wielkaracza.reservation.domain.Reservation;
import lynx.code.wielkaracza.room.domain.Room;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "unavailable_term")
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@FieldNameConstants
public class UnavailableTerm extends AbstractUUIDEntity {

    @Column(nullable = false)
    private LocalDate reservationDayStart;

    @Column(nullable = false)
    private LocalDate reservationDayEnd;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "reservation_id")
    private Reservation reservation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;
}
