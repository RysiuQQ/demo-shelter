package lynx.code.wielkaracza.user.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserQueryRepository extends UuidAwareJpaQueryRepository<User, Long> {

    Optional<User> findUsersByUsername(final String username);

    boolean existsByUsername(final String username);
}
