package lynx.code.wielkaracza.user.domain;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaRepository;

interface UserRepository extends UuidAwareJpaRepository<User, Long> {
}
