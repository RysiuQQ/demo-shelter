package lynx.code.wielkaracza.user.domain;

public enum UserRole {
    ROLE_ADMIN, ROLE_USER
}
