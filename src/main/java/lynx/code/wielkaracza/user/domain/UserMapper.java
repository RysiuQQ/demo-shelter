package lynx.code.wielkaracza.user.domain;

import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.user.UserFacade.UserDTO;
import lynx.code.wielkaracza.user.UserFacade.UserForm;
import org.springframework.security.crypto.password.PasswordEncoder;

import static java.util.Objects.nonNull;

@UtilityClass
class UserMapper {

    UserDTO toDTO(final User user) {
        return UserDTO.builder()
                .uuid(user.getUuid())
                .userRole(user.getRole().name())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .username(user.getUsername())
                .build();
    }

    User toEntity(final UserForm userForm, final PasswordEncoder encoder) {
        return User.builder()
                .username(userForm.username())
                .firstName(userForm.firstName())
                .lastName(userForm.lastName())
                .password(encoder.encode(userForm.password()))
                .notification(0)
                .role(UserRole.ROLE_USER)
                .build();
    }

    void updateUser(final UserForm userForm, final User user) {
        if (nonNull(userForm.firstName())) user.setFirstName(userForm.firstName());
        if (nonNull(userForm.lastName())) user.setLastName(userForm.lastName());
        if (nonNull(userForm.username())) user.setUsername(userForm.username());
    }
}
