package lynx.code.wielkaracza.user.domain;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.common.exception.NotFoundException;
import lynx.code.wielkaracza.user.UserFacade;
import org.springframework.security.acls.model.AlreadyExistsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.user.domain.UserService.ErrorMessages.USER_DOES_NOT_EXIST;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Service
@RequiredArgsConstructor
class UserService implements UserFacade {

    @UtilityClass
    static final class ErrorMessages {
        static final String USER_DOES_NOT_EXIST = "User with UUID: %s does not exist!";
        static final String USERNAME_ALREADY_EXISTS = "User with this login already exists!";
    }

    private final UserQueryRepository userQueryRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    @Override
    public List<UserDTO> getUsers() {
        return userQueryRepository.findAll().stream()
                .map(UserMapper::toDTO)
                .toList();
    }

    @Override
    public UserDTO getUser(final UUID uuid) {
        return UserMapper.toDTO(getUserOrThrow(uuid));
    }

    @Override
    public UserDTO createUser(final UserForm userForm) {
        if (isTrue(userQueryRepository.existsByUsername(userForm.username())))
            throw new AlreadyExistsException(ErrorMessages.USERNAME_ALREADY_EXISTS);

        final var user = UserMapper.toEntity(userForm, encoder);

        return UserMapper.toDTO(userRepository.save(user));
    }

    @Override
    @Transactional
    public void updateUser(final UUID uuid, final UserForm userForm) {
        final var user = getUserOrThrow(uuid);
        UserMapper.updateUser(userForm, user);
    }

    @Override
    @Transactional
    public void deleteUser(final UUID uuid) {
        userRepository.deleteByUuid(uuid);
    }

    @Override
    @Transactional
    public void addNotification() {
        userQueryRepository.findAll().forEach(user -> user.setNotification(user.getNotification() + 1));
    }

    @Override
    public int getNotifications(final UUID uuid) {
        return getUserOrThrow(uuid).getNotification();
    }

    @Override
    @Transactional
    public void resetNotification(final UUID uuid) {
        getUserOrThrow(uuid).setNotification(0);
    }

    @Override
    @Transactional
    public void changeUserPassword(final UUID uuid, final UserPassword password) {
        getUserOrThrow(uuid).setPassword(encoder.encode(password.password()));
    }

    private User getUserOrThrow(final UUID uuid) {
        return userQueryRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException(USER_DOES_NOT_EXIST, uuid.toString()));
    }
}
