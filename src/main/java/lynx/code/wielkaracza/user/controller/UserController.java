package lynx.code.wielkaracza.user.controller;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lynx.code.wielkaracza.user.UserFacade;
import lynx.code.wielkaracza.user.UserFacade.UserDTO;
import lynx.code.wielkaracza.user.UserFacade.UserForm;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.user.UserFacade.UserPassword;
import static lynx.code.wielkaracza.user.controller.UserController.Routes.*;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequiredArgsConstructor
@CrossOrigin(originPatterns = "*")
class UserController {

    @UtilityClass
    static final class Routes {
        static final String ROOT = "api/users";
        static final String SINGLE_USER = ROOT + "/{uuid}";
        static final String NOTIFICATIONS = ROOT + "/notifications" + "/{uuid}";
    }

    private final UserFacade userFacade;

    @GetMapping(ROOT)
    @PreAuthorize("hasRole('ADMIN')")
    List<UserDTO> getUsers() {
        return userFacade.getUsers();
    }

    @GetMapping(SINGLE_USER)
    @PreAuthorize("hasRole('ADMIN')")
    UserDTO getUser(@PathVariable final UUID uuid) {
        return userFacade.getUser(uuid);
    }

    @PostMapping(ROOT)
//    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(CREATED)
    UserDTO createUser(@RequestBody final UserForm userForm) {
        return userFacade.createUser(userForm);
    }

    @PutMapping(SINGLE_USER)
    @ResponseStatus(NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN')")
    void updateUser(@PathVariable final UUID uuid, @Valid @RequestBody final UserForm userForm) {
        userFacade.updateUser(uuid, userForm);
    }

    @PatchMapping(SINGLE_USER)
    @ResponseStatus(NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN')")
    void changeUserPassword(@PathVariable final UUID uuid, @Valid @RequestBody final UserPassword password) {
        userFacade.changeUserPassword(uuid, password);
    }

    @DeleteMapping(SINGLE_USER)
    @ResponseStatus(NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN')")
    void deleteUser(@PathVariable final UUID uuid) {
        userFacade.deleteUser(uuid);
    }

    @GetMapping(NOTIFICATIONS)
    @PreAuthorize("hasRole('ADMIN')")
    int getUserNotifications(@PathVariable final UUID uuid) {
        return userFacade.getNotifications(uuid);
    }

    @PatchMapping(NOTIFICATIONS)
    @ResponseStatus(NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN')")
    void resetUserNotifications(@PathVariable final UUID uuid) {
        userFacade.resetNotification(uuid);
    }
}