package lynx.code.wielkaracza.user;

import lombok.Builder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;

public interface UserFacade {

    List<UserDTO> getUsers();

    UserDTO getUser(final UUID uuid);

    UserDTO createUser(final UserForm userForm);

    void updateUser(final UUID uuid, final UserForm userForm);

    void deleteUser(final UUID uuid);

    void addNotification();

    int getNotifications(final UUID uuid);

    void resetNotification(final UUID uuid);

    void changeUserPassword(final UUID uuid, final UserPassword password);

    @Builder
    record UserDTO(UUID uuid,
                   String username,
                   String firstName,
                   String userRole,
                   String lastName) {
    }

    @Builder
    record UserForm(@NotBlank
                    @Length(max = 50)
                    String username,

                    @NotBlank
                    @Length(max = 50)
                    String firstName,

                    @NotBlank
                    @Length(max = 50)
                    String lastName,

                    String password) {
    }

    record UserPassword(@NotBlank @Length(max = 50) String password) {

    }
}
