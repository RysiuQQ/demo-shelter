package lynx.code.wielkaracza.amenities;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaRepository;

public interface AmenitiesRepository extends UuidAwareJpaRepository<Amenities, Long> {
}
