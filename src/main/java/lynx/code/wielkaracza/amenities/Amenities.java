package lynx.code.wielkaracza.amenities;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lynx.code.wielkaracza.common.entity.AbstractUUIDEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "amenities")
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class Amenities extends AbstractUUIDEntity {
    private String name;
    private String description;
    private String icon;
}
