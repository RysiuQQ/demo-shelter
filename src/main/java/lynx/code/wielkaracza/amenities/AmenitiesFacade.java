package lynx.code.wielkaracza.amenities;

import java.util.List;
import java.util.UUID;

public interface AmenitiesFacade {

    List<AmenitiesDTO> getAllAmenities();

    AmenitiesDTO getAmenity(final UUID uuid);

    void createAmenity(final AmenitiesDTO amenitiesDTO);

    void updateAmenity(final UUID uuid, final AmenitiesDTO amenitiesDTO);

    void deleteAmenity(final UUID uuid);

    record AmenitiesDTO(String name, String description, String icon) {
    }
}
