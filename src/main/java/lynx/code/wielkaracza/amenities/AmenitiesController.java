package lynx.code.wielkaracza.amenities;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static lynx.code.wielkaracza.amenities.AmenitiesFacade.AmenitiesDTO;

@RestController
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class AmenitiesController {

    private final AmenitiesFacade amenitiesFacade;

    @GetMapping("/api/amenities")
    public List<AmenitiesDTO> getAmenities() {
        return amenitiesFacade.getAllAmenities();
    }

    @GetMapping("/api/amenities/{uuid}")
    public AmenitiesDTO getAmenity(@PathVariable final UUID uuid) {
        return amenitiesFacade.getAmenity(uuid);
    }

    @PostMapping("/api/amenities")
    public void createAmenity(@RequestBody final AmenitiesDTO amenitiesDTO) {
        amenitiesFacade.createAmenity(amenitiesDTO);
    }

    @PutMapping("/api/amenities/{uuid}")
    public void updateAmenity(@PathVariable final UUID uuid, @RequestBody final AmenitiesDTO amenitiesDTO) {
        amenitiesFacade.updateAmenity(uuid, amenitiesDTO);
    }

    @DeleteMapping("/api/amenities/{uuid}")
    public void deleteAmenity(@PathVariable final UUID uuid) {
        amenitiesFacade.deleteAmenity(uuid);
    }
}
