package lynx.code.wielkaracza.amenities;

import lynx.code.wielkaracza.common.repository.UuidAwareJpaQueryRepository;

public interface AmenitiesQueryRepository extends UuidAwareJpaQueryRepository<Amenities, Long> {
}
