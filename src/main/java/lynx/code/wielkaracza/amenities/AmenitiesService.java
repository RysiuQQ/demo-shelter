package lynx.code.wielkaracza.amenities;

import lombok.RequiredArgsConstructor;
import lynx.code.wielkaracza.common.exception.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AmenitiesService implements AmenitiesFacade {

    private final AmenitiesQueryRepository amenitiesQueryRepository;
    private final AmenitiesRepository amenitiesRepository;

    @Override
    public List<AmenitiesDTO> getAllAmenities() {
        return amenitiesQueryRepository.findAll().stream()
                .map(amenities -> new AmenitiesDTO(amenities.getName(), amenities.getDescription(), amenities.getIcon()))
                .toList();
    }

    @Override
    public AmenitiesDTO getAmenity(final UUID uuid) {
        final var amenities = amenitiesQueryRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException("Amenity not found"));

        return new AmenitiesDTO(amenities.getName(), amenities.getDescription(), amenities.getIcon());
    }

    @Override
    public void createAmenity(final AmenitiesDTO amenitiesDTO) {
        amenitiesRepository.save(new Amenities(amenitiesDTO.name(), amenitiesDTO.description(), amenitiesDTO.icon()));
    }

    @Override
    public void updateAmenity(final UUID uuid, final AmenitiesDTO amenitiesDTO) {
        amenitiesQueryRepository.findByUuid(uuid)
                .ifPresentOrElse(amenities -> {
                    amenities.setName(amenitiesDTO.name());
                    amenities.setDescription(amenitiesDTO.description());
                    amenities.setIcon(amenitiesDTO.icon());
                    amenitiesRepository.save(amenities);
                }, () -> {
                    throw new NotFoundException("Amenity not found");
                });
    }

    @Override
    public void deleteAmenity(final UUID uuid) {
        amenitiesQueryRepository.findByUuid(uuid)
                .ifPresentOrElse(amenities -> amenitiesRepository.deleteByUuid(amenities.getUuid()), () -> {
                    throw new NotFoundException("Amenity not found");
                });
    }
}
