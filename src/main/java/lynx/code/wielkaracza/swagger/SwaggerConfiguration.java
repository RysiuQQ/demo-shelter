package lynx.code.wielkaracza.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
class SwaggerConfiguration {

    @Bean
    public Docket get() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .apiInfo(createApiInfo());
    }

    private ApiInfo createApiInfo() {
        return new ApiInfo("Wielka-Racza API",
                "API stworzone na potrzeby schroniska górskiego - Wielka Racza",
                "1.00",
                "http://wielka-racza.pttk.pl",
                "All Right Reserved",
                "http://wielka-racza.pttk.pl",
                "2022"
        );
    }
}
