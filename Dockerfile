FROM maven:3.6.3-openjdk-17-slim AS build

WORKDIR /opt/app

COPY ./ /opt/app
RUN mvn clean install -DskipTests

FROM openjdk:17.0.2-slim-buster

COPY --from=build /opt/app/target/*.jar app.jar

ENV PORT 8085
EXPOSE $PORT

ENTRYPOINT ["java","-jar","-Xmx1024M", "-Djava.security.egd=file:/dev/./urandom", "-Dspring.profiles.active=dev", "-Dserver.port=${PORT}","app.jar"]
